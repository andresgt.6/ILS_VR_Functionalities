﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Resalta elementos 2D usando el color.
/// </summary>
public class HighlightColor : HighlightBase
{
    #region Variables
    [Header("Color Settings")]
    [Tooltip("Color con el que empieza el highLight")]
    public Color initialColor;
    [Tooltip("Color con el que finaliza el highLight")]
    public Color finalColor;    
    #endregion

    #region Mono
    protected override void Awake()
    {
        base.Awake();
    }
    #endregion

    #region Overrides Set
    public override void SetEnabled()
    {
        base.SetEnabled();
    }

    public override void SetDisabled(bool DisableRendererComponent = false)
    {
        base.SetDisabled(DisableRendererComponent);
    }

    public override void SetDefault()
    {
        base.SetDefault();
        switch (type)
        {
            case TypeOfHighlight.SpriteRenderer:
                currentSpriteRenderer.color = Color.clear;
                break;

            case TypeOfHighlight.SpriteRendererChilds:                
                for (int i = 0; i < currentSprites.Length; i++)
                    currentSprites[i].color = Color.white;
                break;

            case TypeOfHighlight.Image:                
                currentImage.color = Color.white;
                break;

            case TypeOfHighlight.ImageChilds:                
                for (int i = 0; i < currentImages.Length; i++)
                    currentImages[i].color = Color.white;
                break;

            default:
                break;
        }
    }
    #endregion

    #region Overrides Make Highlight
    protected override void BeginHighlight()
    {
        base.BeginHighlight();               
    }

    protected override void UpdateHighlight(float value)
    {
        base.UpdateHighlight(value);

        switch (type)
        {
            case TypeOfHighlight.SpriteRenderer:
                currentSpriteRenderer.color = Color.Lerp(initialColor, finalColor, value);
                break;

            case TypeOfHighlight.SpriteRendererChilds:
                for (int i = 0; i < currentSprites.Length; i++)
                    currentSprites[i].color = Color.Lerp(initialColor, finalColor, value);                
                break;

            case TypeOfHighlight.Image:
                currentImage.color = Color.Lerp(initialColor, finalColor, value);
                break;

            case TypeOfHighlight.ImageChilds:
                for (int i = 0; i < currentImages.Length; i++)
                    currentImages[i].color = Color.Lerp(initialColor, finalColor, value);                
                break;

            default:
                break;
        }        
    }

    protected override void MiddleHighlight()
    {
        base.MiddleHighlight();
    }

    protected override void CompleteHighlight()
    {
        base.CompleteHighlight();
    }

    #endregion
}
