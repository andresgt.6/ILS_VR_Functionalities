﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Resalta elementos 2D usando el tamaño.
/// </summary>
public class HighlightSize : HighlightBase
{
    #region Variables
    [Header("Size Settings")]
    [Tooltip("Tamaño en el que empieza el highLight")]
    public Vector2 initialSize;
    [Tooltip("Tamaño en el que finaliza el highLight")]
    public Vector2 finalSize;


    private Vector2 startedSize;
    #endregion

    #region Mono
    protected override void Awake()
    {
        base.Awake();
        startedSize = transform.localScale;
    }
    #endregion

    #region Overrides Set
    public override void SetEnabled()
    {
        base.SetEnabled();
    }

    public override void SetDisabled(bool DisableRendererComponent)
    {
        base.SetDisabled(DisableRendererComponent);
    }

    public override void SetDefault()
    {
        base.SetDefault();
        switch (type)
        {
            case TypeOfHighlight.SpriteRenderer:
            case TypeOfHighlight.Image:
                transform.localScale = startedSize;
                break;

            case TypeOfHighlight.SpriteRendererChilds:
                for (int i = 0; i < currentSprites.Length; i++)
                    currentSprites[i].transform.localScale = startedSize;
                break;


            case TypeOfHighlight.ImageChilds:
                for (int i = 0; i < currentImages.Length; i++)
                    currentImages[i].transform.localScale = startedSize;
                break;

            default:
                break;
        }
    }
    #endregion

    #region Overrides Make Highlight
    protected override void BeginHighlight()
    {
        base.BeginHighlight();
    }

    protected override void UpdateHighlight(float value)
    {
        base.UpdateHighlight(value);

        switch (type)
        {
            case TypeOfHighlight.SpriteRenderer:
            case TypeOfHighlight.Image:                               
                transform.localScale = Vector3.Lerp(initialSize, finalSize, value);                
                break;

            case TypeOfHighlight.SpriteRendererChilds:
                for (int i = 0; i < currentSprites.Length; i++)
                    currentSprites[i].transform.localScale = Vector3.Lerp(initialSize, finalSize, value);
                break;


            case TypeOfHighlight.ImageChilds:
                for (int i = 0; i < currentImages.Length; i++)
                    currentImages[i].transform.localScale = Vector3.Lerp(initialSize, finalSize, value);
                break;

            default:
                break;
        }
    }

    protected override void MiddleHighlight()
    {
        base.MiddleHighlight();
    }

    protected override void CompleteHighlight()
    {
        base.CompleteHighlight();
    }

    #endregion
}
