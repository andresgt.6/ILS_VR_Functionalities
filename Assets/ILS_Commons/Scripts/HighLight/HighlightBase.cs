﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum TypeOfHighlight
{
    SpriteRenderer,
    SpriteRendererChilds,
    Image,
    ImageChilds,
    Shader
}
/// <summary>
/// Resalta objetos 2D, Sprite Renderers y Images(UI)
/// </summary>
public class HighlightBase : MonoBehaviour
{
    #region Variables
    public UnityAction CompleteTimes;

    [Header("Current Settings")]
    [Tooltip("número de Identificador")]
    public int currentId;
    [Tooltip("Tipo de Componente que va a realizar el HighLight")]
    public TypeOfHighlight type;
    [Tooltip("¿Esta activo el highlight?")]
    public bool isActive;

    [Header("Duration Settings")]
    [Tooltip("Tiempo de duracion en que va a estar el HighLight")]
    public float highlightDuration;
    [Tooltip("Tiempo en que va a ejecutar el higlight")]
    public float highlightSpeed;
    [Tooltip("Numero de veces en que va a reproducir el highlight si esta en 0 se va a realizar siempre")]
    public int times;

    [Header("Value settings")]
    [Tooltip("Valor con el que comienza el Value en el highlight inicial")]
    [SerializeField] protected int fromValueInitial = 0;
    [Tooltip("Valor con el que termina el Value en el highlight inicial")]
    [SerializeField] protected int toValueInitial = 1;
    [Space]
    [Tooltip("Valor con el que comienza el Value en el highlight final")]
    [SerializeField] protected int fromValueFinal = 1;
    [Tooltip("Valor con el que termina el Value en el highlight final")]
    [SerializeField] protected int toValueFinal = 0;
    [Space]
    [Tooltip("¿Comienza realizando el Highlight?")]
    [SerializeField] private bool startWithHighlight;

    private int amountOfTimes;

    protected SpriteRenderer currentSpriteRenderer;
    protected SpriteRenderer[] currentSprites;

    protected Image currentImage;
    protected Image[] currentImages;
    #endregion

    #region Mono

    protected virtual void Awake()
    {
        switch (type)
        {
            case TypeOfHighlight.SpriteRenderer:
                currentSpriteRenderer = GetComponent<SpriteRenderer>();
                break;

            case TypeOfHighlight.SpriteRendererChilds:
                currentSprites = GetComponentsInChildren<SpriteRenderer>();
                break;

            case TypeOfHighlight.Image:
                currentImage = GetComponent<Image>();
                break;

            case TypeOfHighlight.ImageChilds:
                currentImages = GetComponentsInChildren<Image>();
                break;

            default:
                Debug.LogError("No se ha especificado el tipo de highlight correctamente");
                break;
        }

        if (startWithHighlight)
            SetEnabled();
    }
    #endregion

    #region Set Enabled
    /// <summary>
    /// Habilita el HighLight
    /// </summary>
    public virtual void SetEnabled()
    {
        isActive = true;
        BeginHighlight();
    }

    /// <summary>
    /// Deshabilita el HighLight
    /// </summary>
    /// <param name="DisableRendererComponent">Deshabilita el componente que hace que se renderize.</param>
    public virtual void SetDisabled(bool DisableRendererComponent = false)
    {
        isActive = false;
        LeanTween.cancel(gameObject);
        amountOfTimes = 0;
        if (DisableRendererComponent)
            SetRenderComponent(false);
        SetDefault();
    }

    /// <summary>
    /// Habilita o deshabilita el componente que hace que se renderize el efecto de highlight.
    /// </summary>
    /// <param name="enable"></param>
    private void SetRenderComponent(bool enable)
    {
        switch (type)
        {
            case TypeOfHighlight.SpriteRenderer:
                currentSpriteRenderer.enabled = enable;
                break;

            case TypeOfHighlight.SpriteRendererChilds:
                for (int i = 0; i < currentSprites.Length; i++)
                    currentSprites[i].enabled = enable;
                break;

            case TypeOfHighlight.Image:
                currentImage.enabled = enable;
                break;

            case TypeOfHighlight.ImageChilds:
                for (int i = 0; i < currentImages.Length; i++)
                    currentImages[i].enabled = enable;
                break;

            case TypeOfHighlight.Shader:
                break;

            default:
                Debug.LogError("No se ha especificado el tipo de highlight correctamente");
                break;
        }
    }

    /// <summary>
    /// Configura el objeto en el estado como comenzó
    /// </summary>
    public virtual void SetDefault()
    {
        amountOfTimes = 0;
    }

    #endregion

    #region Make HighLight
    /// <summary>
    /// Comienza el HighLight
    /// </summary>
    protected virtual void BeginHighlight()
    {
        if (!isActive)
            return;

        LeanTween.value(gameObject, UpdateHighlight, fromValueInitial, toValueInitial, highlightSpeed).setOnComplete(DelayForMiddleHighlight);
    }

    /// <summary>
    /// Al actualizar el valor del highLight, aqui cada hijo debe de actualizar sus valores para realizar el highlight
    /// </summary>
    /// <param name="value">Valor actual del parametro a cambiar</param>
    protected virtual void UpdateHighlight(float value)
    {

    }

    /// <summary>
    /// Tiempo en que dura el highlight
    /// </summary>
    protected void DelayForMiddleHighlight()
    {
        Invoke("MiddleHighlight", highlightDuration);
    }

    /// <summary>
    /// Al completar el 
    /// </summary>
    protected virtual void MiddleHighlight()
    {
        if (!isActive)
            return;

        LeanTween.value(gameObject, UpdateHighlight, fromValueFinal, toValueFinal, highlightSpeed).setOnComplete(CompleteHighlight);
    }

    /// <summary>
    /// Al completar el HighLight
    /// </summary>
    protected virtual void CompleteHighlight()
    {
        if (gameObject.activeInHierarchy)
        {
            if (times == 0 || amountOfTimes <= times)
            {
                BeginHighlight();
                amountOfTimes++;
            }
            else
            {
                CompleteTimes?.Invoke();
                enabled = false;
            }
        }
        else
        {
            SetDisabled();
        }
    }

    #endregion
}
