﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightShader : HighlightBase
{
    #region Variables
    [Space,Header("Shader Settings")]
    [Tooltip("Nombre del shader")]    
    public string shaderName;

    [Space, Header("Shader Current State")]
    [Tooltip("Mesh Renderers a interactuar")]
    [SerializeField] private List<MeshRenderer> meshRenderers;
    [Tooltip("Skinned Mesh Renderers a interactuar")]
    [SerializeField] private List<SkinnedMeshRenderer> skinnedMeshRenderers;
    #endregion

    #region Mono
    protected override void Awake()
    {
        base.Awake();        
    }
    #endregion

    #region Overrides Set
    public override void SetEnabled()
    {
        base.SetEnabled();
    }

    public override void SetDisabled(bool DisableRendererComponent = false)
    {
        base.SetDisabled(DisableRendererComponent);
        LeanTween.cancel(gameObject);
        for (int i = 0; i < meshRenderers.Count; i++)
        {
            meshRenderers[i].material.SetFloat(shaderName, 0);
        }
        for (int i = 0; i < skinnedMeshRenderers.Count; i++)
        {
            skinnedMeshRenderers[i].material.SetFloat(shaderName, 0);
        }

    }

    public override void SetDefault()
    {
        base.SetDefault();
        
    }
    #endregion

    #region Overrides Make Highlight
    protected override void BeginHighlight()
    {
        base.BeginHighlight();        
    }

    protected override void UpdateHighlight(float value)
    {
        base.UpdateHighlight(value);

        for (int i = 0; i < meshRenderers.Count; i++)
        {
            meshRenderers[i].material.SetFloat(shaderName, value);
        }
        for (int i = 0; i < skinnedMeshRenderers.Count; i++)
        {
            skinnedMeshRenderers[i].material.SetFloat(shaderName, value);
        }
    }

    protected override void MiddleHighlight()
    {
        base.MiddleHighlight();
    }

    protected override void CompleteHighlight()
    {
        base.CompleteHighlight();
    }

    #endregion
}
