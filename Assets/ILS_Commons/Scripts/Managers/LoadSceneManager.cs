﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class LoadSceneManager : MonoBehaviour
{
	private Action completeSceneCallback;
	private String currentScene = null;

	[SerializeField] private AmbienceSoundManager ambienceSoundManager;

	public void LoadAsyncScene(Places places, Action CompleteSceneLoad)
    {
		completeSceneCallback = CompleteSceneLoad;
        StartCoroutine(LoadScene(places));
    }

    protected IEnumerator LoadScene(Places places)
    {
		
		string sceneToLoad = "";
		LeanTween.value(gameObject, value => Shader.SetGlobalFloat("_animationTime", value), 0, 1, 2);

		if(currentScene != null)
			SceneManager.UnloadSceneAsync(currentScene);

		switch (places)
        {
            case Places.Empty:
                sceneToLoad = "Art_Placeholder";
                break;

            case Places.Musseum:
                sceneToLoad = "Art_Placeholder";				
				break;

			case Places.Module2:
				sceneToLoad = "Module_2_ArtPlaceholder";				
				break;

			case Places.Module5:
				sceneToLoad = "Rule_1";
				break;

			case Places.MineOutside:
                sceneToLoad = "MineExterior_Placeholder";                
                break;

			case Places.DressingRoom:
				sceneToLoad = "Module_5_ArtPlaceholder";
				break;

			case Places.Warehouse:
				sceneToLoad = "WarehouseDressinRoom_ArtPlaceholder";
				break;

			default:
                Debug.LogError("No se ha especificado la habitación");
                break;
        }

		currentScene = sceneToLoad;

		yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
        asyncOperation.allowSceneActivation = false;
        Debug.Log(string.Format("Carga de la escena {0} : {1}", sceneToLoad, asyncOperation.progress));
        while (!asyncOperation.isDone)
        {
			//m_Text.text = "Loading progress: " + (asyncOperation.progress * 100) + "%";

			if (asyncOperation.progress >= 0.9f)
            {

				asyncOperation.allowSceneActivation = true;
            }
			yield return new WaitForEndOfFrame();
        }
		
		yield return new WaitForSeconds(1);
		ambienceSoundManager.SetAmbientSound(places);
		completeSceneCallback?.Invoke();
	}
}
