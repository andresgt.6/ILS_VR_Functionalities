﻿using System.Xml;
using UnityEngine;

public class XMLManager
{
    private XmlDocument xmlDocument;
    private TextAsset textAsset;

    private bool loaded = false;

    public void LoadXML()
    {
        textAsset = Resources.Load("XML/IndicatorUIs") as TextAsset;
        xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(textAsset.text);

        loaded = true;
    }

    public string ModuleTitleText(int moduleId)
    {
        if (!loaded)
        {
            Debug.LogError("Aun no se ha cargado el XML");
            return "No se ha cargado el XML";
        }

        if (xmlDocument == null)
        {
            Debug.LogError("xml is null");
            return "xml is null";
        }

        string currentString = xmlDocument.SelectSingleNode(string.Format("IndicatorUI/Module{0}/Title", moduleId)).InnerText;

        return currentString;
    }

    public string ModuleNameText(int moduleId)
    {
        if (!loaded)
        {
            Debug.LogError("Aun no se ha cargado el XML");
            return "No se ha cargado el XML";
        }

        if (xmlDocument == null)
        {
            Debug.LogError("xml is null");
            return "xml is null";
        }

        string currentString = xmlDocument.SelectSingleNode(string.Format("IndicatorUI/Module{0}/Name", moduleId)).InnerText;

        return currentString;
    }

    public InformativeSettings InformativeTexts(InformativeState informativeState,int moduleId)
    {
        InformativeSettings info = new InformativeSettings();
        if (!loaded)
        {
            Debug.LogError("Aun no se ha cargado el XML");
            return info;
        }

        if (xmlDocument == null)
        {
            Debug.LogError("xml is null");
            return info;
        }

        string state = "";
        switch (informativeState)
        {
            case InformativeState.Fail:
                state = "Fail";
                break;
            default:
                break;
        }        

        string currentString = xmlDocument.SelectSingleNode(string.Format("IndicatorUI/Informative/{0}/Title", state, moduleId)).InnerText;
        info.titleString = xmlDocument.SelectSingleNode(string.Format("IndicatorUI/Informative/{0}/Title", state, moduleId)).InnerText;

        return info;
    }


    public string ModuleGuideText(int moduleId, int textId)
    {
        if(!loaded)
        {
            Debug.LogError("Aun no se ha cargado el XML");
            return "No se ha cargado el XML";
        }

        if(xmlDocument == null)
        {
            Debug.LogError("xml is null");
            return "xml is null";
        }

        string currentString  = xmlDocument.SelectSingleNode(string.Format("IndicatorUI/Module{0}/Indicator{1}",moduleId,textId)).InnerText;        

        return currentString;
    }

	public DidYouKnowInfo GetDidUKnow(int moduleId, int id)
	{
		DidYouKnowInfo info = new DidYouKnowInfo();
		if (!loaded)
		{
			Debug.LogError("Aun no se ha cargado el XML");
			return info;
		}

		if (xmlDocument == null)
		{
			Debug.LogError("xml is null");
			return info;
		}

		info.title = xmlDocument.SelectSingleNode(string.Format("IndicatorUI/Informative/DidYouKnow")).InnerText;
		info.body = xmlDocument.SelectSingleNode(string.Format("IndicatorUI/Module{0}/DidYouKnow{1}", moduleId, id)).InnerText;

		return info;
	}
}