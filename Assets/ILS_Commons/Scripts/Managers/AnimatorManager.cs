﻿using UnityEngine;

public class AnimatorManager : MonoBehaviour
{
	protected Animator animator;

	protected virtual void Awake ()
	{
		animator = GetComponent <Animator> ();
		DesactiveAnimations ();
	}

	public virtual void ChangeAnimation<T> (T animation, bool isTrigger = false, bool activate = true)
	{
		DesactiveAnimations ();
		if (!isTrigger) {
			animator.SetBool (animation.ToString (), activate);
		} else {
			animator.SetTrigger (animation.ToString ());
		}
	}

	public virtual void DesactiveAnimations ()
	{        
        for (int i = 0; i < animator.parameterCount; i++)
            if (animator.parameters[i].type == AnimatorControllerParameterType.Bool)
                animator.SetBool(animator.parameters[i].name, false);
    }

    protected virtual void OnStartAnimation(string animationName)
    {

    }

    protected virtual void OnCompleteAnimation (string animationName)
	{
        
    }
    
}
