using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Singlenton Administrador del Sonido
/// </summary>
public class SoundManager : MonoBehaviour
{
	private static AudioSource[] sources;
	public AudioSource[] sourcespublic;

	private static SoundManager _instance;
	
	public static SoundManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<SoundManager>();
			}
			
			return _instance;
		}
	}

	private void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
		sources = _instance.GetComponentsInChildren<AudioSource> ();
		sourcespublic = sources;
	}

    public static AudioSource GetAudioSourceByName(string audioSourceName)
    {
        AudioSource acToReturn = null;
        for(int i = 0; i < sources.Length; i++)
        {
            if(sources[i].gameObject.name == audioSourceName)
            {
                acToReturn = sources[i];
                break;
            }
        }

        if(acToReturn == null)
        {
            print("No se encontro el AudioSource");
        }
        return acToReturn;
    }

	public static void LoadState()
	{
		if (PlayerPrefs.HasKey ("IsSoundActive")) {
			int isAudioActive = PlayerPrefs.GetInt("IsSoundActive");
			if(isAudioActive == 1)
			{
				Globals.IsAudioActive = true;
			}
			else
			{
				Globals.IsAudioActive = false;
			}
		} else {
			if(Globals.IsAudioActive)
			{
				PlayerPrefs.SetInt("IsSoundActive",1);
				
			}else
			{
				PlayerPrefs.SetInt("IsSoundActive",0);
			}
			PlayerPrefs.Save();
		}
	}

	public static void PlaySoundByName(string name, bool isLoop = false)
	{
		if (!ManagerChecker ())
			return;
        if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					if (!sources [i].isPlaying) {
						sources [i].loop = isLoop;
						sources [i].Play ();
                    }
				}
			}
		}
	}

	public static void UnpauseSoundByName(string name, bool isLoop = false)
	{
		if (!ManagerChecker())
			return;
		if (Globals.IsAudioActive)
		{
			for (int i = 0; i < sources.Length; i++)
			{
				if (sources[i].gameObject.name == name)
				{
					if (!sources[i].isPlaying)
					{
						sources[i].loop = isLoop;
						sources[i].UnPause();
					}
				}
			}
		}
	}


	public static void UpdateSoundByName(string name, bool isLoop = false, float volume = 1)
    {
        if (!ManagerChecker())
            return;

        if (Globals.IsAudioActive)
        {
            for (int i = 0; i < sources.Length; i++)
            {
                if (sources[i].gameObject.name == name)
                {
                    if (!sources[i].isPlaying)
                    {
                        sources[i].loop = isLoop;
                        sources[i].volume = volume;
                        sources[i].Play();
                    }
                }
            }
        }
    }

	public static void PlayResourcesSoundByName(string name, string address, bool isLoop = false)
	{
		if (!ManagerChecker ())
			return;

		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {

					AudioClip newAudio = Resources.Load <AudioClip> (address);

                    if (newAudio)
                    {
                        sources[i].clip = newAudio;
                        sources[i].loop = isLoop;
                        sources[i].Play();
                    }
                    else
                        Debug.LogError("No se encontro el Audio Source");

				}
			}
		}
	}

	public void PlayResourcesLoopByName(string name, string address, string loopAddress, float waitTime, bool isLoop = false)
	{
		if (!ManagerChecker ())
			return;

		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {

					AudioClip newAudio = Resources.Load <AudioClip> (address);
					sources [i].clip = newAudio;
					sources [i].loop = isLoop;
					sources [i].Play ();
				}
			}
		}

		if (waitTime != 0) {
			StartCoroutine (WaitToPlayLoop (name, loopAddress, waitTime, true));
		}
	}

	private IEnumerator WaitToPlayLoop(string name, string address, float waitTime, bool isLoop = false)
	{
		yield return new WaitForSeconds (waitTime);

		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {

					AudioClip newAudio = Resources.Load <AudioClip> (address);
					sources [i].clip = newAudio;
					sources [i].loop = isLoop;
					sources [i].Play ();

				}
			}
		}
	}

	private static bool ManagerChecker()
	{
		if (instance) {
			return true;
		} else {
			Debug.LogError ("No existe el manager de sonido");
			return false;
		}
	}

	public static void PauseSoundByName(string name)
	{
		if (!ManagerChecker ())
			return;
		
		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					sources [i].Pause ();
				}
			}
		}
	}

	public static void StopSoundByName(string name, bool isLoop)
	{
		if (!ManagerChecker ())
			return;
		
		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					sources [i].loop = isLoop;
					sources [i].Stop ();
				}
			}
		}
	}

	public static void ChangeSoundSpeedByName(string name, float speed)
	{
		if (!ManagerChecker ())
			return;

		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
						sources [i].pitch = speed;
				}
			}
		}
	}

	public static void ActiveAll()
	{
		if (!ManagerChecker ())
			return;

		Globals.IsAudioActive = true;
		for (int i = 0; i < sources.Length; i++) {
			sources[i].enabled = true;
		}
		PlayerPrefs.SetInt("IsSoundActive",1);
		PlayerPrefs.Save();
	}

	public static void DesactiveAll()
	{
		if (!ManagerChecker ())
			return;

		Globals.IsAudioActive = false;
		for (int i = 0; i < sources.Length; i++) {
			sources[i].enabled = false;
		}
		PlayerPrefs.SetInt("IsSoundActive",0);
		PlayerPrefs.Save();
	}

	public static void SilenceAll()
	{
		if (!ManagerChecker ())
			return;
		
		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				sources [i].volume = 0;
			}
		}
	}

	public static void StopAll()
	{
		if (!ManagerChecker ())
			return;
		
		if (Globals.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				sources [i].loop = false;
				sources [i].Stop ();
			}
		}
	}

}
