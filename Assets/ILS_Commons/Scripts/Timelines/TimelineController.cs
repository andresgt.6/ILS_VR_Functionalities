﻿
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Events;
public class TimelineController : MonoBehaviour
{
    #region Variables
    public UnityAction CompleteTimeline;
	/// <summary>
	/// When it´s invoked this will send the event id starting with 0
	/// </summary>
    public event UnityAction<int> ActionEvent;

    public enum TimelineState
    {
        Paused,
        Playing,
        Stopped,
        Completed
    }
    
    [Tooltip("Estdo actual del timeline")]
    [SerializeField] private TimelineState currentTimelineState;

    private PlayableDirector director;

    private int currentActionTimeProcess;

    #endregion

    #region Mono
    private void Awake()
    {
        InitializeController();
        Pause();
    }
    #endregion

    #region Set Director
    
    public void InitializeController()
    {
        if (director != null)
            return;

        director = GetComponent<PlayableDirector>();
        if (!director)
            Debug.LogError("No se ha encontrado el director");
    }

    public void Play(UnityAction CompleteTimeline = null)
    {
        this.CompleteTimeline = CompleteTimeline;
        director.Play();
        currentTimelineState = TimelineState.Playing;
    }

    public void Resume()
    {
        director.Resume();
        currentTimelineState = TimelineState.Playing;
    }

    public void Pause()
    {
        director.Pause();
        currentTimelineState = TimelineState.Paused;
    }

    public void Stop()
    {
        director.Stop();
        currentTimelineState = TimelineState.Stopped;
    }
    #endregion

    #region Events
    public void ResumeAtTime(double startTimePoint)
    {
        director.time = startTimePoint;
    }
    public virtual void OnActionTimeline()
    {
        ActionEvent?.Invoke(currentActionTimeProcess++);        
    }

    public virtual void OnCompleteTimeline()
    {
        currentActionTimeProcess = 0;
        currentTimelineState = TimelineState.Completed;
        CompleteTimeline?.Invoke();
        CompleteTimeline = null;
    } 
    #endregion

}
