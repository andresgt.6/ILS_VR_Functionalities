﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    private Transform playerT;

    private void Awake()
    {
        playerT = Camera.main.transform;
    }

    private void Update()
    {
        transform.LookAt(playerT);
    }
}
