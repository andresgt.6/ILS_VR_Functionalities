﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UISpriteController : MonoBehaviour
{
	[Header("UIImages")]
	[SerializeField] private Image gestureImage;
	[SerializeField] private Sprite[] sprites;

	[Header("Time To Loop/Animate")]
	[Tooltip("Tiempo de transición de la imagen")]
	[SerializeField] private float transitionTime;

	[Header("Text")]
	[SerializeField] private TMP_Text textToShow;

	private bool changeSprite = false;

	public enum UIImage
	{
		ClosedHand,
		Ok,
		CareOfU,
		Supervisor
	}

	private void Awake()
	{
		gestureImage.enabled = false;
		textToShow.text = "";
	}

	public void ChangeSprite(UIImage uiToShow)
	{
		gestureImage.enabled = true;
		switch (uiToShow)
		{
			case UIImage.Ok:
				changeSprite = true;
				StartCoroutine(ChangeSpriteCourutine(UIImage.ClosedHand, UIImage.Ok));
				break;
		}
	}

	/// <summary>
	/// Change the ui in a loop of images
	/// </summary>
	/// <param name="firstImage = Firts image to loop"></param>
	/// <param name="secondImage = Second image to loop"></param>
	/// <returns></returns>
	IEnumerator ChangeSpriteCourutine(UIImage firstImage, UIImage secondImage)
	{

		while (changeSprite)		{
			gestureImage.sprite = sprites[(int)firstImage];
			yield return new WaitForSeconds(transitionTime);
			gestureImage.sprite = sprites[(int)secondImage];
			yield return new WaitForSeconds(transitionTime);
		}
	}
	/// <summary>
	/// Change the ui for a image and animate
	/// </summary>
	/// <param name="sprite = Image to show"></param>
	/// <returns></returns>
	IEnumerator ChangeSpriteOnce(UIImage sprite)
	{
		gestureImage.sprite = sprites[(int)sprite];
		gestureImage.transform.localScale = new Vector3(0, 0, 0);
		LeanTween.scale(gestureImage.gameObject, new Vector3(1f, 1f, 1f), 1);
		yield return new WaitForSeconds(3);
		LeanTween.scale(gestureImage.gameObject, new Vector3(0, 0, 0), 1);
		yield return new WaitForSeconds(2);
	}
	public void DisableImage()
	{
		changeSprite = false;
		textToShow.text = "";
		//gestureImage.transform.localPosition = new Vector3(0, 2.3f, 0);
		gestureImage.enabled = false;
	}
}