﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditorInternal;
using UnityEngine;

/// <summary>
/// Its controls a single key of the keyBoard
/// </summary>
public class KeyController : MonoBehaviour
{
    public enum States
    {
        Disabled,
        Idle,
        Pressed
    }

    public States currentState = States.Disabled;
    [SerializeField] private TypeKeys type;

    private KeySettings currentSettings = new KeySettings();
    private Material keyMaterial;

    private void Awake()
    {
        currentState = States.Disabled;
        keyMaterial = GetComponentInChildren<MeshRenderer>().material;
    }

    public void Initialized(KeySettings customKeySettings = null)
    {
        currentState = States.Idle;

        if (customKeySettings != null)
            currentSettings = customKeySettings;
    }

    public void KeyPressed()
    {
        if (currentState != States.Idle)
            return;

        KeyBoardController.KeyPressed?.Invoke(transform.name[0], type);

        keyMaterial.SetFloat("_isPressed", 1);
        LeanTween.value(gameObject, value => keyMaterial.SetFloat("_isPressed", value), 0, 1, currentSettings.pressedSpeed);

        currentState = States.Pressed;
        //LeanTween.moveLocal(gameObject, transform.localPosition + currentSettings.pressedKeyValuePosition, currentSettings.pressedSpeed);
        LeanTween.scale(transform.GetChild(0).gameObject, new Vector3(currentSettings.scalePressed, currentSettings.scalePressed), currentSettings.pressedSpeed);

        Invoke("PressedComplete", currentSettings.pressedSpeed);
    }

    private void PressedComplete()
    {
        //LeanTween.moveLocal(gameObject, transform.localPosition - currentSettings.pressedKeyValuePosition, currentSettings.pressedSpeed);
        LeanTween.scale(transform.GetChild(0).gameObject, new Vector3(10,10,10), currentSettings.pressedSpeed);

        LeanTween.value(gameObject, value => keyMaterial.SetFloat("_isPressed", value), 1, 0, currentSettings.upSpeed);

        //LeanTween.delayedCall(currentSettings.upSpeed, CheckForDelay);
        Invoke("CheckForDelay", currentSettings.upSpeed);
    }

    private void CheckForDelay()
    {
        LeanTween.delayedCall(currentSettings.delayToCanPressAgain, KeyCompleteUnpressed);
    }

    private void KeyCompleteUnpressed()
    {
        currentState = States.Idle;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finger"))
        {
            KeyPressed();
        }
    }
}
