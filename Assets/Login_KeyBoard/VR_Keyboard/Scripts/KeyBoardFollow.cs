﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBoardFollow : MonoBehaviour
{
	public bool isActive;
	public float lerpPositionSpeed;
	private Transform playerEyes;


	private void Awake()
	{
		playerEyes = Camera.main.transform;
	}

	public void SetFollowerActive(bool isTrue)
	{
		if(!playerEyes)
			playerEyes = Camera.main.transform;

		isActive = isTrue;

		if (isTrue)
			StartCoroutine(FollowingProcess());
	}

	private IEnumerator FollowingProcess()
	{
		while (isActive)
		{
			CalculateLerp();
			yield return new WaitForEndOfFrame();			
		}

	}

	private void CalculateLerp()
	{
		Vector3 posToFollow = new Vector3(transform.position.x, playerEyes.position.y - 0.5f, transform.position.z);
		transform.position = Vector3.Lerp(transform.position, posToFollow, Time.deltaTime * lerpPositionSpeed);						
	}
}
