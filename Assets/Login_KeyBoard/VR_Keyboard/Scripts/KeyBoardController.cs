﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum TypeKeys
{
    Normal,
    Space,
    Delete,
    Accept    
}

public class KeyBoardController : MonoBehaviour
{
    public static Action<char, TypeKeys> KeyPressed;
    public event Action<string> UpdateString;
    public event Action Accept;

    public string currentString;

    [Space, Header("Settings")]
    [SerializeField] private KeySettings keySettings;
    [SerializeField] private bool autoInit;
    [SerializeField] private float delayToPressAgain;

    private KeyController[] keys;

    private void Awake()
    {
        KeyPressed += OnKeyPressed;
        keys = GetComponentsInChildren<KeyController>();
    }

    private void Start()
    {
        if (autoInit)
            Initialized();
    }

    public void Initialized()
    {
        for (int i = 0; i < keys.Length; i++)
        {
            keys[i].Initialized(keySettings);
        }
    }

    private void OnKeyPressed(Char currentChar, TypeKeys type)
    {
        switch (type)
        {
            case TypeKeys.Normal:
                currentString += currentChar;
                break;

            case TypeKeys.Space:
                currentString += " ";
                break;

            case TypeKeys.Delete:
                DeleteLastCharacter();
                break;

            case TypeKeys.Accept:
                Accept?.Invoke();
                break;

            default:
                break;
        }

        UpdateString?.Invoke(currentString);

        for (int i = 0; i < keys.Length; i++)
        {
            if (keys[i].currentState == KeyController.States.Idle)
                keys[i].currentState = KeyController.States.Disabled;
        }

        LeanTween.delayedCall(delayToPressAgain, KeysBackToNormal);
    }

    public void DeleteLastCharacter()
    {
        //if (currentString.Length > 0)
        //    currentString = currentString.Substring(0, currentString.Length - 1);

        if (currentString.Length > 0)
            currentString = currentString.Remove(currentString.Length - 1, 1);
    }

    private void KeysBackToNormal()
    {
        for (int i = 0; i < keys.Length; i++)
        {
            keys[i].currentState = KeyController.States.Idle;
        }
    }

    private void OnDestroy()
    {
        KeyPressed -= OnKeyPressed;
    }
}
