﻿


using System.Numerics;
/// <summary>
/// Set the configuration of the keys
/// </summary>
[System.Serializable]
public class KeySettings
{
    /// <summary>
    /// It sets the position of the key when its pressed
    /// </summary>
    public UnityEngine.Vector3 pressedKeyValuePosition;
    /// <summary>
    /// delay for can be pressed again
    /// </summary>
    public float delayToCanPressAgain = 1;
    public float pressedSpeed = 0.1f;
    public float upSpeed = 0.1f;
    public float scalePressed = 8;
}
