﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginUIManager : MonoBehaviour
{
    [SerializeField] private Canvas loadingCanvas;
    [SerializeField] private Canvas loginCanvas;

    [SerializeField] private Image loadingImage;
    [SerializeField] private Text loadingText;
    [SerializeField] private Text warningText;

    private void Awake()
    {
        warningText.enabled = false;
    }

    public void SetLoadingScreen(bool enabled,string message = "")
    {
        loadingCanvas.enabled = enabled;
        StartRotating();
        loadingText.text = message;
    }

    public void SetLoginScreen(bool enabled)
    {
        loginCanvas.enabled = enabled;
    }

    public void SetWarningText(string message)
    {
        warningText.enabled = true;

        if (message != "")
            warningText.text = message;
    }

    private void StartRotating()
    {
        if (!LeanTween.isTweening(loadingImage.gameObject))
            LeanTween.rotateAroundLocal(loadingImage.gameObject, new Vector3(0, 0, 1), -360, 1).setOnComplete(StartRotating);
    }
}
