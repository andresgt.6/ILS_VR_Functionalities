﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LoginManager : MonoBehaviour
{
    private enum CurrentState
    {
        code,
        Logging,
        Error,
        Complete
    }

    public event Action CompleteLogin;

    [Header("Current State")]
    [SerializeField] private CurrentState currentState;

    [Space, Header("UI components")]
    [SerializeField] private Text[] codeTexts;

    [Space, Header("Main components")]
    [SerializeField] private KeyBoardController keyBoard;
    [SerializeField] private LoginUIManager uiManager;

    [Space, Header("Settings")]
    [SerializeField] private int codeLenght = 5;

    private readonly DataBase.DataBaseManager DataBaseManager = new DataBase.DataBaseManager();

    private string failedMessage;

    private void Start()
    {
        keyBoard.gameObject.SetActive(false);
        keyBoard.Accept += OnAccept;
        keyBoard.UpdateString += OnUpdateString;

        LeanTween.delayedCall(3, DelayForKeyBoard);

        for (int i = 0; i < codeTexts.Length; i++)
        {
            codeTexts[i].text = "";
        }

#if UNITY_EDITOR
        //Borrame
        //DataBaseManager.Login("948902");
        //DataBaseManager.ErrorResponse += OnErrorResponse;
        //DataBaseManager.LoginSuccess += LoginSuccess;
#endif
    }

    private void DelayForKeyBoard()
    {
        keyBoard.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnAccept();
        }

        if (currentState == CurrentState.Error)
        {
            uiManager.SetLoginScreen(true);
            uiManager.SetLoadingScreen(false);
            uiManager.SetWarningText(failedMessage);

            currentState = CurrentState.code;
            keyBoard.currentString = "";
        }

        if (currentState == CurrentState.Logging)
        {
            LoginSuccess();
            currentState = CurrentState.Complete;
        }
    }

    private void OnAccept()
    {
        switch (currentState)
        {
            case CurrentState.code:
                if (LoginDataCorrect())
                    LogIn();
                break;
            default:
                break;
        }
    }
    private bool LoginDataCorrect()
    {
        string code = keyBoard.currentString;
        if (code.Length < codeLenght)
        {
            failedMessage = string.Format("El código debe de tener como mínimo {0} caracteres", codeLenght);
            uiManager.SetWarningText(failedMessage);
            return false;
        }
        else
        {
            Debug.Log("data correct");
            return true;
        }
    }

    private void OnUpdateString(string currentString)
    {
        if (codeTexts.Length < currentString.Length)
            keyBoard.DeleteLastCharacter();

        string keyboardString = keyBoard.currentString;

        for (int i = 0; i < codeTexts.Length; i++)
        {
            if (i < keyboardString.Length)
                codeTexts[i].text = keyboardString[i].ToString();
            else
                codeTexts[i].text = "";
        }

        //codeInputField.text = currentString.ToLower();        
    }

    private void LogIn()
    {
        uiManager.SetLoginScreen(false);
        uiManager.SetLoadingScreen(true, "Ingresando");

        //LeanTween.delayedCall(2, () =>
        //{
        //    uiManager.SetLoadingScreen(false);
        //    CompleteLogin?.Invoke();
        //});

        DataBaseManager.Login(keyBoard.currentString);
        DataBaseManager.ErrorResponse += OnErrorResponse;
        DataBaseManager.LoginSuccess += LoginSuccess;

        Debug.Log("loging");
    }

    private void OnErrorResponse(ErrorResponse errorResponse)
    {
        DataBaseManager.ErrorResponse -= OnErrorResponse;

        OnError(errorResponse.message);
    }

    private void OnCompleteLogin()
    {
        print("Ingresó correctamente");
        //networkManager.CompleteLogin -= OnCompleteLogin;
        //CompleteLogin?.Invoke();
        currentState = CurrentState.Logging;
    }

    private void LoginSuccess()
    {
        OnCompleteLogin();
        DataBaseManager.LoginSuccess -= LoginSuccess;
        CompleteLogin?.Invoke();
    }

    private void OnError(string failedMessage)
    {
        //networkManager.Error -= OnError;
        this.failedMessage = failedMessage;
        currentState = CurrentState.Error;
    }
}
