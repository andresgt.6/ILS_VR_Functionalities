﻿[System.Serializable]
public class LoginData
{
    #region Old way comment
    //public string email;
    //public string password;

    //public LoginData(string email, string password)
    //{
    //    this.email = email;
    //    this.password = password;
    //} 
    #endregion

    public string code;    
    public string courseId;    

    public LoginData(string code, string courseId)
    {
        this.code = code;        
        this.courseId = courseId;        
    }
}