﻿#undef DEBUG

using System;
using Proyecto26;
using UnityEngine;

namespace DataBase
{
    public class DataBaseManager
    {
        public Action LoginSuccess;
        public Action<int> DatabaseAnswer;
        public Action<ErrorResponse> ErrorResponse;
        private bool requestPending = false;

        private const string API = "https://ils-api-test-3ltxxuh6cq-ue.a.run.app/api/";
        //private const string API = "https://api.indielevelstudio.com/api/";
        private const string loginEp = "login/code";
        private const string scoreEp = "trainings/score";
        private const string surveyPath = "survey";

        public DataBaseManager() { }

        public void Register(string name, string email, string age, string job, string jobTime, string nat, string pass)
        {
            //RestClient.Post(API + "register", new UserData(name, email, age, job, jobTime, nat, pass, "", Constants.Game)).Then((response) => OnRegister(response)).Catch((error) => OnCatch((RequestException)error));
        }

        private void OnRegister(ResponseHelper response)
        {
            DatabaseAnswer?.Invoke((int)response.StatusCode);
        }

        public void Login(string code)
        {
            if (!requestPending)
            {
                requestPending = true;
                RestClient.Post(API + loginEp, new LoginData(code,Constants.courseId)).Then((response) => OnLogin(response)).Catch((error) => OnCatch((RequestException)error));
            }
        }
        private void OnLogin(ResponseHelper response)
        {
            // Store the user token and save to future requests
            if ((int)response.StatusCode == 200)
            {
                LoginResponse loginResponse = JsonUtility.FromJson<LoginResponse>(response.Text);
                RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + loginResponse.idToken;
                LoginSuccess?.Invoke();
            }
            requestPending = false;
            if (DatabaseAnswer != null)
            {
                DatabaseAnswer((int)response.StatusCode);
            }
        }

        public void Score(SessionResults data)
        {
            if (!requestPending)
            {
                requestPending = true;
                RestClient.Post(API + scoreEp, data).Then((response) => OnResultScore(response)).Catch((error) => OnCatch((RequestException)error));
            }
        }

        public void SendCalification(Survey survey)
        {

            if (!requestPending)
            {
                requestPending = true;
                RestClient.Post(API + surveyPath, survey).Then((response) => OnResultScore(response)).Catch((error) => OnCatch((RequestException)error));
            }
        }


        private void OnResultScore(ResponseHelper response)
        {
            requestPending = false;
            if (DatabaseAnswer != null)
            {
                DatabaseAnswer((int)response.StatusCode);
            }
        }

        private void OnCatch(RequestException error)
        {
            requestPending = false;
            ErrorResponse errorResponse = JsonUtility.FromJson<ErrorResponse>(error.Response);
            Debug.Log("error: " + errorResponse);
            if (ErrorResponse != null)
            {
                ErrorResponse(errorResponse);
            }
        }
    }
}