﻿using System;

[Serializable]
public class UserData
{
    public string name;
    public string email;
    public string age;
    public string job;
    public string jobTime;
    public string nat;
    public string pass;
    public string game;
    public string token;
}