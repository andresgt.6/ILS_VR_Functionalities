﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StarHoverButton : HoverButton
{
	public event Action<StarNumber> OnStarSelected;

	[SerializeField] Image spriteToShow;

	private Sprite onStar;
	private Sprite offStar;

	public enum StarNumber
	{
		One,
		Two,
		Three,
		Four,
		Five
	}
	public StarNumber currentStar;
	public override void InteractWithButton(Side handSide = Side.RightSide)
	{
		base.InteractWithButton();
		Debug.LogError("Se esta llamando el hover button cuando no deberia");
		OnStarSelected?.Invoke(currentStar);
	}

	public void Initialized(Sprite[] starStatesSprites)
	{
		offStar = starStatesSprites[0];
		onStar = starStatesSprites[1];
	}

	public void SetStar(bool filled)
	{
		if(filled)
		{
			spriteToShow.sprite = onStar;

		}
		else
		{
			spriteToShow.sprite = offStar;

		}
	}
}
