﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class UICalification : MonoBehaviour
{
	public int CalificationNumber { get; private set; }

	[SerializeField] StarHoverButton[] starHover;
	[SerializeField] Sprite[] fullStar;

	[SerializeField] HoverButton sendCalificationButton;
	[SerializeField] Canvas canvas;

	private Action CompleteCallback;

	private void Awake()
	{
		starHover = GetComponentsInChildren<StarHoverButton>();
		SetEnabled(false);
	}

	public void SetEnabled(bool enabled, Action Complete = null)
	{
		CompleteCallback = Complete;

		canvas.enabled = enabled;

		if(enabled)
		{
			sendCalificationButton.OnStateChanged += OnSendButtonPressed;
			sendCalificationButton.ActiveInteractions(1);
			for (int i = 0; i < starHover.Length; i++)
			{
				starHover[i].Initialized(fullStar);
				starHover[i].ActiveInteractions();
				starHover[i].OnStarSelected += ActiveStars;
			}
		}
		else
		{
			sendCalificationButton.OnStateChanged -= OnSendButtonPressed;
			sendCalificationButton.DesactiveInteractions();
		}
	}


	private void ActiveStars(StarHoverButton.StarNumber number)
	{
		for (int i = 0; i < starHover.Length; i++)
		{
			starHover[i].SetStar(i <= (int)number);
		}

		CalificationNumber = (int)number + 1;
	}
	private void OnSendButtonPressed(bool pressed)
	{
		CompleteCallback?.Invoke();
		CompleteCallback = null;
	}
}