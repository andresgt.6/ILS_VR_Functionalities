﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

#pragma warning disable 649

public class SteeringSensor : MonoBehaviour
{
    public UnityAction OnStateChanged;

    public bool isEnabled;

    [SerializeField] private RotableGrab grabPlace = null;
	[SerializeField] private Transform handlerContainer = null;

    [Range(-90,350)]
    [SerializeField] private float limitStart = 0;
    [Range(10,355)]
    [SerializeField] private float limitEnd = 90;
    [SerializeField] private float recoverySpeed = 5;

	public float SteerFactor { get; private set; }

    private float handlerXRotation;

	[SerializeField] private Transform rotationRef1 = null;
    [SerializeField] private Transform rotationRef2 = null;

    private Transform transformLeft;
	private Transform transformRight;

	private bool canInteract;

	private readonly Vector3 UP = Vector3.up;
	private readonly Vector3 FORWARD = Vector3.right;

    [SerializeField] private bool turnOffOnChengeState = false;
    [SerializeField] private bool startActive = false;
    [SerializeField] private bool recoverState = false;

    private void Awake()
    {
        if (limitEnd < limitStart + 1)
            limitEnd = limitStart + 1;

        SetControllers(rotationRef1, rotationRef2);

        //grabPlace.Initialize(handlerContainer);
    }

    private void Start()
    {
        if (startActive)
            SetActiveSteering(true, !isEnabled);
    }

    public void SetControllers(Transform _Controller1,  Transform _Controller2)
	{
        handlerXRotation = handlerContainer.localEulerAngles.x;

		Vector3 control1LocalPos = transform.InverseTransformPoint(_Controller1.position);
		Vector3 control2LocalPos = transform.InverseTransformPoint(_Controller2.position);

		if(control1LocalPos.x > control2LocalPos.x)
		{
			transformLeft = _Controller2;
			transformRight = _Controller1;
		}
		else
		{
			transformLeft = _Controller1;
			transformRight = _Controller2;
		}
	}

    public void SetActiveSteering(bool _isActive, bool startDesactive = true)
	{
		canInteract = _isActive;

        isEnabled = !startDesactive;

        if (_isActive)
        {
            handlerContainer.localRotation = Quaternion.Euler(handlerXRotation, startDesactive ? limitStart : limitEnd, 0);
            //grabPlace.UpdateRestingPosition();
            StartCoroutine(SteeringProcess());
        }
    }

	private IEnumerator SteeringProcess()
    {
        if (transformLeft == null  || transformRight == null)
            SetControllers(rotationRef1, rotationRef2);

        while (canInteract)
        {
            CalculateSteering();

            yield return null;
        }
        
	}

	private void CalculateSteering()
	{
		Vector3 transformsLocalDirection = transform.InverseTransformDirection((transformRight.position - transformLeft.position).normalized);

        transformsLocalDirection.y = 0;
        transformsLocalDirection.Normalize();

		float handlerRotation = Vector3.SignedAngle(FORWARD, transformsLocalDirection, UP);

        if (recoverState /*&& !grabPlace.isGrabbed*/)
            handlerRotation += isEnabled ? Time.deltaTime * recoverySpeed : Time.deltaTime * -recoverySpeed;

        handlerRotation = Mathf.Clamp(handlerRotation, limitStart, limitEnd);
        handlerContainer.localRotation = Quaternion.Euler(handlerXRotation, handlerRotation, 0);

        
            if (!isEnabled && handlerRotation >= limitEnd)
                ChangeState(true);
            else if (isEnabled && handlerRotation <= limitStart)
                ChangeState(false);

        SteerFactor = (handlerRotation - limitStart) / (limitEnd - limitStart);

        //if (recoverState && !grabPlace.isGrabbed)
        //    grabPlace.UpdateRestingPosition();

    }    

    private void ChangeState(bool _isActive)
    {
        Debug.LogError("Valve activated > " + _isActive);

        if (turnOffOnChengeState)
        {
            canInteract = false;
            isEnabled = _isActive;

            if (OnStateChanged != null)
                OnStateChanged();
        }
    }

}
