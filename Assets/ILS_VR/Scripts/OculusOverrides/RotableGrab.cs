﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class RotableGrab : ILSOculusGrabbable
{
    [Tooltip("Objeto que realiza la rotacion")]

    [SerializeField] private Transform handler;

    private Vector3 handlerRelativePosition;
    private Vector3 relativeRotation;

    //public void Initialize(Transform _handler)
    //{
    //    handler = _handler;
    //    handlerRelativePosition = transform.localPosition - handler.localPosition;
    //    relativeRotation = Vector3.up * (transform.localEulerAngles.y - handler.localEulerAngles.y);
    //}

    //public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
    //{
    //    base.GrabBegin(hand, grabPoint);

    //    StartCoroutine(GrabCheckProcess());
    //}


    //public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    //{
    //    base.GrabEnd(Vector3.zero, Vector3.zero);
    //    UpdateRestingPosition();
    //}

    //public void UpdateRestingPosition()
    //{
    //    transform.localPosition = handler.transform.localPosition + handlerRelativePosition;
    //    transform.localEulerAngles = handler.transform.localEulerAngles + relativeRotation;
    //}

    //protected IEnumerator GrabCheckProcess()
    //{
    //    while(isGrabbed)
    //    {
    //        if (Vector3.Distance(handler.position + handlerRelativePosition, transform.position) > 0.4f)
    //            grabbedBy?.ForceRelease(this);
            
    //        yield return null;
    //    }
    //}
}
