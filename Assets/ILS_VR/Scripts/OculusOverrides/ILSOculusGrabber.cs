﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ILSOculusGrabber //: OVRGrabber
{
    //protected enum HandFinger
    //{
    //    Thumb = OVRHand.HandFinger.Thumb,
    //    Index = OVRHand.HandFinger.Index,
    //    Middle = OVRHand.HandFinger.Middle,
    //    Ring = OVRHand.HandFinger.Ring,
    //    Pinky = OVRHand.HandFinger.Pinky,
    //    Max = OVRHand.HandFinger.Max,
    //}

    //public OVRInput.Controller Controller { get { return m_controller; } }

    //[SerializeField] protected OVRHand hand = null;
    //[SerializeField] protected HandFinger[] pitchVerificationFingers = new HandFinger[4] { HandFinger.Thumb, HandFinger.Index, HandFinger.Middle, HandFinger.Ring };

    //protected Dictionary<HoverButton, int> buttonCandidates = new Dictionary<HoverButton, int>();

    //protected float buttonInteractCooldown = 0;

    //protected float currentHandPinch = 0;


    //public override void Update()
    //{
    //    base.Update();

    //    var oldHandPinch = currentHandPinch;
    //    currentHandPinch = GetHandPinch();

    //    CheckForGrabOrReleaseHand(oldHandPinch); 
    //}

    //private float GetHandPinch()
    //{
    //    var maxPinchForce = 0f;

    //    foreach (var finger in pitchVerificationFingers)
    //    {
    //        var currentPinch = hand.GetFingerPinchStrength((OVRHand.HandFinger)finger);

    //        if (maxPinchForce < currentPinch)
    //            maxPinchForce = currentPinch;
    //    }

    //    return maxPinchForce;
    //}

    //protected void CheckForGrabOrReleaseHand(float oldHandPinch)
    //{
    //    if ((currentHandPinch >= grabBegin) && (oldHandPinch < grabBegin))
    //    {
    //        GrabBegin();
    //    }
    //    else if ((currentHandPinch <= grabEnd) && (oldHandPinch > grabEnd))
    //    {
    //        GrabEnd();
    //    }
    //}

    //void OnUpdatedAnchors()
    //{
    //    Vector3 handPos = OVRInput.GetLocalControllerPosition(m_controller);
    //    Quaternion handRot = OVRInput.GetLocalControllerRotation(m_controller);
    //    Vector3 destPos = m_parentTransform.TransformPoint(m_anchorOffsetPosition + handPos);
    //    Quaternion destRot = m_parentTransform.rotation * handRot * m_anchorOffsetRotation;
    //    GetComponent<Rigidbody>().MovePosition(destPos);
    //    GetComponent<Rigidbody>().MoveRotation(destRot);

    //    if (!m_parentHeldObject)
    //    {
    //        MoveGrabbedObject(destPos, destRot);
    //    }
    //    m_lastPos = transform.position;
    //    m_lastRot = transform.rotation;

    //    float prevFlex = m_prevFlex;
    //    // Update values from inputs
    //    m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

    //    CheckForGrabOrRelease(prevFlex);

    //    if (buttonInteractCooldown > 0)
    //        buttonInteractCooldown -= Time.fixedDeltaTime;
    //    else if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, m_controller))
    //    {
    //        float closestMagSq = float.MaxValue;
    //        HoverButton closestButton = null;
    //        Collider closestButtonCollider = null;

    //        // Iterate grab candidates and find the closest grabbable candidate
    //        foreach (HoverButton button in buttonCandidates.Keys)
    //        {
    //            Collider[] buttonColliders = button.GetColliders();

    //            for (int i = 0; i < buttonColliders.Length; i++)
    //            {
    //                Vector3 closestPointOnBounds = buttonColliders[i].ClosestPointOnBounds(m_gripTransform.position);
    //                float grabbableMagSq = (m_gripTransform.position - closestPointOnBounds).sqrMagnitude;

    //                if (grabbableMagSq < closestMagSq)
    //                {
    //                    closestMagSq = grabbableMagSq;
    //                    closestButton = button;
    //                    closestButtonCollider = buttonColliders[i];
    //                }
    //            }
                
    //            //button.InteractWithButton();
                
    //        }

    //        if (closestButton != null && closestButton.CanInteract)
    //        {
    //            closestButton.InteractWithButton();
    //            TriggerVibration(50, 1, 225);
    //            buttonInteractCooldown = 0.2f;
    //        }
    //    }
    //}

    //void OnDestroy()
    //{
    //    if (m_grabbedObj != null)
    //    {
    //        GrabEnd();
    //    }
    //}

    //void OnTriggerEnter(Collider otherCollider)
    //{
    //    // Get the grab trigger
    //    OVRGrabbable grabbable = otherCollider.GetComponent<OVRGrabbable>() ?? otherCollider.GetComponentInParent<OVRGrabbable>();
    //    if (grabbable != null)
    //    {
    //        if (!m_grabCandidates.ContainsKey(grabbable))
    //        {
    //            //ILSOculusGrabbable ILSgrabbable = grabbable.GetComponent<ILSOculusGrabbable>();
    //            //ILSgrabbable?.SetHover(true);

    //            if (grabbable is ILSOculusGrabbable)
    //                (grabbable as ILSOculusGrabbable).SetHover(true);
    //        }

    //        // Add the grabbable
    //        int refCount = 0;
    //        m_grabCandidates.TryGetValue(grabbable, out refCount);
    //        m_grabCandidates[grabbable] = refCount + 1;

    //    }
    //    else
    //    {
    //        HoverButton button = otherCollider.GetComponent<HoverButton>() ?? otherCollider.transform.parent.GetComponent<HoverButton>() ;

    //        if(button != null)
    //        {
    //            //if (!buttonCandidates.ContainsKey(button))
    //                //button.SetButtonHoverState(true);

    //            int buttonRefCount = 0;
    //            buttonCandidates.TryGetValue(button, out buttonRefCount);
    //            buttonCandidates[button] = buttonRefCount + 1;
    //        }
    //    }


    //}

    //void OnTriggerExit(Collider otherCollider)
    //{
    //    OVRGrabbable grabbable = otherCollider.GetComponent<OVRGrabbable>() ?? otherCollider.GetComponentInParent<OVRGrabbable>();

    //    if (grabbable != null)
    //    {
    //        // Remove the grabbable
    //        int refCount = 0;
    //        bool found = m_grabCandidates.TryGetValue(grabbable, out refCount);
    //        if (!found)
    //            return;

    //        if (refCount > 1)
    //            m_grabCandidates[grabbable] = refCount - 1;
    //        else
    //        {
    //            m_grabCandidates.Remove(grabbable);

    //            if (grabbable is ILSOculusGrabbable)
    //                (grabbable as ILSOculusGrabbable).SetHover(false);
    //        }
            
    //    }
    //    else
    //    {
    //        HoverButton button = otherCollider.GetComponent<HoverButton>() ?? otherCollider.transform.parent?.GetComponent<HoverButton>();

    //        if (button != null)
    //        {
    //            int buttonRefCount = 0;

    //            bool found = buttonCandidates.TryGetValue(button, out buttonRefCount);
    //            if (!found)
    //                return;

    //            if (buttonRefCount > 1)
    //                buttonCandidates[button] = buttonRefCount - 1;
    //            else
    //            {
    //                //button.SetButtonHoverState(false);
    //                buttonCandidates.Remove(button);
    //            }
    //        }
    //    }
        
    //}

    //protected override void GrabVolumeEnable(bool enabled)
    //{
    //    base.GrabVolumeEnable(enabled);

    //    if (!m_grabVolumeEnabled)
    //    {
    //        buttonCandidates.Clear();
    //    }
    //}

    //public void TriggerVibration(int iteration, int frequency, int strength)
    //{
    //    OVRHapticsClip clip = new OVRHapticsClip();

    //    for (int i = 0; i < iteration; i++)
    //        clip.WriteSample(i % frequency == 0 ? (byte)strength : (byte)0);

    //    if (m_controller == OVRInput.Controller.LTouch)
    //        OVRHaptics.LeftChannel.Preempt(clip);
    //    else
    //        OVRHaptics.RightChannel.Preempt(clip);
    //}

    //public void TriggerVibration(AudioClip audio)
    //{
    //    OVRHapticsClip clip = new OVRHapticsClip(audio);

    //    if (m_controller == OVRInput.Controller.LTouch)
    //        OVRHaptics.LeftChannel.Preempt(clip);
    //    else
    //        OVRHaptics.RightChannel.Preempt(clip);
    //}
}
