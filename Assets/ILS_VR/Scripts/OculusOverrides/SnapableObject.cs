﻿using System.Collections;
using System;
using UnityEngine;

public class SnapableObject : MonoBehaviour
{
    public event Action<SnapableObject> OnAttachCompleted;

    [SerializeField] protected bool canAttach = true;
    [SerializeField] protected float attachTime = 1;
    [SerializeField] protected bool lockOnAttach = false;
    [SerializeField] protected bool attachToStartPoint = false;

    protected bool isAttached = false;
    public bool IsAttached { get { return isAttached; } }
    public bool IsAttachedToStartPoint => startPoint != null && attachTarget == startPoint;

    private Transform attachTarget;
    private Transform startParent;
    private Transform startPoint;

    private ILSOculusGrabbable grabbable;
    private bool hasGrabbable;


    private void Awake()
    {
        grabbable = GetComponent<ILSOculusGrabbable>();
        hasGrabbable = grabbable != null;

        startParent = transform.parent;

        SetActiveAttach(canAttach);

        if (attachToStartPoint)
            CreateStartPoint();
    }

    public void SetActiveAttach(bool _canAttach)
    {
        canAttach = _canAttach;

        CheckForStartAttaching();

        if (!hasGrabbable)
            return;

        //if(_canAttach)
        //{
        //    grabbable.OnGrabBegin -= GrabBegin; // To avoid double registration
        //    grabbable.OnGrabEnd -= GrabEnd;     // To avoid double registration

        //    grabbable.OnGrabBegin += GrabBegin;
        //    grabbable.OnGrabEnd += GrabEnd;
        //}
        //else
        //{
        //    grabbable.OnGrabBegin -= GrabBegin;
        //    grabbable.OnGrabEnd -= GrabEnd;
        //}
    }

    private void GrabBegin()
    {
        isAttached = false;
        transform.parent = startParent; 
    }

    private void GrabEnd()
    {
        if (attachTarget != null)
            StartAttachLerp(attachTarget);
    }

    private void CheckForStartAttaching()
    {
        if (!canAttach)
            return;

        if (hasGrabbable)
        {
            //if (!grabbable.isGrabbed && attachTarget != null)
            //    StartAttachLerp(attachTarget);
        }
        else
        {
            if (attachTarget != null)
                StartAttachLerp(attachTarget);
        }
    }

    private void CreateStartPoint()
    {
        startPoint = new GameObject(name + "_StartPoint").transform;
        startPoint.SetParent(transform.parent);
        startPoint.SetPositionAndRotation(transform.position, transform.rotation);

        attachTarget = startPoint;
        isAttached = true;
    }

    public void SetAttachProperties(bool _hasAttachPoint, Transform target = null)
    {
        attachTarget = _hasAttachPoint ? target : null;

        CheckForStartAttaching();
    }

    public void StartAttachLerp(Transform _target)
    {
        if (!canAttach)
            return;

        Rigidbody rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
        rb.angularVelocity = Vector3.zero;
        rb.velocity = Vector3.zero;

        isAttached = true;
        //grabbable?.SetActiveGrab(false);

        StartCoroutine(AttachingProcess(_target));
    }

    protected IEnumerator AttachingProcess(Transform _target)
    {
        float lerpAmount = 0;
        //grabbable?.SetActiveGrab(false);

        Vector3 startPosition = transform.position;
        Quaternion startRotation = transform.rotation;

        Vector3 finalPosition = _target.position;
        Quaternion finalRotation = _target.rotation;

        while (lerpAmount < 1)
        {
            lerpAmount += Time.deltaTime / attachTime;

            transform.position = Vector3.Lerp(startPosition, finalPosition, lerpAmount);
            transform.rotation = Quaternion.Lerp(startRotation, finalRotation, lerpAmount);

            yield return null;
        }

        transform.position = _target.position;
        transform.rotation = _target.rotation;

        //grabbable?.SetActiveGrab((startPoint != null && _target == startPoint) ? true : !lockOnAttach);
        
        transform.SetParent(_target);

        if (OnAttachCompleted != null)
            OnAttachCompleted(this);
    }

}
