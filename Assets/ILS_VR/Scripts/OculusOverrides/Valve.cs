﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Valve : MonoBehaviour
{
    [SerializeField] private Transform objectToRotate;

    private bool isActive;

    private float rotateProgress;

    Rigidbody myBody;

    private void Awake()
    {
        SetValve(true);
        myBody = GetComponent<Rigidbody>();
    }

    public void SetValve(bool activate)
    {
        isActive = activate;
    }

    private void FixedUpdate()
    {
        if (!isActive)
            return;

        print("Rotacion es de " + rotateProgress);

        if ((rotateProgress >= 80 && rotateProgress < 100) || (rotateProgress > 80 && rotateProgress < 270))
        {
            GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
            GetComponent<MeshRenderer>().material.color = Color.white;

        //if (rotateProgress < 91 && rotateProgress > -91)
        //{
        //    GetComponent<MeshRenderer>().material.color = Color.red;
        //    transform.localPosition = Vector3.zero;
        //    transform.localEulerAngles = new Vector3(objectToRotate.transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z);
        //}
        //    float value = objectToRotate.transform.localEulerAngles.x;

        //    if (value >= 270)
        //        value -= 360;
        transform.localPosition = Vector3.zero;
        //transform.localEulerAngles = new Vector3(objectToRotate.transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z);
        float vectorToMove = Quaternion.Angle(transform.rotation, objectToRotate.transform.rotation);
        myBody.rotation = objectToRotate.transform.rotation;
        print(myBody.rotation);

        //myBody.rotation = Quaternion.Euler (myBody.rotation.eulerAngles.x, 0, 0);
        //myBody.rotation = new Quaternion(;

        rotateProgress = objectToRotate.transform.localEulerAngles.x;

    }
}
