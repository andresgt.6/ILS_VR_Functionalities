﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class ILSGrabbableUsable : ILSOculusGrabbable
{
    protected bool canUse;

    private event UnityAction TriggerVibration;

    [Space]
    [Header("Usable Settings")]
    [SerializeField] protected bool holdToUse = true;

    protected bool isUsing = false;

    //public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
    //{
    //    base.GrabBegin(hand, grabPoint);


    //    ILSOculusGrabber ILSGrabber = hand.GetComponent<ILSOculusGrabber>();

    //    if (ILSGrabber)
    //    {
    //        canUse = true;
    //        TriggerVibration += () => ILSGrabber.TriggerVibration(50, 2, 255);
    //        StartCoroutine(UseCheck(ILSGrabber.Controller));
    //    }
    //}

    //public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    //{
    //    base.GrabEnd(linearVelocity, angularVelocity);
    //    canUse = false;
    //    isUsing = false;

    //    TriggerVibration = null;
    //}

    //protected IEnumerator UseCheck(OVRInput.Controller controller)
    //{
    //    while(canUse)
    //    {
    //        if (holdToUse)
    //        {
    //            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, controller))
    //                StartUsing();

    //            if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, controller))
    //                EndUsing();
    //        }
    //        else
    //        {
    //            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, controller))
    //            {
    //                if (!isUsing)
    //                    StartUsing();
    //                else
    //                    EndUsing();
    //            }
    //        }

    //        yield return null;
    //    }
    //}

    protected virtual void StartUsing()
    {
        TriggerVibration?.Invoke();
        isUsing = true;
    }

    protected virtual void EndUsing()
    {
        isUsing = false;
    }


}
