﻿using UnityEngine;

public class SnapPlace : MonoBehaviour
{
    [SerializeField] protected Transform snapPoint = null;
    [SerializeField] protected bool startActive = false;

    [SerializeField] protected bool usableOnce = true;
    
    protected Collider sensor;
    protected bool isSnapActive;

    protected void Awake()
    {
        sensor = GetComponent<Collider>();

        if(sensor == null)
        {
            sensor = gameObject.AddComponent<SphereCollider>();
            (sensor as SphereCollider).radius = 0.1f;
        }

        sensor.isTrigger = true;

        if (snapPoint == null)
            snapPoint = transform.childCount > 0 ? transform.GetChild(0) : transform;

        SetActiveSnapPlace(startActive);
    }

    public virtual void SetActiveSnapPlace(bool active)
    {
        sensor.enabled = active;
        isSnapActive = active;
    }

    protected virtual bool ConditionFullfilled(SnapableObject objectToChek)
    {
        return true;
    }

	protected void OnTriggerEnter(Collider other)
	{
		SnapableObject snapable = other.GetComponent<SnapableObject>();

		if (snapable != null && ConditionFullfilled(snapable))
		{
			snapable.SetAttachProperties(true, snapPoint);

			//if (usableOnce)
			//SetActiveSnapPlace(false);
		}

	}
}
