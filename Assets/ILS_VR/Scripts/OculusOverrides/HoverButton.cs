﻿using UnityEngine;
using UnityEngine.Events;

public class HoverButton : MonoBehaviour
{
    public event UnityAction<bool> OnStateChanged;
    public event UnityAction<bool> OnHoverChanged;

    [SerializeField] protected Collider[] hoverColliders;

    public bool CanInteract { get; protected set; }
    private bool noLimitInteractions;
    public bool IsButtonActive { get; protected set; }

    private int remainingInteractions;

    public virtual void ActiveInteractions(int amount = 0)
    {
        noLimitInteractions = amount == 0;

        remainingInteractions = amount;
        CanInteract = true;

    }

    public virtual void DesactiveInteractions()
    {
        CanInteract = false;
        remainingInteractions = 0;
        noLimitInteractions = false;
    }

    public virtual void InteractWithButton(Side handSide = Side.RightSide)
    {
        if (!CanInteract) return;

        if (!IsButtonActive)
            ActiveButton();
        else
            DeactiveButton();

        OnStateChanged?.Invoke(IsButtonActive);

        if (!noLimitInteractions)
        {
            remainingInteractions--;

            if (remainingInteractions <= 0)
                DesactiveInteractions();
        }
    }

    protected virtual void ActiveButton()
    {
        IsButtonActive = true;

    }

    protected virtual void DeactiveButton()
    {
        IsButtonActive = false;
    }

    public void SetButtonActiveState(bool isActive)
    {
        if (isActive)
            ActiveButton();
        else
            DeactiveButton();
    }

    public void SetButtonHoverState(bool isOnHover)
    {
        OnHoverChanged?.Invoke(isOnHover);

        if (isOnHover)
            HoverStarted();
        else
            HoverEnded();
    }

    protected virtual void HoverStarted()
    {

    }

    protected virtual void HoverEnded()
    {

    }

    public Collider[] GetColliders()
    {
        if (hoverColliders.Length == 0)
        {
            Collider selfCollider = GetComponent<Collider>();

            if (selfCollider == null)
            {
                selfCollider = gameObject.AddComponent<BoxCollider>();
                (selfCollider as BoxCollider).size = Vector3.one * 0.1f;
            }

            hoverColliders = new Collider[1] { selfCollider };
        }

        return hoverColliders;
    }
}
