﻿using UnityEngine;
using System;
using UnityEngine.Events;

public class ILSOculusGrabbable //: OVRGrabbable
{
 //   public event UnityAction OnGrabBegin;
 //   public event UnityAction OnGrabEnd;

	//public OVRGrabber CurrentGrabber { get; private set; }

	//protected MeshRenderer objectRender;
 //   protected Color idleColor;
 //   protected Color hoverColor;


 //   protected virtual void Awake()
 //   {
 //       if (m_grabPoints.Length == 0)
 //       {
 //           // Get the collider from the grabbable
 //           Collider collider = this.GetComponent<Collider>();
 //           if (collider == null)
 //           {
 //               throw new ArgumentException("Grabbables cannot have zero grab points and no collider -- please add a grab point or collider.");
 //           }

 //           // Create a default grab point
 //           m_grabPoints = new Collider[1] { collider };
 //       }

 //       objectRender = GetComponent<MeshRenderer>();

 //       hoverColor = Color.white;
 //       idleColor = objectRender && objectRender.material.HasProperty("_InnerColor")? objectRender.material.GetColor("_InnerColor"): Color.gray;
        
 //   }

 //   protected void OnDestroy()
 //   {
 //       ForceRelease();
 //   }

 //   public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
 //   {
 //       base.GrabBegin(hand, grabPoint);

 //       SetHover(false);

	//	CurrentGrabber = hand;

	//	ILSOculusGrabber ILSGrabber = hand.GetComponent<ILSOculusGrabber>();
 //       ILSGrabber?.TriggerVibration(60, 1, 225);

 //       OnGrabBegin?.Invoke();
 //   }

 //   public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
 //   {     
 //       base.GrabEnd(linearVelocity, angularVelocity);
 //       GetComponent<Rigidbody>().useGravity = !m_grabbedKinematic;

	//	CurrentGrabber = null;

	//	OnGrabEnd?.Invoke();
 //   }

 //   public void SetActiveGrab(bool isTrue)
 //   {
 //       m_allowOffhandGrab = isTrue;
 //   }

 //   public virtual void SetHover(bool isHovering)
 //   {
 //       //objectRender?.material.SetColor("_InnerColor", isHovering ? hoverColor : idleColor);
 //   }

 //   public virtual void ForceRelease()
 //   {
 //       if (m_grabbedBy != null)
 //       {
 //           m_grabbedBy.ForceRelease(this);
 //       }
 //   }
}
