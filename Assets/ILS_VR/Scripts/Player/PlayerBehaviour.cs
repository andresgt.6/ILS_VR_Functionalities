﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerBehaviour : MonoBehaviour
{
	public event Action CompleteDescending;

	public enum State
	{
		Idle,
		Sit
	}

	public enum GlovesTypes
	{
		None,
		cotton,
		Latex,
		Leather
	}

	[Header("Hands Renderers")]
	[SerializeField] private SkinnedMeshRenderer[] noneGloves;
	[SerializeField] private SkinnedMeshRenderer[] cottonGloves;
	[SerializeField] private GlovesTypes currentGloves;

	[Header("Hands")]
	public HandController handLeftController;
	public HandController handRightController;

	public State currentState = State.Idle;
	public Transform[] interactionSpots;
	public Transform[] trackingPoints;

	[SerializeField] private Transform playerBody;

	private ModuleEntities entities;
	private int trackerBodyId;

	private Transform playersHead;
	private Transform lastParent;

	private void Awake()
	{
		for (int i = 0; i < trackingPoints.Length; i++)
		{
			trackingPoints[i].gameObject.SetActive(false);
		}
		SetGloves(currentGloves);
		playersHead = Camera.main.transform;		
	}

	public void Initialized(ModuleEntities entities)
	{
		this.entities = entities;
		handRightController.ChangeState += HandRightController_ChangeState;
		handLeftController.ChangeState += HandLeftController_ChangeState;
	}

	public void SetInParent(bool set, Transform parent)
	{
		if (set)
		{
			lastParent = transform.parent;
			Teleport(parent);
			transform.SetParent(parent);
		}
		else
		{
			transform.SetParent(lastParent);
		}
	}
	private void HandLeftController_ChangeState(HandController.State state)
	{
		noneGloves[0].enabled = false;
		cottonGloves[0].enabled = false;

		switch (currentGloves)
		{
			case GlovesTypes.None:
				noneGloves[0].enabled = state.Equals(HandController.State.Enabled);
				break;
			case GlovesTypes.cotton:
				cottonGloves[0].enabled = state.Equals(HandController.State.Enabled);
				break;
			default:
				break;
		}
	}

	private void HandRightController_ChangeState(HandController.State state)
	{
		noneGloves[1].enabled = false;
		cottonGloves[1].enabled = false;

		switch (currentGloves)
		{
			case GlovesTypes.None:
				noneGloves[1].enabled = state.Equals(HandController.State.Enabled);
				break;
			case GlovesTypes.cotton:
				cottonGloves[1].enabled = state.Equals(HandController.State.Enabled);
				break;
			default:
				break;
		}
	}

	public void SetGloves(GlovesTypes glovesToSet)
	{
		currentGloves = glovesToSet;

		for (int i = 0; i < 2; i++)
		{
			noneGloves[i].enabled = false;
			cottonGloves[i].enabled = false;
		}

		switch (glovesToSet)
		{
			case GlovesTypes.None:
				for (int i = 0; i < noneGloves.Length; i++)
				{
					noneGloves[i].enabled = true;
				}
				break;
			case GlovesTypes.cotton:
				for (int i = 0; i < cottonGloves.Length; i++)
				{
					cottonGloves[i].enabled = true;
				}
				break;
			default:
				break;
		}
	}
	public void SitInChair(bool seated)
	{
		if (seated)
		{
			currentState = State.Sit;
		}
		else
		{
			currentState = State.Idle;
		}
	}

	public void Teleport(int id)
	{
		transform.position = interactionSpots[id].position;
		transform.rotation = interactionSpots[id].rotation;
	}

	public void Teleport(Transform pointToTp)
	{
		transform.position = pointToTp.position;
		transform.rotation = pointToTp.rotation;
	}

	public void Teleport(Vector3 tpZone)
	{
		transform.position = tpZone;
	}
}

