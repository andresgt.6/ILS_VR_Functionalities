﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class LightSettings : MonoBehaviour
{
    [SerializeField] private Transform globalLightDirection;
    [SerializeField] private Color globalLightColor = new Color (0.6f,0.58f,0.56f,1);
    [SerializeField] private bool updateLighting = false;
    void Awake()
    {
        Shader.SetGlobalVector ("_lightPosition",globalLightDirection.forward);
        Shader.SetGlobalColor ("_lightColor",globalLightColor);
    }
    void Update()
    {
        if (updateLighting)
        {
        Shader.SetGlobalVector ("_lightPosition",globalLightDirection.forward);
        Shader.SetGlobalColor ("_lightColor",globalLightColor);
        }
    }
}
