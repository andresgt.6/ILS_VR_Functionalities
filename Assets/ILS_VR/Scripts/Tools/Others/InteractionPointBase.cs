﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractionPointBase : MonoBehaviour
{
    public event UnityAction OnInteraction;
    public bool disabledInteraction;    
    public virtual void Interaction()
    {
        if (disabledInteraction)
            return;

        OnInteraction?.Invoke();
    }
}
