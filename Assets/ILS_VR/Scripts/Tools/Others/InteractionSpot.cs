﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InteractionSpot : MonoBehaviour
{
    public Action OnInteractionTouch;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("InteractionPlace"))
        {
            OnInteractionTouch?.Invoke();
            other.GetComponent<InteractionPointBase>().Interaction();            
            print("Melo");
        }        
    }
}
