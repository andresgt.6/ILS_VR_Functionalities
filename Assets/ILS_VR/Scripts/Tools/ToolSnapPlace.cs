﻿using UnityEngine;

public class ToolSnapPlace : SnapPlace
{
    [SerializeField] private SnapableToolType toolType;

	protected override bool ConditionFullfilled(SnapableObject objectToChek)
	{
		base.ConditionFullfilled(objectToChek);

		SnapableTool snapableHand = objectToChek.GetComponent<SnapableTool>();

		if (snapableHand != null)
			return snapableHand.ToolType == toolType;
		else
			return false;

	}	
}
