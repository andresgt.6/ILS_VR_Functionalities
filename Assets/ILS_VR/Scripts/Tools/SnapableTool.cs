﻿using UnityEngine;

public class SnapableTool : SnapableObject
{
    [SerializeField] private SnapableToolType toolType;
    public SnapableToolType ToolType { get { return toolType; } }
}
