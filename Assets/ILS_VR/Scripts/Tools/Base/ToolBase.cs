﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ToolBase : MonoBehaviour
{
    public event Action<ToolStateInfo> ChangeState;

    [Header("State info")]
    public List<ToolStateInfo> allToolStates;
    [Space]
    public ToolStateInfo currentState;
    [SerializeField] protected int stateCount;

	[Header("Main components")]
	//[SerializeField] protected GameObject ghostTool;
	//[SerializeField] protected ToolSnapPlace snapPlace;



	protected IndicatorController indicator;    

    protected HighlightBase highlight;

    protected ToolSound toolSound;

    private Vector3 startLocalPosition;
    private Quaternion startLocaslRotation;
    private Transform startParent;
    private int startChildIndex;

    private Vector3 startPosition;

    protected virtual void Awake()
    {
        startLocalPosition = transform.localPosition;
        startLocaslRotation = transform.localRotation;
        startParent = transform.parent;
        startChildIndex = transform.GetSiblingIndex();
        startPosition = transform.position;

        SetState();
        highlight = GetComponent<HighlightBase>();
    }

    public void SetHighLight(bool enabled)
    {
        if (!highlight)
            return;

        if (enabled)
            highlight.SetEnabled();
        else
            highlight.SetDisabled();
    }
    private void Update()
    {
        if (transform.position.y < -1)
        {
            transform.position = startPosition;
        }
    }
    public void EnabledTool(IndicatorController indicator, TagInfo info = null,Vector3? customPlaceToAppear = null)
    {
        if(currentState.state == ToolStates.Disabled)
        {
            this.indicator = indicator;
            indicator.transform.SetParent(transform);
            if(customPlaceToAppear.HasValue)
                indicator.transform.localPosition = customPlaceToAppear.Value;
            else
                indicator.transform.localPosition = Vector3.zero;
            indicator.transform.localEulerAngles = Vector3.zero;
            if (info != null)
                print("Desde enable hay info");
            indicator.Enabled(info);
            AdvanceState();
        }
    }

	public void EnabledTool()
	{
		if (currentState.state == ToolStates.Disabled)
		{			
			AdvanceState();
		}
	}

	protected virtual void SetState()
    {
        if (allToolStates.Count > 0 && allToolStates.Count > stateCount)
        {
            currentState = allToolStates[stateCount];
            currentState.tool = this;
            ChangeState?.Invoke(currentState);
            BeginState();
        }
    }

    protected virtual void BeginState()
    {
        switch (currentState.state)
        {
            case ToolStates.Disabled:
                break;
            case ToolStates.Idle:                
                break;
            case ToolStates.Grabbed:
                break;
            case ToolStates.Opened:
                break;
            case ToolStates.InPosition:
                break;
            case ToolStates.Used:
                break;
            case ToolStates.Reset:
                break;
            default:
                break;
        }

        if (currentState.hover)
        {
            currentState.hover.ActiveInteractions(1);
            currentState.hover.OnStateChanged += OnHoverStateChange;
        }
        //if(currentState.grabbable)
        //{
        //    switch (currentState.state)
        //    {                
        //        case ToolStates.Idle:
        //            currentState.grabbable.OnGrabBegin += OnGrabBegin;
        //            break;
        //        case ToolStates.Grabbed:
        //        case ToolStates.Opened:
        //            currentState.grabbable.OnGrabEnd += OnGrabEnd;                    
        //            break;
        //    }            
        //}
        if(currentState.snapable)
        {
            currentState.snapable.OnAttachCompleted += Snapable_OnAttachCompleted;
        }
        if(currentState.interaction)
        {
            currentState.interaction.OnInteractionTouch += OnInteractionSpotTouched;
        }
    }

    private void OnInteractionSpotTouched()
    {
        currentState.interaction.OnInteractionTouch -= OnInteractionSpotTouched;
        AdvanceState();
    }

    private void Snapable_OnAttachCompleted(SnapableObject obj)
    {
        currentState.snapable.OnAttachCompleted -= Snapable_OnAttachCompleted;
        AdvanceState();
    }

    protected virtual void OnGrabBegin()
    {
        //currentState.grabbable.OnGrabBegin -= OnGrabBegin;
        AdvanceState();
    }

    protected virtual void OnGrabEnd()
    {
        //currentState.grabbable.OnGrabEnd -= OnGrabEnd;
        AdvanceState();
    }

    protected virtual void OnHoverStateChange(bool enabled)
    {
        currentState.hover.OnStateChanged -= OnHoverStateChange;
        AdvanceState();
    }

    public virtual void AdvanceState(ToolStates customState)
    {
        if (customState == currentState.state)
            return;

        int? stepID = null;
        for (int i = 0; i < allToolStates.Count; i++)
        {
            if (allToolStates[i].state == customState)
            {
                stepID = i;
                break;
            }
        }

        if (stepID.HasValue)
        {
            stateCount = stepID.Value;
            SetState();
        }
        else
        {
            Debug.LogError("No se encontró este paso");
        }
    }

    public virtual void AdvanceState()
    {
        stateCount++;
        SetState();
    }

    public virtual void Restart()
    {
        gameObject.SetActive(true);

        if (currentState.state == ToolStates.Disabled)
            return;

        if(TryGetComponent(out HighlightShader shader))
        {
            shader.SetDisabled();
        }

        stateCount = 0;
        SetState();

        transform.SetParent(startParent);
        transform.SetSiblingIndex(startChildIndex);
        transform.localPosition = startLocalPosition;
        transform.localRotation = startLocaslRotation;
    }

	public void RestartPos()
	{
		gameObject.SetActive(true);

		if (currentState.state == ToolStates.Disabled)
			return;

		if (TryGetComponent(out HighlightShader shader))
		{
			shader.SetDisabled();
		}

		transform.SetParent(startParent);
		transform.SetSiblingIndex(startChildIndex);
		transform.localPosition = startLocalPosition;
		transform.localRotation = startLocaslRotation;
	}
}
