﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ToolStates
{
    Disabled,
    Idle,
    Grabbed,
    Opened,
    InPosition,
    Used,
    Reset
}

[System.Serializable]
public class ToolStateInfo
{
    public ToolStates state;
    //public Vector3 localPositionInState = Vector3.zero;
    //public Vector3 localRotationInState = Vector3.zero;
    public ILSOculusGrabbable grabbable;
    public HoverButton hover = null;
    public SnapableTool snapable = null;
    public InteractionSpot interaction = null;
    [HideInInspector] public ToolBase tool;
}
