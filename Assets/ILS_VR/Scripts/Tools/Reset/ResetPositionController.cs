﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ResetPositionController : MonoBehaviour
{
    private ToolBase tool;

    private void Awake()
    {
        tool = GetComponent<ToolBase>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.CompareTag("Floor"))
        {
            RestartPosition();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.CompareTag("Floor"))
        {
            StopAllCoroutines();
        }
    }

    public void RestartPosition()
    {
        StartCoroutine(MoveToInitialPoticion());
    }

    IEnumerator MoveToInitialPoticion()
    {
        yield return new WaitForSeconds(2f);
        tool.RestartPos();
    }
}
