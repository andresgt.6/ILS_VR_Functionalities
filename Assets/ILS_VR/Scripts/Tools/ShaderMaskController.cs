﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class ShaderMaskController : MonoBehaviour
{
    [SerializeField] private Color maskColor = new Color(0, 0.36f, 1, 1);
    [SerializeField] private bool isHighLighted;
    [SerializeField] private bool isMasked;
    private Transform maskPosition;
    [SerializeField] private float maskRadius = 0.8f;
    [SerializeField] private float maskFalloff = 20;
    [SerializeField] private float maskHeight = 1.5f;
    [SerializeField] [Range (-1,1)] private float temperature = 0;
    [SerializeField] [Range (0, 2)] private float ambientLights = 1.0f;
    void Awake()
    {
        if (maskPosition == null) maskPosition = this.transform;
    }
    void Update()
    {
        if (isMasked)
        {
            Shader.SetGlobalFloat("_isMasked", 1);
            Shader.SetGlobalFloat("_temperature", temperature);
            Shader.SetGlobalFloat("_maskHeight", maskHeight);
            Shader.SetGlobalFloat("_intensity", ambientLights);
        }
        else
        {
            Shader.SetGlobalFloat("_temperature", 0);
            Shader.SetGlobalFloat("_isMasked", 0);
        }
        if (isHighLighted)
        {
            Shader.SetGlobalFloat("_isHighlighted", 1);
            Shader.SetGlobalColor("_maskColor", maskColor);
            Shader.SetGlobalVector("_maskPosition", maskPosition.position);
            Shader.SetGlobalFloat("_maskHeight", maskHeight);
            Shader.SetGlobalFloat("_maskRadius", maskRadius);
            Shader.SetGlobalFloat("_maskFalloff", maskFalloff);
        }
        else
            Shader.SetGlobalFloat("_isHighlighted", 0);
    }
}