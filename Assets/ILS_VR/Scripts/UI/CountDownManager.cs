﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class CountDownManager : MonoBehaviour
{
    public static Action<bool, Action> SetCountDown;

    [SerializeField] private CanvasGroup[] countDownPanels;

    private Action CompleteCountDown;

    private Coroutine countDownCoroutine;

    private void Awake()
    {
        SetCountDown += OnSetCountDown;
    }

    private void OnSetCountDown(bool enable, Action OnCompleteCallback = null)
    {        
        if (enable)
        {
            CompleteCountDown = OnCompleteCallback;
            countDownCoroutine = StartCoroutine(CountDown());
        }
        else
        {
            if(countDownCoroutine != null)
            {
                StopCoroutine(countDownCoroutine);
                countDownCoroutine = null;
            }
            DisableAllPanels();
        }
    }

    private void DisableAllPanels()
    {
        for (int i = 1; i < countDownPanels.Length; i++)
        {            
            countDownPanels[i].alpha = 0;                        
        }
    }

    private IEnumerator CountDown()
    {
        countDownPanels[0].alpha = 1;
        yield return new WaitForSeconds(1);

        for (int i = 1; i < countDownPanels.Length; i++)
        {
            LeanTween.alphaCanvas(countDownPanels[i - 1], 0, .3f);
            countDownPanels[i].alpha = 0;
            LeanTween.alphaCanvas(countDownPanels[i], 1, .7f);
            yield return new WaitForSeconds(1);
        }
        CompleteCountDown?.Invoke();
        yield return new WaitForSeconds(1);
        DisableAllPanels();
    }


    private void OnDestroy()
    {
        SetCountDown -= OnSetCountDown;
    }
}
