﻿using System;

public class GemsLookTarget : RaycastTargetBase
{
    public event Action<bool> OnLookStateChange;

    public override void LookAtMe(bool look, RaycastType lastLooking)
    {
        if(lastLooking == RaycastType.CenterEye)
            base.LookAtMe(look, lastLooking);
    }

    protected override void OnLookingAtMe()
    {
        base.OnLookingAtMe();

        OnLookStateChange?.Invoke(true);
    }

    protected override void OnStopLookingAtMe()
    {
        base.OnStopLookingAtMe();

        OnLookStateChange?.Invoke(false);
    }
}
