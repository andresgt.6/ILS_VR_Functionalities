﻿using UnityEngine;
using UnityEngine.UI;

public class GemsUIController : UIPanelBase
{
    [SerializeField] private RectTransform gemsContainer = null;

    private Image[] gems;

    protected override void Awake()
    {
        base.Awake();
        gems = gemsContainer.GetComponentsInChildren<Image>(true);
        SetGemsState(0);
    }

    public void SetGemsState(int currentState)
    {
        if(gems != null && gems.Length > 0)
            for (int i = 0; i < gems.Length; i++)
                gems[i].enabled = i < currentState;
    }
}
