﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUIController : MonoBehaviour
{
    [SerializeField] private Text[] scoreTexts;
    [SerializeField] private Text scoreFinal;

    private Canvas canvas;

    private void Awake()
    {
        canvas = GetComponent<Canvas>();
        SetEnabled(false);
    }

    public void SetEnabled(bool enabled, float[] scores = null)
    {
        canvas.enabled = enabled;
        if(enabled)
        {
            float scoreTotal = 0;
            for (int i = 1; i < scores.Length; i++)
            {
                scoreTexts[i-1].text = string.Format("{0}", scores[i]);
                scoreTotal += scores[i];
            }
            scoreFinal.text = scoreTotal.ToString();            
        }        
    }
}
