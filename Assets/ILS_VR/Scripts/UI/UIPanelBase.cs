﻿using UnityEngine;
using System;

public class UIPanelBase : MonoBehaviour
{
    public event Action OnCompleteShow;
    public event Action OnCompleteHide;

    [SerializeField] protected LeanTweenType tweenType = LeanTweenType.easeInOutQuad;

    protected Canvas uiCanvas;
    protected CanvasGroup uiGroup;

    protected readonly Vector3 ONE = Vector3.one;

    private float currentFactor = 0;

    protected virtual void Awake()
    {
        uiCanvas = GetComponentInChildren<Canvas>();
        uiGroup = GetComponentInChildren<CanvasGroup>();

        uiCanvas.enabled = false;
        SetScaleAndOpacity(0);
    }

    public void ShowUI()
    {
        if (uiCanvas.worldCamera == null)
            uiCanvas.worldCamera = Camera.main;

        uiCanvas.enabled = true;
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, SetScaleAndOpacity, currentFactor, 1, 1).setEase(tweenType).setOnComplete(ShowCompleted);
    }

    protected virtual void ShowCompleted()
    {
        OnCompleteShow?.Invoke();
    }

    public void CloseUI()
    {
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, SetScaleAndOpacity, currentFactor, 0, 1).setEase(tweenType).setOnComplete(CloseCompleted);
    }

    protected virtual void CloseCompleted()
    {
        uiCanvas.enabled = false;
        OnCompleteHide?.Invoke();
    }

    protected void SetScaleAndOpacity(float factor)
    {
        transform.localScale = ONE * factor;
        uiGroup.alpha = factor;
        currentFactor = factor;
    }
}
