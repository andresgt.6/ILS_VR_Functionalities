﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MobileUISettings
{
    public Vector3 customLocalPos = Vector3.zero;    
    public float transitionTime;
    public UnityAction OnCompleteCallback;
    public int? startInCustomId;
}
