﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MobileUIController : MonoBehaviour
{
    /// <summary>
    /// Transform:Padre, CustomClass:Ajustes adicionales, Bool:¿Activar?, Enum: Que transición se va a hacer
    /// </summary>
    public static UnityAction<Transform, MobileUISettings, bool, TransitionsUIMobile> SetUIMobile;

    [Space, Header("Screen")]
    [SerializeField] private Sprite[] tvSprites;

    [Space, Header("Settings")]
    [SerializeField] private MobileUISettings defaultSettings;
    [SerializeField] private MobileUISpritesTransition[] transitions;

    private Canvas screen;

    private MobileUISettings currentSettings;
    private MobileUISpritesTransition currentTransition;

    private Image tvImage;
    private int currentImageId;

    private Coroutine screenTransitionCoroutine;

    private void Awake()
    {
        SetUIMobile += OnSetUIMobile;

        screen = GetComponentInChildren<Canvas>();
        tvImage = GetComponentInChildren<Image>();
    }

    private void OnSetUIMobile(Transform parent, MobileUISettings customSettings, bool enable, TransitionsUIMobile transition)
    {
        screen.enabled = enable;
        if (parent)
            transform.SetParent(parent);

        if (customSettings == null)
            currentSettings = defaultSettings;
        else
            currentSettings = customSettings;

        for (int i = 0; i < transitions.Length; i++)
        {
            if (transitions[i].transitionToSet == transition)
            {
                currentTransition = transitions[i];
            }
        }

        transform.localPosition = currentSettings.customLocalPos;

        if(currentSettings.startInCustomId.HasValue)
        {
            currentImageId = currentSettings.startInCustomId.Value;
        }

        if (screenTransitionCoroutine != null)
        {
            currentImageId = 0;
            StopCoroutine(screenTransitionCoroutine);
        }

        screenTransitionCoroutine = StartCoroutine(SetScreenTransition());
    }

    private IEnumerator SetScreenTransition()
    {
        while (currentImageId <= currentTransition.spriteIds.Length)
        {
            if(tvSprites.Length <= currentImageId)
            {                
                break;
            }

            tvImage.sprite = tvSprites[currentTransition.spriteIds[currentImageId]];
            if(currentTransition.customDurations.Length > 0 && currentTransition.customDurations.Length > currentImageId)
                yield return new WaitForSeconds(currentTransition.customDurations[currentImageId]);
            else
                yield return new WaitForSeconds(currentSettings.transitionTime);
            currentImageId++;
        }

        CompleteScreenTransition();
    }

    private void CompleteScreenTransition()
    {
        currentSettings.OnCompleteCallback?.Invoke();
        currentImageId = 0;
        //print("Completó");
    }

    public void SetDefault()
    {
        screen.enabled = false;
    }

    private void OnDestroy()
    {
        SetUIMobile -= OnSetUIMobile;
    }
}
