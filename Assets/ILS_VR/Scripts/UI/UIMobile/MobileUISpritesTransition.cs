﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MobileUISpritesTransition
{
    [Header("Type")]    
    public TransitionsUIMobile transitionToSet;

    [Header("Sprites Ids")]
    [Tooltip("Se colocan los identificadores de los sprites que van a aparecer en esta transición")]
    public int[] spriteIds;

    [Header("timers")]
    [Tooltip("Duración custom entre cada pantalla, deje así si quiere la de por defecto")]
    public float[] customDurations;
}

