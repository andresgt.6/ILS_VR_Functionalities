﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class UILogo : MonoBehaviour
{
    [SerializeField] private Logos logo;

    [SerializeField] Sprite spriteAc;
    [SerializeField] Sprite spritePositiva;

    [Tooltip("Elemento 0: Ac, Elemento 1: Positiva")]
    [SerializeField] Image[] images;

#if UNITY_EDITOR
    void Update()
    {
        //I made a switch because it will be more logos
        switch (logo)
        {
            case Logos.AmbienteConsultores:
                for (int i = 0; i < images.Length; i++)
                {
                    images[i].sprite = spriteAc;
                }
                break;
            case Logos.Positiva:
                for (int i = 0; i < images.Length; i++)
                {
                    images[i].sprite = spritePositiva;
                }
                break;
            default:
                break;
        }
    }
#endif
}

public enum Logos
{
    AmbienteConsultores,
    Positiva
}
