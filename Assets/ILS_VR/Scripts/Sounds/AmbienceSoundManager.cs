﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceSoundManager : MonoBehaviour
{
    [SerializeField] private int minimunTimeToNextSound;
    [SerializeField] private int maximumTimeToNextSound;

    private Places currentPlace;

    private int currentSoundId;

    public void SetAmbientSound(Places currentPlace)
    {
        this.currentPlace = currentPlace;

        StopAmbienceSound();

        SoundManager.StopSoundByName(AmbienceSounds.Workers_Effect_Ambience.ToString(), true);
        SoundManager.StopSoundByName(AmbienceSounds.Mine_Ambience.ToString(), true);

        switch (this.currentPlace)
        {
            case Places.None:
            case Places.Empty:
                break;

            case Places.Musseum:
                break;

            case Places.MineOutside:
                break;

            case Places.Module2:
            case Places.Module5:
            case Places.GoldenRule:
                SoundManager.PlaySoundByName(AmbienceSounds.Mine_Ambience.ToString(), true);
                break;

            case Places.DressingRoom:
            case Places.Warehouse:
                SoundManager.PlaySoundByName(AmbienceSounds.Workers_Effect_Ambience.ToString(), true);
                break;

            default:
                break;
        }

        PlayAmbienceSound();
    }

    public void PlayAmbienceSound()
    {
        AmbienceSounds choosedSound = ChooseSound();
        SoundManager.PlaySoundByName(choosedSound.ToString());

        AudioSource currentAc = SoundManager.GetAudioSourceByName(choosedSound.ToString());

        if (currentAc != null)
            LeanTween.delayedCall(currentAc.clip.length, NextAmbientSound);
    }

    public void StopAmbienceSound()
    {
        if (LeanTween.isTweening(gameObject))
            LeanTween.cancel(gameObject);

        SoundManager.StopSoundByName(string.Format("{0}", (AmbienceSounds)currentSoundId), false);
    }

    private void NextAmbientSound()
    {
        int timeToNextSound;
        timeToNextSound = Random.Range(minimunTimeToNextSound, maximumTimeToNextSound + 1);

        LeanTween.delayedCall(timeToNextSound, () => PlayAmbienceSound());
    }

    public AmbienceSounds ChooseSound()
    {
        int soundId = 0;

        switch (currentPlace)
        {
            case Places.None:
            case Places.Empty:
                break;

            case Places.Musseum:
                break;

            case Places.MineOutside:
                break;

            case Places.Module2:
            case Places.Module5:
            case Places.GoldenRule:
                soundId = Random.Range(2, 8);
                break;

            case Places.DressingRoom:
            case Places.Warehouse:
                break;

            default:
                break;
        }

        currentSoundId = soundId;
        AmbienceSounds currentSound = (AmbienceSounds)soundId;

        return currentSound;
    }
}

