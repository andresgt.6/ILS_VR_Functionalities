﻿using UnityEngine;

[System.Serializable]
public class ModuleEntities 
{    
    public ILSOculusBlinker blinker;
    public PlayerBehaviour player;
    public TaskManager taskManager;
    public PersonBase maleTrainer;
    public GestureManager gestureManager;
    public MobileModules startModuleUI;
    public IndicatorController tagIndicator;
    public DialogueManager dialogueManager;
    public Canvas repeatCanvas;
    public TimelineController esmeraldGem;
    //public GemsUIManager GemUI;
    public NextModule nextModuleButton;
}
