﻿using System;

[Serializable]
public class ModuleSettings
{
    public enum State
    {
        Disable,
        Enabled,
        Completed
    }
    public State currentState;
    public ModuleType gameplayStep;
    public float currentScore;
}
