﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ModuleBase[] modules = null;

    [SerializeField] private ModuleEntities entities = new ModuleEntities();

    [Space]
    [Header("Steps")]    
    [SerializeField] private ModuleSettings[] moduleSettings = new ModuleSettings[0];
    [SerializeField] private ModuleSettings currentModule = new ModuleSettings();

	[Space]
	[Header("¿Did You Know?")]
	[SerializeField] private DidYouKnowManager didYouKnowManager;

	[Space]
    [Header("Settings")]
    [SerializeField] private bool showPlayerResults = true;
    [SerializeField] private bool login = true;
    [SerializeField] private float experienceTime;
    [SerializeField] private float timeToReloadExperience;
    [SerializeField] private string sceneNameToLoad;

    [Space]
    [Header("Others")]
    [SerializeField] private LoginManager loginManager;
    [SerializeField] private UIInformative uiInformative;
	[SerializeField] private GameObject tpEffect;
    [SerializeField] private GameObject finalScreen;

    [Header("Final")]
    [SerializeField] private UICalification calification;
    [SerializeField] private ScoreUIController scoreUI;

    private readonly DataBase.DataBaseManager DataBaseManager = new DataBase.DataBaseManager();

    private ModuleBase currentModuleBase;

    private LoadSceneManager loadManager;    
    
    private int currentModuleStep = 0;

    private void Start()
    {
        loginManager.gameObject.SetActive(false);
        entities.blinker.SetBlinkerFullEffect(true);
        StartGameplay();
        entities.player.Initialized(entities);

        loadManager = GetComponentInChildren<LoadSceneManager>();

        if(didYouKnowManager)
		    SelectDidYouKnow();

		if (TimerManager.Instance != null)
        {
            TimerManager.Instance.SetTimerLength(experienceTime);
            TimerManager.Instance.OnCompleteTimer += Instance_OnCompleteTimer;
            TimerManager.Instance.OnUpdateTimer += Instance_OnUpdateTimer;

            entities.startModuleUI.SetTimer(TimerManager.Instance.CurrentTime, experienceTime);
            if (TimerManager.Instance.timerState == TimerManager.TimerState.Paused)
                entities.startModuleUI.OnCompleteWelcome += OnCompleteIntro;
        }
    }

	private void SelectDidYouKnow()
	{
		List<int> random = new List<int>();
		random = didYouKnowManager.GenerateRandom();

		for (int i = 0; i < random.Count; i++)
		{
			modules[random[i]].SetDidYouKnow();
		}
	}

    private void OnCompleteIntro()
    {
        entities.startModuleUI.OnCompleteWelcome -= OnCompleteIntro;
        TimerManager.Instance.StartTimer();
    }

    #region Timer
    private void Instance_OnUpdateTimer(float arg0)
    {
        entities.startModuleUI.SetTimer(arg0, experienceTime);
    }

    private void Instance_OnCompleteTimer()
    {
        TimerManager.Instance.OnCompleteTimer -= Instance_OnCompleteTimer;
        FinishExperience();
    }
    #endregion

    private void StartGameplay()
    {
		tpEffect.SetActive(true);
		LoadScene();
    }

	private void LoadScene()
	{
		entities.blinker.HalfBlinkFadeIn(() => { loadManager.LoadAsyncScene(Places.Empty, ShowIntro); });
	}
    private void ShowIntro()
    {
        entities.blinker.HalfBlinkFadeOut();
        if(login)
            Login();
        else
            SelectNextStep();

        LeanTween.value(gameObject, value => Shader.SetGlobalFloat("_animationTime", value), 0, 1, 2);
		LeanTween.delayedCall(2, () => { tpEffect.SetActive(false); }); 
	}

    private void Login()
    {
        loginManager.gameObject.SetActive(true);
        loginManager.CompleteLogin += OnCompleteLogin;
    }

    private void OnCompleteLogin()
    {
        loginManager.gameObject.SetActive(false);
        loginManager.CompleteLogin -= OnCompleteLogin;
        SelectNextStep();
    }

    private void SelectNextStep()
    {
        if (currentModuleStep < moduleSettings.Length)
        {            
            if ((int)currentModule.gameplayStep > 5)
                ModuleSelectionWindow();
            else
                SearchForModule(moduleSettings[currentModuleStep]);
        }
        else
        {

            SendPlayerData();
        }
    }

    /// <summary>
    /// Module Selection Window Appears and the player must choose a module
    /// </summary>
    private void ModuleSelectionWindow()
    {        
        List<ModuleSettings> modulesToSend = new List<ModuleSettings>();
        for (int i = 0; i < moduleSettings.Length; i++)
        {
            if((int)moduleSettings[i].gameplayStep > 2)
                modulesToSend.Add(moduleSettings[i]);
        }
    }
    

    private void SearchForModule(ModuleSettings newModule)
    {
        for (int i = 0; i < modules.Length; i++)
        {
            if (modules[i].TypeOfModule == newModule.gameplayStep)
            {
                ActiveModule(modules[i]);
                currentModuleBase = modules[i];
                currentModule = moduleSettings[currentModuleStep];
                currentModule.currentState = ModuleSettings.State.Enabled;
                break;
            }
        }
    }

    private void ActiveModule(ModuleBase _module)
    {
        _module.InitializeModule(entities);
        _module.OnModuleCompleted += ModuleCompleted;
        _module.ChangePlace += OnChangePlace;
        _module.StartModule();
    }

    private void OnChangePlace(Places selectedPlace)
    {
        entities.blinker.HalfBlinkFadeIn(() => 
        {
            //modules[currentModuleStep].ChangePlace -= OnChangePlace;
            loadManager.LoadAsyncScene(selectedPlace, CompleteSceneLoading);
			modules[currentModuleStep].OnLoadingScene();
		});        
    }

    private void CompleteSceneLoading()
    {
        entities.blinker.HalfBlinkFadeOut(() =>
        {
            if(modules[currentModuleStep].TypeOfModule.Equals(ModuleType.Module0))
                modules[currentModuleStep].CompleteStep();
        });
    }

    private void ModuleCompleted(ModuleBase _module)
    {
		if(currentModuleBase.TypeOfModule != ModuleType.Module0)
			currentModule.currentScore = currentModuleBase.temporalScore;

        modules[currentModuleStep].ChangePlace -= OnChangePlace;
		_module.OnModuleCompleted -= ModuleCompleted;

        switch (_module.ModuleState)
        {
            case ModuleStates.Normal:
                print("Complete " + currentModule.gameplayStep);
                if (currentModule != null)
                {
                    currentModuleStep++;                    
                    currentModule.currentState = ModuleSettings.State.Completed;
                }

                SelectNextStep();
                break;
            case ModuleStates.Failed:
				RepeatModule();
				break;
            default:
                break;
        }
    }

    private void RepeatModule()
    {        
        currentModuleStep--;
        SelectNextStep();
    }

    private void SendPlayerData()
    {
        List<ActivityStep> stepsToSend = new List<ActivityStep>();

        for (int i = 0; i < modules.Length; i++)
        {
            for (int j = 0; j < modules[i].stepsToSend.Count; j++)
            {
                stepsToSend.Add(modules[i].stepsToSend[j]);
            }
        }

        ActivityStep[] steps = stepsToSend.ToArray();
        SessionResults sessionResults = new SessionResults(steps);

        DataBaseManager.Score(sessionResults);
        DataBaseManager.DatabaseAnswer += OnDatabaseAnswer;

        LeanTween.delayedCall(2, () => 
        {
            if (showPlayerResults)
                ShowResults();
            else
                EndGameplay();
        });
    }

    private void OnDatabaseAnswer(int answer)
    {
        DataBaseManager.DatabaseAnswer -= OnDatabaseAnswer;
        print("Database answer:" + answer);
    }

    private void OnDatabaseAnswerSurvey(int answer)
    {
        DataBaseManager.DatabaseAnswer -= OnDatabaseAnswerSurvey;
        print("Database answer survey:" + answer);
    }

    private void ShowResults()
    {
        float[] scores = new float[moduleSettings.Length];

        for (int i = 1; i < scores.Length; i++)
        {
            scores[i] = moduleSettings[i].currentScore;
        }

        scoreUI.SetEnabled(true, scores);

        LeanTween.delayedCall(5, () => { scoreUI.SetEnabled(false, null); EndGameplay(); });
    }

    private void EndGameplay()
    {
        #region Old finish
        //if (sceneNameToLoad != null || sceneNameToLoad != "")
        //{
        //    entities.blinker.HalfBlinkFadeIn(() => { SceneManager.LoadScene(sceneNameToLoad); });
        //}
        //else
        //{
        //    FinishExperience();
        //} 
        #endregion

        Dialogue[] dialogues = new Dialogue[3];
        dialogues[0] = new Dialogue(161, 2);
        dialogues[1] = new Dialogue(161, 2, 1);
        dialogues[2] = new Dialogue(162, 2);

        finalScreen.SetActive(true);
        SoundManager.PlaySoundByName("Aplause");

        float speechDuration = entities.dialogueManager.GetDialogueDuration(dialogues);
        entities.dialogueManager.PlayDialogue(dialogues);

        LeanTween.delayedCall(speechDuration, () => { calification.SetEnabled(true, CompleteCalification); });        
    }

    private void CompleteCalification()
    {
        calification.SetEnabled(false);
        finalScreen.SetActive(false);

        DataBaseManager.SendCalification(new Survey(calification.CalificationNumber));
        DataBaseManager.DatabaseAnswer += OnDatabaseAnswerSurvey;

        Dialogue[] dialogues = new Dialogue[2];
        dialogues[0] = new Dialogue(163, 2);
        dialogues[1] = new Dialogue(164, 2);

        float speechDuration = entities.dialogueManager.GetDialogueDuration(dialogues) + dialogues.Length;
        entities.dialogueManager.PlayDialogue(dialogues);

        LeanTween.delayedCall(speechDuration, () => 
        {
            NPCActionSettings femaleTrainerSettings = new NPCActionSettings();
            femaleTrainerSettings.delayToStart = 0;
            femaleTrainerSettings.duration = null;
            femaleTrainerSettings.action = AIActions.Goodbye;
            femaleTrainerSettings.objectToInteract = entities.maleTrainer.transform;
            entities.maleTrainer.SetAction(femaleTrainerSettings);
        });        

        Invoke("FinishExperience",5);
    }

    private void FinishExperience()
    {
        //Debug.Log("GAMEPLAY ENDED");
        //finalScreen.SetActiveScreen(true);
        //finalScreen.SetFillAmount(timeToReloadExperience);
        //LeanTween.delayedCall(timeToReloadExperience + 1, ReloadExperience);
        entities.blinker.HalfBlinkFadeIn();
    }

    private void ReloadExperience()
    {
        //finalScreen.SetActiveScreen(false);
        entities.blinker.HalfBlinkFadeIn(() => { SceneManager.LoadScene("Login"); });
    }

}
