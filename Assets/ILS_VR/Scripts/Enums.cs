﻿public enum TransitionsUIMobile
{
    TestingTransition,
    TestingTrandition2
}

public enum HarnessTool
{
    ChestCarabiner,
    RightCarabiner,
    LeftCarabiner,
    ArestingBrake,
    Descendant,
    Carabiner
}

public enum Tasks
{
    Delay,
    Timeline,
    Speech,
    WearTools,
    HoverButtons,
    PersonCompleteTask,
    SetTool,
    Gesture
}

public enum ModuleType
{
    None,
    Module0,
    Module1,
    Module2,
    Module3,
    Module4,
    Module5,
    Module6,
    SubModule1
}

public enum WearableType
{
    Helmet,
    Harness,
    Gloves,
    Others
}

public enum AIActions
{
    Walk,
    Talk,
    Teleport,
    Rotate,
    ExplainObject,
    Ok_Gesture,
    Goodbye,
	Bend,
	LiftUp
}

public enum SnapableToolType
{
    Test,
    Pencil,
	Paper,
	AditionalItems,
	Mouse,
	Keyboard,
	Printer,
	Telephone,
	Monitor,
	Box,
	Memo,
	Paper0,
	Paper1,
	Paper2,
    Peak,
    Shovel,
    PneumaticHammer
}

public enum ToolTipType
{
    Trigger,
    Tutorial,
    Text
}

public enum UI_Quiz
{
    Idle,
    Correct,
    Incorrect,
    Win,
    Lose
}

public enum ToolAudioTexture
{
    Metal,
    Cloth,
    Combined
}

public enum ToolAudioType
{
    Grab, 
    Drop
}

public enum Side
{
    LeftSide,
    RightSide
}

public enum ButtonsType
{
    Place,
    PcType,
    HomeWorkPlace
}

public enum Places
{
    None,
    Empty,
    Musseum,
    MineOutside,
	Module2,
	Module5,
	DressingRoom,
	Warehouse,
    GoldenRule
}

public enum AmbienceSounds
{
    None,
    Mine_Ambience,
    Rocks_Effect_Ambience,
    Whistle_Effect_Ambience_1,
    Whistle_Effect_Ambience_2,
    Whistle_Effect_Ambience_3,
    Whistle_Effect_Ambience_4,
    Hammer_Effect_Ambience,
    Blower_Effect_Ambience,
    Workers_Effect_Ambience
}

public enum ComputerType
{
    None,
    Deskop,
    Laptops
}

public enum MineItem
{
	Helmet,
	Gloves,
	Boots,
	Workwear,
	HearingProtection,
	SafetyGlasses,
	RespiratoryProtection,
	SelfRescuer,
	Lamp,
	Pick,
	Hammer,
	Shovel
}

public enum ItemState
{
	Broken,
	Good
}

public enum TrainerElementType
{
    Helmet,
    Bag,
    Gloves,
    SelfRescuer,
    Mask,
    GasDetector,
    Headphones,
    EyesProtection,
    BeltGlasses
}