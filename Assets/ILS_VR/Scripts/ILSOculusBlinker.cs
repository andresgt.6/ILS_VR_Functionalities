﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class ILSOculusBlinker : MonoBehaviour
{
    //[SerializeField] private OVRScreenFade fader = null;
    /// <summary>
	/// How long the transition takes. Usually this is greater than Teleport Delay.
	/// </summary>
	[Tooltip("How long the transition takes. Usually this is greater than Teleport Delay.")]
    [Range(0.01f, 2.0f)]
    public float TransitionDuration = 1f;

    /// <summary>
    /// At what percentage of the elapsed transition time does the action occur?
    /// </summary>
    [Tooltip("At what percentage of the elapsed transition time does the action occur?")]
    [Range(0.0f, 1.0f)]
    public float actionDelay = 0.5f;

    /// <summary>
    /// Fade to black over the duration of the transition.
    /// </summary>
    [Tooltip("Fade to black over the duration of the transition")]
    public AnimationCurve FadeLevels = new AnimationCurve(new Keyframe[3] { new Keyframe(0, 0), new Keyframe(0.5f, 1.0f), new Keyframe(1.0f, 0.0f) });

    
    public void SetBlinkerFullEffect(bool fullBlack)
    {
        //fader.SetFadeLevel(fullBlack ? 0 : 1);
    }

    public void FullBlink(UnityAction _OnHalfBlink = null, UnityAction OnCompleteFade = null)
    {
        StartCoroutine(FullBlinkCoroutine(_OnHalfBlink, OnCompleteFade));
    }


    private IEnumerator FullBlinkCoroutine(UnityAction _OnHalfBlink, UnityAction OnCompleteFade)
    {
        float elapsedTime = 0;
        float halfBlinkTime = TransitionDuration * actionDelay;
        bool halfBlinkActionDone = false;

        while (elapsedTime < TransitionDuration)
        {
            yield return null;
            elapsedTime += Time.deltaTime;

            if (!halfBlinkActionDone && elapsedTime >= halfBlinkTime)
            {
                halfBlinkActionDone = true;
                _OnHalfBlink?.Invoke();
            }

            float fadeLevel = FadeLevels.Evaluate(elapsedTime / TransitionDuration);
            //fader.SetFadeLevel(fadeLevel);
        }

        //fader.SetFadeLevel(0);

        if (OnCompleteFade != null)
            OnCompleteFade();
    }


    public void HalfBlinkFadeIn(UnityAction OnCompleteFade = null)
    {
        StartCoroutine(HalfBlinkFadeInCoroutine(OnCompleteFade));
    }

    private IEnumerator HalfBlinkFadeInCoroutine(UnityAction OnCompleteFade)
    {
        float elapsedTime = 0;
        float halfBlinkTime = TransitionDuration * actionDelay;

        while (elapsedTime < halfBlinkTime)
        {
            yield return null;
            elapsedTime += Time.deltaTime;

            float fadeLevel = FadeLevels.Evaluate(elapsedTime / TransitionDuration);
            //fader.SetFadeLevel(fadeLevel);
        }

        //fader.SetFadeLevel(1);

        if (OnCompleteFade != null)
            OnCompleteFade();
    }

    public void HalfBlinkFadeOut(UnityAction OnCompleteFade = null)
    {
        StartCoroutine(HalfBlinkFadeOutCoroutine(OnCompleteFade));
    }

    private IEnumerator HalfBlinkFadeOutCoroutine(UnityAction OnCompleteFade)
    {
        float elapsedTime = TransitionDuration * actionDelay;

        while (elapsedTime < TransitionDuration)
        {
            yield return null;
            elapsedTime += Time.deltaTime;

            float fadeLevel = FadeLevels.Evaluate(elapsedTime / TransitionDuration);
            //fader.SetFadeLevel(fadeLevel);
        }

        //fader.SetFadeLevel(0);

        if (OnCompleteFade != null)
            OnCompleteFade();
    }

    public void SetFixedFadeAmount(float amount)
    {
        //fader.SetFadeLevel(Mathf.Clamp(amount, 0, 1));
    }
}
