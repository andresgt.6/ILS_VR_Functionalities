﻿[System.Serializable]

public class Survey
{
    public int result;
    public string idCourse;

    public Survey(int result)
    {
        this.result = result;
        this.idCourse = Constants.courseId;
    }
}
