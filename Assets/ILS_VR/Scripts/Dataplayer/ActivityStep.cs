﻿[System.Serializable]
public class ActivityStep
{
    public int step;
    public string description;
    public float time;
    public int repeats;
    public bool win;

    public ActivityStep(int step, string description, float time, int repeats, bool win)
    {
        this.step = step;
        this.description = description;
        this.time = time;
        this.repeats = repeats;
        this.win = win;
    }
}
