﻿[System.Serializable]
public class SessionResults
{
    public string courseId;
    public ActivityStep[] steps;

    public SessionResults(ActivityStep[] steps)
    {
        this.courseId = Constants.courseId;
        this.steps = steps;
    }
}
