﻿using System.Collections;
using UnityEngine;

public class PlayerEyeRaycast : MonoBehaviour
{
    public enum State
    {
        Disabled,
        Enabled
    }

    [Header("Current State")]
    [SerializeField] private State currentState;
    [SerializeField] private Transform lastTransformSeen;

    private Camera playerEyes;
    private RaycastType type = RaycastType.CenterEye;

    public RaycastTargetBase CurrentTargetEye { get; private set; }

    private void Awake()
    {
        playerEyes = Camera.main;

        OnActiveRaycast(true);
    }

    public void OnActiveRaycast(bool enabled)
    {
        currentState = enabled? State.Enabled : State.Disabled;
        
        if (enabled)
            StartCoroutine(RaycastSensor());
    }

    private IEnumerator RaycastSensor()
    {
        while(currentState == State.Enabled)
        {
            CheckTargets();
            yield return null;
        }
    }

    private void CheckTargets()
    {
        Ray ray = playerEyes.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            lastTransformSeen = hit.transform;
            //print("Mirando a " + lastTransformSeen.name);

            if (CurrentTargetEye)
            {
                if (lastTransformSeen != CurrentTargetEye.transform)
                    ForgetCurrentTarget();
            }

            if (CurrentTargetEye == null && lastTransformSeen.TryGetComponent(out RaycastTargetBase target))
                SaveTarget(target);

        }
        else if (CurrentTargetEye)
            ForgetCurrentTarget();
    }

    private void ForgetCurrentTarget()
    {
        CurrentTargetEye.LookAtMe(false, type);
        CurrentTargetEye = null;
    }

    private void SaveTarget(RaycastTargetBase target)
    {
        target.LookAtMe(true, type);
        CurrentTargetEye = target;
    }

    private void OnDestroy()
    {
        currentState = State.Disabled;
        StopAllCoroutines();
    }

}
