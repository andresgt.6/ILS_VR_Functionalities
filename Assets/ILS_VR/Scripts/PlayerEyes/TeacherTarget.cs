﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeacherTarget : RaycastTargetBase
{

    [SerializeField] private UISpriteController okGesture;

    [SerializeField] private Animator animator;

    private bool enableOkHand;    

    protected override void OnLookingAtMe()
    {
        base.OnLookingAtMe();

        if (currentRaycast == RaycastType.CenterEye && enableOkHand)
        {
            okGesture.ChangeSprite(UISpriteController.UIImage.Ok);
            animator.SetBool("Ok_Gesture", true);
        }
    }

    public void SetEnabledOkHand(bool enabled)
    {
        enableOkHand = enabled;

        if (!enableOkHand)
            okGesture.DisableImage();
    }

    protected override void OnStopLookingAtMe()
    {
        base.OnStopLookingAtMe();
        if (currentRaycast == RaycastType.CenterEye && enableOkHand)
        {
            okGesture.DisableImage();
            animator.SetBool("Ok_Gesture", false);
        }
    }
}
