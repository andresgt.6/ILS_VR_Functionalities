﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TargetEye : RaycastTargetBase
{

    public enum Type
    {
        Calibration,
        ChairTest,
        Teacher
    }

    public Type currentType;

    [SerializeField] private Image mainImage;
    [SerializeField] private Text infoText;
    [SerializeField] private Sprite[] loadingSecuence;

    private Action ScanTime;
    private Action CompleteFill;

    private Coroutine FillCoroutine;

    private int trackingSpriteCount = 1;

    public void SetAction(States currentState, Type? typeToChange = null)
    {
        if (typeToChange.HasValue)
            currentType = typeToChange.Value;

        mainImage.rectTransform.sizeDelta = new Vector2(300, 300);
        switch (currentState)
        {
            case States.Disabled:
                break;
            case States.LookingAtMe:
            case States.Counting:
                infoText.text = "MANTÉN TU MIRADA AQUÍ";
                mainImage.sprite = loadingSecuence[0];
                break;
            case States.Scanning:
                infoText.text = "ESCANEANDO";
                mainImage.sprite = loadingSecuence[1];
                trackingSpriteCount = 1;
                break;
            case States.Completed:
                infoText.text = "COMPLETADO";
                StartCoroutine(CompleteScannProgress());
                break;
            default:
                break;
        }
    }

    public void SetFillProgress(bool enabled, float time, Action ScanTime, Action CallbackComplete)
    {
        trackingSpriteCount = 1;

        CompleteFill = CallbackComplete;
        this.ScanTime = ScanTime;

        if (enabled)
            FillCoroutine = StartCoroutine(FillingProcess(time));
        else
        {
            if (FillCoroutine != null)
            {
                StopCoroutine(FillCoroutine);
                FillCoroutine = null;
            }
        }
    }

    private IEnumerator CompleteScannProgress()
    {
        mainImage.sprite = loadingSecuence[8];
        mainImage.rectTransform.sizeDelta = new Vector2(400, 400);
        yield return new WaitForSeconds(0.5f);
        mainImage.sprite = loadingSecuence[9];
        mainImage.rectTransform.sizeDelta = new Vector2(500, 500);
    }

    private IEnumerator FillingProcess(float time)
    {
        for (float i = 0; i < time; i++)
        {
            SetProgress();
            ScanTime?.Invoke();
            yield return new WaitForSeconds(1);
        }
        CompleteFill?.Invoke();
        //SetProgress();
    }

    private void SetProgress()
    {
        mainImage.sprite = loadingSecuence[++trackingSpriteCount];

        if (currentType == Type.Calibration)
        {
            LeanTween.delayedCall(.5f, () =>
            {
                mainImage.sprite = loadingSecuence[++trackingSpriteCount];
            });
        }
    }
}
