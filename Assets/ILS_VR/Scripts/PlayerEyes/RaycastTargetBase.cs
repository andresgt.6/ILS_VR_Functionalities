﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum EyeTarget
{
    None,
    Circle,
    Teacher
}

public enum RaycastType
{
    None,
    CenterEye,
    Finger
}

public class RaycastTargetBase : MonoBehaviour
{
    public enum States
    {
        Disabled,
        LookingAtMe,
        Counting,
        Scanning,
        Completed
    }

    public Action<bool, EyeTarget> LookingAtMe;
    public EyeTarget targetType;

    public bool isLookingAtMe;

    protected RaycastType currentRaycast;

    public virtual void LookAtMe(bool look, RaycastType lastLooking)
    {
        if (isLookingAtMe != look)
            LookingAtMe?.Invoke(look, targetType);

        currentRaycast = lastLooking;

        isLookingAtMe = look;

        if (look)
        {
            //Player is looking at me            
            OnLookingAtMe();
        }
        else
        {
            //Player is not looking at me            
            OnStopLookingAtMe();
        }
    }

    protected virtual void OnLookingAtMe()
    {
        
    }

    protected virtual void OnStopLookingAtMe()
    {

    }
}
