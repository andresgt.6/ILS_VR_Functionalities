﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModuleDidUknowInfo
{
    public bool enabled;
    public Dialogue[] dialogues;
}
