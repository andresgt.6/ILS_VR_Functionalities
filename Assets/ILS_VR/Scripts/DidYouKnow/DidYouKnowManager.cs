﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DidYouKnowManager : MonoBehaviour
{

	private List<int> didYouKnow = new List<int>();

	[SerializeField] private List<int> selectedOnes;
	void Awake()
    {
		didYouKnow.Add(1);
		didYouKnow.Add(2);
		didYouKnow.Add(3);
		didYouKnow.Add(4);
		didYouKnow.Add(5);
	}

	public List<int> GenerateRandom()
	{
		List<int> randomDidYouKnow = new List<int>();
		for (int i = 0; i < 2; i++)
		{
			int RandomDid = Random.Range(1, didYouKnow.Count);
			randomDidYouKnow.Add(didYouKnow[RandomDid]);
			didYouKnow.RemoveAt(RandomDid);
		}		
		selectedOnes = randomDidYouKnow;
		return randomDidYouKnow;
	}
}
