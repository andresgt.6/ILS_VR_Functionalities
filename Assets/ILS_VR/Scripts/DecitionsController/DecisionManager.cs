﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecisionManager : MonoBehaviour
{
    [SerializeField] private Sprite[] Images;
    [SerializeField] private Buttons[] InteractableButtons;
    [SerializeField] private Canvas UIOptions;

    private void Awake()
    {
        SetDisable();
    }

    public void SetInteraction()
    {
        for (int i = 0; i < InteractableButtons.Length; i++)
            InteractableButtons[i].SetActiveInteraction();
    }
    public void ActivateButton(ButtonsType buttonsType, Action<int> OnSelect)
    {
        SetEnabled();
        for (int i = 0; i < InteractableButtons.Length; i++)
        {
            InteractableButtons[i].buttonId = i;
            InteractableButtons[i].ActiveButton(buttonsType, OnSelect, Images[i]);
            InteractableButtons[i].OnStateChanged += OnInteract; ;
        }
    }

    public void SetEnabled()
    {
        UIOptions.enabled = true;
    }
    public void SetDisable()
    {
        UIOptions.enabled = false;
        for (int i = 0; i < InteractableButtons.Length; i++)
            InteractableButtons[i].DesactiveInteractions();
    }

    private void OnInteract(bool arg0)
    {
        for (int i = 0; i < InteractableButtons.Length; i++)
        {
            //InteractableButtons[i].ActiveButton(buttonsType, OnSelect, Images[i]);
            InteractableButtons[i].OnStateChanged -= OnInteract; ;
        }
    }
}
