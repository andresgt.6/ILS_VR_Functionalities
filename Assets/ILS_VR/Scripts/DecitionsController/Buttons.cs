﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buttons : HoverButton
{
    public int buttonId;

    private Action<int> Interactable;

    private Image currentImage;

	private void Awake()
    {
		currentImage = GetComponent<Image>();
		//fillImage = GetComponentsInChildren<Image>().;
	}

    public void SetActiveInteraction()
    {
        ActiveInteractions(1);
    }

    public void ActiveButton(ButtonsType Type, Action<int> Active, Sprite SourceImage)
    {
        Interactable = Active;


        switch (Type)
        {
            case ButtonsType.Place:
                currentImage.sprite = SourceImage;
                break;
            case ButtonsType.PcType:
                currentImage.sprite = SourceImage;
                break;
            default:
                break;
        }

    }

    public override void InteractWithButton(Side handSide = Side.RightSide)
    {
        base.InteractWithButton();
        Interactable?.Invoke(buttonId);
    }

    protected override void HoverStarted()
    {
        base.HoverStarted();
        if (LeanTween.isTweening(gameObject))
            LeanTween.cancel(gameObject);

        LeanTween.scale(gameObject, new Vector3(0.7f, 0.7f, 0.7f), 1);

	}

    protected override void HoverEnded()
    {
        base.HoverEnded();
        if (LeanTween.isTweening(gameObject))
            LeanTween.cancel(gameObject);

        LeanTween.scale(gameObject, new Vector3(0.5f, 0.5f, 0.5f), 1);
    }
}
