﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
//using Oculus.Platform.Samples.VrHoops;

public enum GestureType
{
    None,
    Ok,
    FuckU,
    Point
}

[Serializable]
public struct Gesture
{
    public string name;
    public List<Vector3> fingerDatas;
    public GestureType type;
}

public class GestureDetector : MonoBehaviour
{
    public enum State
    {
        Disabled,
        EnabledForCustomGestureLookingInstructor,
        EnabledForCustomGestureLookingInstructorAndPoint,
        EnabledForAnyGesture
    }

    public event Action<GestureType> GestureDetected;

    [SerializeField] private float threshold = 0.1f;
    [SerializeField] private bool debugMode = true;
    //[SerializeField] private OVRSkeleton handSkeleton;
    [SerializeField] private List<Gesture> gestures;

    [Header("Current State")]
    [SerializeField] private State currentState;

    private Action CompleteRequestedGesture;

    private GestureType requestedGesture = GestureType.None;
    private GestureType[] requestedGestures;

    private Gesture previousGesture = new Gesture();

    //private List<OVRBone> fingerBones = new List<OVRBone>();

    private PlayerEyeRaycast playerEye;

    void Awake()
    {
        //StartCoroutine(CheckingBones());
    }


    //private IEnumerator CheckingBones()
    //{
    //    while (fingerBones.Count == 0)
    //    {
    //        fingerBones = new List<OVRBone>(handSkeleton.Bones);
    //        yield return new WaitForEndOfFrame();
    //    }
    //}

    public void SetEnabledGestureDetector(State state)
    {
        currentState = state;
    }

    public void StartListenAnyGesture(bool enabled = true)
    {
        if (enabled)
        {
            previousGesture = new Gesture();
            SetEnabledGestureDetector(State.EnabledForAnyGesture);
        }
        else
        {
            SetEnabledGestureDetector(State.Disabled);
        }
    }

    public void ListeningForGestureLookingInstructor(GestureType gestureToListen, Action completeCallback, PlayerEyeRaycast playerEye, bool andpoint)
    {
        this.playerEye = playerEye;
        previousGesture = new Gesture();
        requestedGesture = gestureToListen;
        CompleteRequestedGesture = completeCallback;
        if(!andpoint)
            SetEnabledGestureDetector(State.EnabledForCustomGestureLookingInstructor);
        else
            SetEnabledGestureDetector(State.EnabledForCustomGestureLookingInstructorAndPoint);
    }

	public void ListeningForGestureLookingInstructor(GestureType[] gestureToListen, Action completeCallback, PlayerEyeRaycast playerEye, bool andpoint)
	{
		this.playerEye = playerEye;
		previousGesture = new Gesture();
		requestedGestures = gestureToListen;
		CompleteRequestedGesture = completeCallback;
        if (!andpoint)
            SetEnabledGestureDetector(State.EnabledForCustomGestureLookingInstructor);
        else
            SetEnabledGestureDetector(State.EnabledForCustomGestureLookingInstructorAndPoint);
    }

	void Update()
    {
        CheckForSavingGesture();

        if (currentState.Equals(State.Disabled))
            return;

        CheckingForGestures();
    }

    private void CheckingForGestures()
    {
        Gesture currentGesture = Recongnize();
        bool hasRecognized = !currentGesture.Equals(new Gesture());

		if (currentState == State.Disabled)
			return;

        if (currentState == State.EnabledForAnyGesture || currentState == State.EnabledForCustomGestureLookingInstructorAndPoint)
        {
            if (!hasRecognized)
                GestureDetected?.Invoke(GestureType.None);
            else
                GestureDetected?.Invoke(currentGesture.type);
        }

        if (hasRecognized)
        {
            Debug.Log("gesto encontrado : " + currentGesture.name);

            switch (currentState)
            {
                case State.Disabled:
                    return;

                case State.EnabledForCustomGestureLookingInstructor:
                case State.EnabledForCustomGestureLookingInstructorAndPoint:
					if(requestedGesture != GestureType.None)
					{
						if (requestedGesture.Equals(currentGesture.type) && playerEye.CurrentTargetEye && playerEye.CurrentTargetEye.targetType == EyeTarget.Teacher && !currentGesture.Equals(previousGesture))
						{
							requestedGesture = GestureType.None;
							previousGesture = currentGesture;
							CompleteRequestedGesture?.Invoke();
							CompleteRequestedGesture = null;
						}
					}    
					else
					{
						if (requestedGestures == null)
							return;

						for (int i = 0; i < requestedGestures.Length; i++)
						{
							if (requestedGestures[i].Equals(currentGesture.type) && playerEye.CurrentTargetEye && playerEye.CurrentTargetEye.targetType == EyeTarget.Teacher && !currentGesture.Equals(previousGesture))
							{
								requestedGestures = new GestureType[0];
								previousGesture = currentGesture;
								CompleteRequestedGesture?.Invoke();
								CompleteRequestedGesture = null;
								break;
							}
						}						
					}
                    break;

                case State.EnabledForAnyGesture:
                    previousGesture = currentGesture;
                    break;

                default:
                    break;
            }
        }
    }

    private Gesture Recongnize()
    {
        Gesture currentGesture = new Gesture();
        float currentMin = Mathf.Infinity;

        for (int i = 0; i < gestures.Count; i++)
        {
            float sumDistance = 0;
            bool isDiscarted = false;
            //for (int a = 0; a < fingerBones.Count; a++)
            //{
            //    Vector3 currentData = handSkeleton.transform.InverseTransformPoint(fingerBones[a].Transform.position);
            //    float distance = Vector3.Distance(currentData, gestures[i].fingerDatas[a]);
            //    if (distance > threshold)
            //    {
            //        isDiscarted = true;
            //        break;
            //    }

            //    sumDistance += distance;
            //}

            if (!isDiscarted && sumDistance < currentMin)
            {
                currentMin = sumDistance;
                currentGesture = gestures[i];
            }
        }

        return currentGesture;
    }

    private void CheckForSavingGesture()
    {
        if (Input.GetKeyDown(KeyCode.Space) && debugMode)
        {
            Save();
        }
    }

    public void Save()
    {
        Gesture gesture = new Gesture();
        gesture.name = "New Gesture";
        List<Vector3> data = new List<Vector3>();
        //print("Huesos " + fingerBones.Count);
        //for (int i = 0; i < fingerBones.Count; i++)
        //{
        //    data.Add(handSkeleton.transform.InverseTransformPoint(fingerBones[i].Transform.position));
        //}

        gesture.fingerDatas = data;
        gestures.Add(gesture);
        Debug.Log("Gesture saved!");
    }
}
