﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GestureManager : MonoBehaviour
{
    public event Action<Side, GestureType> AnyGestureMade;

    [SerializeField] private GestureDetector rightHand;
    [SerializeField] private GestureDetector leftHand;

    [SerializeField] private PlayerEyeRaycast playerEye;

    private Action CompleteCustomGestureListening;

    /// <summary>
    /// Waits until the custom gesture is done
    /// </summary>
    /// <param name="gestureType"></param>
    public void ListenForCustomGestureLookingInstructor(GestureType gestureType, Action CompleteCustomGestureListening, bool andPoint)
    {
        //playerEye.OnActiveRaycast(true);

        rightHand.ListeningForGestureLookingInstructor(gestureType, OnCompleteCustomGesture, playerEye,andPoint);
        leftHand.ListeningForGestureLookingInstructor(gestureType, OnCompleteCustomGesture, playerEye,andPoint);

        this.CompleteCustomGestureListening = CompleteCustomGestureListening;
    }

	public void ListenForCustomGestureLookingInstructor(GestureType[] gestureType, Action CompleteCustomGestureListening, bool andPoint)
	{
		//playerEye.OnActiveRaycast(true);

		rightHand.ListeningForGestureLookingInstructor(gestureType, OnCompleteCustomGesture, playerEye,andPoint);
		leftHand.ListeningForGestureLookingInstructor(gestureType, OnCompleteCustomGesture, playerEye, andPoint);

		this.CompleteCustomGestureListening = CompleteCustomGestureListening;
	}

	public void StartListeningForAnyGesture()
    {
        //playerEye.OnActiveRaycast(true);
        print("se encontró desde Manager de gestos");

        rightHand.StartListenAnyGesture();
        rightHand.GestureDetected += OnRightHandAnyGestureDetected;

        leftHand.StartListenAnyGesture();
        leftHand.GestureDetected += OnLeftHandAnyGestureDetected;
    }

    private void OnLeftHandAnyGestureDetected(GestureType gesture)
    {
        AnyGestureMade?.Invoke(Side.LeftSide, gesture);
    }

    private void OnRightHandAnyGestureDetected(GestureType gesture)
    {
        AnyGestureMade?.Invoke(Side.RightSide, gesture);
    }

    public void StopListeningForCustomGesture()
    {
        //playerEye.OnActiveRaycast(true);

        rightHand.StartListenAnyGesture(false);
        rightHand.GestureDetected -= OnRightHandAnyGestureDetected;

        leftHand.StartListenAnyGesture(false);
        leftHand.GestureDetected -= OnLeftHandAnyGestureDetected;
    }

    private void OnCompleteCustomGesture()
    {
        //playerEye.OnActiveRaycast(false);

        StopListenGestures();
        CompleteCustomGestureListening?.Invoke();
        CompleteCustomGestureListening = null;
    }

    public void StopListenGestures()
    {
        rightHand.SetEnabledGestureDetector(GestureDetector.State.Disabled);
        leftHand.SetEnabledGestureDetector(GestureDetector.State.Disabled);
    }
}
