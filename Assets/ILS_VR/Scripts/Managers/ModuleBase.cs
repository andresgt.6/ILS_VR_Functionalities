﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum ModuleStates
{
	Normal,
	Failed
}
public class ModuleBase : MonoBehaviour
{
	public Action<Places> ChangePlace;
	public event Action<ModuleBase> OnModuleCompleted;

	protected Action SingleDialogueComplete;

	public List<ActivityStep> stepsToSend = new List<ActivityStep>();
	public ModuleType TypeOfModule { get; protected set; }
	public ModuleStates ModuleState { get; protected set; }

	[Header("Module Base")]
	[SerializeField] protected Transform playerStartPoint = null;
	[SerializeField] protected GameObject moduleObjects;

	[Header("Did You Know")]
	[SerializeField] protected bool didYouKnowActive = false;
	public ModuleDidUknowInfo[] moduleDidUknowInfos;

	[Header("Trainer Visuals")]
	[SerializeField] private TrainerElementType[] trainerActiveObjects = null;

	protected MobileUISettings settingsScreenSettings = new MobileUISettings();

	protected ModuleEntities entities;
	protected bool isModuleactive;
	protected int step;

	protected TagInfo tagInfo = new TagInfo();

	protected NPCActionSettings femaleTrainerSettings = new NPCActionSettings();

	protected NPCLookSettings femaleTrainerlook = new NPCLookSettings();

	protected PersonLookController femaleTrainerLookController;

	protected Dialogue[] dialogues;
	protected float speechDuration;
	protected float stepDuration;
	protected bool completeCorrect;
	protected int stepId;
	protected int dialogueCount;
	protected string stepName;
	protected Coroutine DialogueCoroutine;
	private Coroutine takeLongCoroutine;

	protected XMLManager xmlManager;

	public float temporalScore;

	private bool isStepCompleted = false;
	private bool isTellingScore = false;

	public enum TimerState
	{
		Pause,
		Stop,
		Run,
		Reset
	}
	protected TimerState currentTimeState;

	private readonly DataBase.DataBaseManager DataBaseManager = new DataBase.DataBaseManager();	

	public virtual void InitializeModule(ModuleEntities _entities)
	{
		xmlManager = new XMLManager();
		xmlManager.LoadXML();

		femaleTrainerLookController = _entities.maleTrainer.GetComponent<PersonLookController>();

		entities = _entities;

		entities.taskManager.Initialized(entities);

		entities.tagIndicator.Disable();
	}

	public void StartModule()
	{
		if (isModuleactive)
			return;

		isModuleactive = true;

		SetTutorslookTarget();

		if (TypeOfModule == ModuleType.Module0)
		{
			ConfigurateModule();
			entities.blinker.HalfBlinkFadeOut(ShowModuleName);
		}
		else
			BlinkToStartModule();
	}

	protected void BlinkToStartModule()
	{
		entities.blinker.FullBlink(ConfigurateModule, ShowModuleName);
	}

	protected virtual void ConfigurateModule()
	{
		ModuleState = ModuleStates.Normal;

		entities.repeatCanvas.enabled = false;

		moduleObjects.SetActive(true);

		entities.taskManager.CleanAllTasks();

		//if(trainerActiveObjects != null && trainerActiveObjects.Length > 0)
		//	entities.maleTrainer.GetComponentInChildren<TrainerVisualsController>().ConfigurateTrainerVisuals(trainerActiveObjects);

		if (playerStartPoint)
			entities.player.Teleport(playerStartPoint);
	}


	protected virtual void ShowModuleName()
	{
		SoundManager.PlaySoundByName("UIInformative");

		if (!TypeOfModule.Equals(ModuleType.SubModule1))
			LeanTween.delayedCall(6.75f, SelectNextStep);
		else
			SelectNextStep();
	}

	protected virtual void SetTutorslookTarget(Transform target = null)
	{
		if (!target)
			target = Camera.main.transform;

		femaleTrainerLookController.SetDefault();
		femaleTrainerlook.delay = 0;
		femaleTrainerlook.duration = null;
		femaleTrainerlook.lookType = LookingType.LookingTransform;
		femaleTrainerlook.lookObject = target;
		femaleTrainerLookController.AddAction(femaleTrainerlook);
	}

	protected virtual void SetTagToCustomObject(Transform parent, TagInfo info)
	{
		entities.tagIndicator.transform.SetParent(parent);
		entities.tagIndicator.transform.localPosition = Vector3.zero;
		entities.tagIndicator.transform.localEulerAngles = Vector3.zero;
		entities.tagIndicator.Enabled(info);
	}

	protected IEnumerator SetDialogs(Dialogue[] dialogues)
	{
		for (int i = 0; i < dialogues.Length; i++)
		{
			float duration = entities.dialogueManager.GetDialogueDuration(dialogues[i]);

			if (dialogues[i].id == 1)
			{
				entities.dialogueManager.PlayDialogue(dialogues[i]);
			}
			else
			{
				femaleTrainerSettings.delayToStart = 0;
				femaleTrainerSettings.duration = duration;
				femaleTrainerSettings.action = AIActions.Talk;
				femaleTrainerSettings.objectToInteract = entities.maleTrainer.transform;
				entities.maleTrainer.SetAction(femaleTrainerSettings);
				entities.dialogueManager.PlayDialogue(dialogues[i]);
			}

			yield return new WaitForSeconds(duration + 1);
			SingleDialogueComplete?.Invoke();
			DialogueCoroutine = null;
		}
	}

	protected virtual void SetTakeLongTime(Dialogue[] dialogues, float time = 7)
	{
		takeLongCoroutine = StartCoroutine(PlayTakeLongTime(dialogues, time));
	}

	private IEnumerator PlayTakeLongTime(Dialogue[] dialogues, float time = 7)
	{
		yield return new WaitForSeconds(time);

		for (int i = 0; i < dialogues.Length; i++)
		{
			if (takeLongCoroutine == null || !isModuleactive)
				break;

			float duration = entities.dialogueManager.GetDialogueDuration(dialogues[i]);

			entities.dialogueManager.PlayDialogue(dialogues[i]);

			yield return new WaitForSeconds(duration + 1);
		}

		if (takeLongCoroutine != null)
			takeLongCoroutine = StartCoroutine(PlayTakeLongTime(dialogues, time));
	}

	public virtual void OnLoadingScene()
	{

	}

	protected virtual void SelectNextStep()
	{
		step++;
		isStepCompleted = false;
		RemoveLongTime();
	}

	public void RemoveLongTime()
	{
		if (takeLongCoroutine != null)
		{
			StopCoroutine(takeLongCoroutine);
			takeLongCoroutine = null;
		}
	}

	public virtual void CompleteStep()
	{
		isStepCompleted = true;

		if(isModuleactive && !isTellingScore)
			SelectNextStep();
	}
	protected virtual void CompleteStepData()
	{
		entities.taskManager.CompleteAllTasks -= CompleteStepData;

		completeCorrect = true;

		ActivityStep[] steps = new ActivityStep[1];
		steps[0] = new ActivityStep(stepId, stepName, stepDuration, 0, completeCorrect);
		print(string.Format("Enviando datos del paso {0}. {1}, Duración:{2}, Fallos:{3}, Aprobado:{4}", stepId, stepName, stepDuration, 0, completeCorrect));
		SessionResults sessionResults = new SessionResults(steps);

		stepsToSend.Add(steps[0]);

		SetTimer(TimerState.Stop);

		SelectNextStep();
	}

	protected virtual void Update()
	{
		if (currentTimeState == TimerState.Run)
		{
			stepDuration += Time.deltaTime;
		}
	}

	protected virtual void SetTimer(TimerState timerState)
	{
		currentTimeState = timerState;
		switch (timerState)
		{
			case TimerState.Pause:
				break;
			case TimerState.Stop:
				stepDuration = 0;
				break;
			case TimerState.Run:
				break;
			case TimerState.Reset:
				stepDuration = 0;
				SetTimer(TimerState.Run);
				break;
			default:
				break;
		}
	}

	/// <summary>
	/// Si pierde el módulo
	/// </summary>
	protected virtual void OnLostModule()
	{
		ModuleState = ModuleStates.Failed;
		completeCorrect = false;//mirarlo
		entities.taskManager.CleanAllTasks();
		CompleteStepData();
	}

	public virtual void SetDidYouKnow()
	{
		didYouKnowActive = true;

		if(moduleDidUknowInfos.Length > 0)
			moduleDidUknowInfos[UnityEngine.Random.Range(0, moduleDidUknowInfos.Length)].enabled = true;
	}

	protected virtual void ShowDidYouKnow(int id)
	{
		if(moduleDidUknowInfos[id].enabled)
		{			
			speechDuration = entities.dialogueManager.GetDialogueDuration(moduleDidUknowInfos[id].dialogues) + moduleDidUknowInfos[id].dialogues.Length;

			StartCoroutine(SetDialogs(moduleDidUknowInfos[id].dialogues));

			DidYouKnowInfo info = new DidYouKnowInfo();
			info = xmlManager.GetDidUKnow((int)TypeOfModule - 1, id);
			entities.startModuleUI.SetDidUKnowEnabled(true, info.title, info.body, speechDuration);

			LeanTween.delayedCall(speechDuration, CompleteStep);
		}
		else
		{
			CompleteStep();
		}
	}

	public float Score(float value, Transform objectPos, bool lectureOnly = false, int stepsToReturnWhenFail = 2, Enum stepName = null)
	{
		if(lectureOnly)
			return temporalScore;

		bool isRight = value > 0;

		//Minner objeto = objectPos.GetComponentInChildren<Minner>();

		//if (objeto)
		//{
		//	objectPos = objeto.transform;
		//}

		temporalScore += value;

		//ParticleManager.SetParticle?.Invoke(new ParticulesValues(objectPos.position,isRight? SignalType.Succes : SignalType.Fail));

		if(!isRight)
		{
			//entities.repeatCanvas.enabled = true;
			step -= stepsToReturnWhenFail;
			//LeanTween.delayedCall(5, () => { entities.repeatCanvas.enabled = false; });
			SoundManager.PlaySoundByName("BadParticle");
		}
		else
		{
			SoundManager.PlaySoundByName("GodParticle"); 
		}

		if(stepName != null)
		{
			AudioSource source = entities.dialogueManager.GetCurrentAudioSource();
			AudioClip clip = Resources.Load<AudioClip>("Sounds/Dialogues/" + (isRight ? "Right" : "Wrong") + "/" + stepName);

			if(clip != null)
			{
				source.Stop();
				source.clip = clip;
				source.Play();
				isTellingScore = true;
				LeanTween.delayedCall(clip.length + 1f, ScoreDialogCompleted);
			}
			else
			{
				Debug.LogWarning("Sounds/Dialogues/" + (isRight ? "Right" : "Wrong") + "/" + stepName + " - Not found");
			}
		}

		return temporalScore;
	}

	private void ScoreDialogCompleted()
	{
		isTellingScore = false;

		if (isModuleactive && isStepCompleted)
			SelectNextStep();
	}
	protected virtual void SkipModule(bool enabled)
	{
		CompleteModule();
	}
	protected virtual void EnabledNextModuloButton()
	{
		Debug.LogError("Activando boton");
		LeanTween.delayedCall(30, () => {
			if (!isModuleactive)
				return;

			entities.nextModuleButton.gameObject.SetActive(true);
			entities.nextModuleButton.OnStateChanged += SkipModule;
			entities.nextModuleButton.EnableButton();
		});

	}
	protected virtual void DisabledSkipModuleButton()
	{
		entities.nextModuleButton.OnStateChanged -= SkipModule;
		entities.nextModuleButton.gameObject.SetActive(false);
	}

	protected virtual void CompleteModule()
	{
		step = 0;
		RemoveLongTime();
		DisabledSkipModuleButton();
		entities.dialogueManager.CancelAll();
		entities.taskManager.CleanAllTasks();
		StopAllCoroutines();
		LeanTween.cancel(gameObject);

		entities = null;
		isModuleactive = false;

		OnModuleCompleted?.Invoke(this);

		moduleObjects.SetActive(false);
	}
}
