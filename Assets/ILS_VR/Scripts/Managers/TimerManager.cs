﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerManager : MonoBehaviour
{
    public event UnityAction OnCompleteTimer;
    public event UnityAction<float> OnUpdateTimer;

    public enum TimerState
    {
        Paused,
        Counting,
        Complete
    }

    public float CurrentTime { get ; private set; }

    public static TimerManager Instance;    
    public TimerState timerState;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        if (timerState == TimerState.Counting)            
            CountTimer();
    }

    public void SetTimerLength(float experienceDuration)
    {
        if(timerState == TimerState.Paused)
            CurrentTime = experienceDuration;
    }

    public void StartTimer()
    {        
        timerState = TimerState.Counting;
    }

    public void PauseTimer()
    {
        timerState = TimerState.Paused;
    }

    private void CountTimer()
    {
        CurrentTime -= Time.deltaTime;
        OnUpdateTimer?.Invoke(CurrentTime);
        if(CurrentTime <= 0)
        {
            CurrentTime = 0;
            timerState = TimerState.Complete;
            OnCompleteTimer?.Invoke();
        }
    }
}
