﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// It manages all the tasks needed for advance
/// </summary>
public class TaskManager : MonoBehaviour
{
	public UnityAction CompleteAllTasks;

	public List<TaskToValidate> currentTasks;

	private List<WeareableTool> currentWeareableTools = new List<WeareableTool>();
	private List<HoverButton> currentHoverButtons = new List<HoverButton>();
	private List<ToolStateInfo> currentTools = new List<ToolStateInfo>();

	private int hoverButtonCount;

	private ModuleEntities entities;

	public void Initialized(ModuleEntities entities)
	{
		this.entities = entities;
	}


	#region ADD TASK

	/// <summary>
	/// When the times out, it completes
	/// </summary>
	/// <param name="currentTask"></param>
	/// <param name="delay"></param>
	public void AddTask(float delay = 10)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.Delay;

		LeanTween.delayedCall(delay, CompleteDelayTime);

		currentTasks.Add(task);
	}

	/// <summary>
	/// When the timeline finish it complete
	/// </summary>
	/// <param name="currentTask"></param>
	/// <param name="timeline"></param>
	public void AddTask(TimelineController timeline)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.Timeline;

		timeline.Play(CompleteTimeline);
		currentTasks.Add(task);
	}

	/// <summary>
	/// When the user grab the tools it completes
	/// </summary>
	/// <param name="currentTask"></param>
	/// <param name="weareableTools"></param>
	public void AddTask(WeareableTool[] weareableTools)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.WearTools;

		for (int i = 0; i < weareableTools.Length; i++)
		{
			weareableTools[i].ActiveWeareable();
			weareableTools[i].OnWeared += CompleteWeared;
			currentWeareableTools.Add(weareableTools[i]);
		}

		currentTasks.Add(task);
	}

	/// <summary>
	/// When the user interacts with hover button it completes
	/// </summary>
	/// <param name="currentTask"></param>
	/// <param name="hoverButtons"></param>
	public void AddTask(HoverButton[] hoverButtons)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.HoverButtons;

		hoverButtonCount = 0;

		for (int i = 0; i < hoverButtons.Length; i++)
		{
			hoverButtons[i].ActiveInteractions(1);
			hoverButtons[i].OnStateChanged += CompleteHoverButtonInteraction;
			currentHoverButtons.Add(hoverButtons[i]);
		}

		currentTasks.Add(task);
	}

	/// <summary>
	/// When the person completes all tasks
	/// </summary>
	/// <param name="currentTask"></param>
	/// <param name="currentPerson"></param>
	public void AddTask(PersonBase currentPerson)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.PersonCompleteTask;

		hoverButtonCount = 0;

		currentPerson.OnCompleteAllActions += CompletePersonTask;

		currentTasks.Add(task);
	}

	/// <summary>
	/// When the person completes all tasks
	/// </summary>
	/// <param name="currentTask"></param>
	/// <param name="currentPerson"></param>
	public void AddTask(ToolBase currentTool, ToolStates toolState)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.SetTool;

		ToolStateInfo tool = new ToolStateInfo();
		tool.state = toolState;
		tool.tool = currentTool;
		currentTools.Add(tool);
		tool.tool.ChangeState += OnChangeToolState;

		currentTasks.Add(task);
	}

	public void AddTask(GestureManager gesture, GestureType gestureToListen, bool andPoint = false)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.Gesture;
		gesture.ListenForCustomGestureLookingInstructor(gestureToListen, CompleteGesture, andPoint);
		entities.maleTrainer.GetComponentInChildren<TeacherTarget>().SetEnabledOkHand(true);
		currentTasks.Add(task);
	}
	#endregion

	public void AddTask(GestureManager gesture, GestureType[] gesturesToListen, bool andPoint = false)
	{
		TaskToValidate task = new TaskToValidate();
		task.taskToValidate = Tasks.Gesture;
		gesture.ListenForCustomGestureLookingInstructor(gesturesToListen, CompleteGesture, andPoint);
		entities.maleTrainer.GetComponentInChildren<TeacherTarget>().SetEnabledOkHand(true);
		currentTasks.Add(task);
	}


	private void CompleteWeared(WearableType wearableType)
	{
		for (int i = 0; i < currentWeareableTools.Count; i++)
		{
			if (wearableType == currentWeareableTools[i].wearableType)
			{
				currentWeareableTools[i].OnWeared -= CompleteWeared;
				currentWeareableTools.RemoveAt(i);
				break;
			}
		}

		if (currentWeareableTools.Count == 0)
			CompleteTask(Tasks.WearTools);
	}

	private void CompletePersonTask()
	{
		CompleteTask(Tasks.PersonCompleteTask);
	}

	private void OnChangeToolState(ToolStateInfo tool)
	{
		for (int i = 0; i < currentTools.Count; i++)
		{
			if (currentTools[i].tool == tool.tool && currentTools[i].state == tool.state)
			{
				currentTools[i].tool.ChangeState -= OnChangeToolState;
				currentTools.RemoveAt(i);
				CompleteTask(Tasks.SetTool);
				break;
			}
		}
	}

	private void CompleteHoverButtonInteraction(bool isActive)
	{
		if (hoverButtonCount++ < currentHoverButtons.Count - 1)
			return;

		for (int i = 0; i < currentHoverButtons.Count; i++)
		{
			currentHoverButtons[i].OnStateChanged -= CompleteHoverButtonInteraction;
		}

		currentHoverButtons.Clear();

		if (currentWeareableTools.Count == 0)
			CompleteTask(Tasks.HoverButtons);
	}

	private void CompleteDelayTime()
	{
		CompleteTask(Tasks.Delay);
	}

	private void CompleteTimeline()
	{
		CompleteTask(Tasks.Timeline);
	}

	private void CompleteGesture()
	{
		entities.maleTrainer.GetComponentInChildren<TeacherTarget>().SetEnabledOkHand(false);
		CompleteTask(Tasks.Gesture);
	}

	private void CompleteTask(Tasks currentTask)
	{
		//Found the task and mark it as completed
		for (int i = 0; i < currentTasks.Count; i++)
		{
			if (currentTasks[i].taskToValidate == currentTask)
			{
				currentTasks[i].Completed = true;
				break;
			}
		}

		//Found if all tasks are completed
		for (int i = 0; i < currentTasks.Count; i++)
		{
			if (!currentTasks[i].Completed)
			{
				return;
			}
		}

		AllTasksCompleted();
	}

	private void AllTasksCompleted()
	{
		currentTasks.Clear();
		CompleteAllTasks?.Invoke();
	}

	public void CleanAllTasks()
	{
		if (LeanTween.isTweening(gameObject))
			LeanTween.cancel(gameObject);

		for (int i = 0; i < currentWeareableTools.Count; i++)
		{
			currentWeareableTools[i].OnWeared -= CompleteWeared;
		}

		for (int i = 0; i < currentHoverButtons.Count; i++)
		{
			currentHoverButtons[i].OnStateChanged -= CompleteHoverButtonInteraction;
		}

		for (int i = 0; i < currentTools.Count; i++)
		{
			currentTools[i].tool.ChangeState -= OnChangeToolState;
		}

		CompleteAllTasks = null;
		currentTasks.Clear();
		currentWeareableTools.Clear();
		currentHoverButtons.Clear();
		currentTools.Clear();
		hoverButtonCount = 0;

	}
}
