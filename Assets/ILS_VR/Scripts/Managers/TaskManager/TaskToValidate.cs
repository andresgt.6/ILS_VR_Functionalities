﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TaskToValidate
{
    public Tasks taskToValidate;    

    public bool Completed = false;
}
