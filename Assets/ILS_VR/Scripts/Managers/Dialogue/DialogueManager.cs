﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection.Emit;

public class DialogueManager : MonoBehaviour
{
    public Action DialogueComplete;    

    private int dialogueStackPosition = 0;
    private Dialogue[] dialogueStack;
    private AudioClip dialogueToPlay;
    private float waitTimeForNextDialogueInStack = 0.5f;
    private string maleDialogueFolder = "Sebastian/";
    private string femaleDialogueFolder = "Juan/";
    private float totalDialoguesDuration;

    [SerializeField] private AudioSource maleTrainer;
    [SerializeField] private AudioSource femaleTrainer;

    private AudioSource currentSource = null;
    public AudioSource GetCurrentAudioSource() => currentSource;

    private void Start()
    {
        //      Dialogue[] diag = new Dialogue[2];
        //      diag[0] = new Dialogue(1, 2);
        //      diag[1] = new Dialogue(1, 1);
        //      PlayDialogue(diag);
        //GetDialogueDuration(diag);

        currentSource = maleTrainer;
    }

    public float GetDialogueDuration(Dialogue dialogue)
    {
        return GetDialogueById(dialogue).length;
    }

    public float GetDialogueDuration(Dialogue[] dialogue)
    {
        totalDialoguesDuration = 0;
        for (int i = 0; i < dialogue.Length; i++)
        {
            totalDialoguesDuration += GetDialogueById(dialogue[i]).length;
        }
        return totalDialoguesDuration;
    }

    public void PlayModuleDialog(int moduleId, int trainerId = 2)
    {
        AudioSource currentAudioS = null;

        if (trainerId == 1)
            currentAudioS = maleTrainer;
        else
            currentAudioS = femaleTrainer;

        dialogueToPlay = Resources.Load<AudioClip>(string.Format("Sounds/Dialogues/Juan/Modulo {0}", moduleId));
        StartCoroutine(StartDialogue(currentAudioS));
    }

    public void PlayDialogue(Dialogue[] dialogues)
    {
        this.dialogueStack = null;
        this.dialogueStack = dialogues;
        PlayDialogueStack();
    }

    public void PlayDialogue(Dialogue dialogue)
    {
        GetDialogueById(dialogue);
        StartCoroutine(StartDialogue(GetAudioSourceByTrainer(dialogue)));
    }

    private void PlayDialogueStack()
    {
        if (dialogueStackPosition < dialogueStack.Length)
        {
            StartCoroutine(StartDialogueStack(GetAudioSourceByTrainer(dialogueStack[dialogueStackPosition]), GetDialogueById(dialogueStack[dialogueStackPosition])));
        }
        else
        {
            DialogueCompleted();
        }
    }

    private AudioSource GetAudioSourceByTrainer(Dialogue dialogue)
    {
        if (dialogue.trainer == 1)
            return maleTrainer;
        else
            return femaleTrainer;
    }

    private AudioClip GetDialogueById(Dialogue dialogue)
    {
        string folderToSearch;

        if (dialogue.trainer == 1)
            folderToSearch = maleDialogueFolder;
        else
            folderToSearch = femaleDialogueFolder;
        if (dialogue.subId == null)
            dialogueToPlay = (AudioClip)Resources.Load("Sounds/Dialogues/" + folderToSearch + dialogue.id);
        else
            dialogueToPlay = (AudioClip)Resources.Load("Sounds/Dialogues/" + folderToSearch + dialogue.id + "." + dialogue.subId);

        if (dialogueToPlay == null)
            Debug.LogError("No se encontró el Audio");

        return dialogueToPlay;
    }

    private IEnumerator StartDialogue(AudioSource characterToPlay)
    {
        SetDialogToPlay(characterToPlay);

        while (characterToPlay.isPlaying)
        {
            yield return new WaitForSeconds(0.1f);
        }
        if (!characterToPlay.isPlaying)
        {
            yield return new WaitForSeconds(0.5f);
            DialogueCompleted();
        }
    }


    private IEnumerator StartDialogueStack(AudioSource characterToPlay, AudioClip dialogueToPlay)
    {
        SetDialogToPlay(characterToPlay);

        yield return new WaitForSeconds(characterToPlay.clip.length);        

        if (!characterToPlay.isPlaying)
        {
            yield return new WaitForSeconds(waitTimeForNextDialogueInStack);
            if (dialogueStackPosition < dialogueStack.Length)
            {
                dialogueStackPosition += 1;
                PlayDialogueStack();
            }
        }
    }

    private void SetDialogToPlay(AudioSource characterToPlay)
    {
        if (!characterToPlay.isPlaying)
        {
            characterToPlay.clip = dialogueToPlay;
            characterToPlay.Play();
        }
        else
        {
            characterToPlay.Stop();
            characterToPlay.clip = dialogueToPlay;
            characterToPlay.Play();
        }

        currentSource = characterToPlay;
    }

    private void DialogueCompleted()
    {
        StopAllCoroutines();
        if (DialogueComplete != null)
        {
            DialogueComplete();
        }
    }

    public void CancelAll()
    {
        StopAllCoroutines();
        dialogueStack = null;
        dialogueToPlay = null;
    }
}
