﻿[System.Serializable]
public class Dialogue
{
    public int id;
    public int trainer = 2; //1 male, 2 female
    public int? subId;
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="id">id of dialogue</param>
    /// <param name="trainer">trainer to reproduce the dialogue: 1 male, 2 female</param>
    /// <param name="subId">sub id of the dialogue, Ex: 50.1, where 1 is the subId </param>
    public Dialogue(int id, int trainer, int? subId = null)
    {
        this.id = id;
        this.trainer = trainer;
        this.subId = subId;
    }
}
