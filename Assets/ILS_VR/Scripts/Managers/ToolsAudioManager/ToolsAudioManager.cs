﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ToolsAudioManager : MonoBehaviour
{
    /// <summary>
    /// Call if want to play a random tool sound based in the texture
    /// </summary>
    public static Action<ToolAudioTexture> PlayGenericToolSound;

    /// <summary>
    /// Call if want to play a specific tool sound
    /// </summary>
    public static Action<ToolSound> PlayToolSound;

    [SerializeField] private AudioSource toolAudioSource;
    private AudioClip toolAudioToPlay;

    private string genericFolder = "Sounds/SFX/Generic/";
    private string metalFolder = "Metallic/Metal_";
	private string clothFolder = "Cloth/Cloth_";
    private string combinedFolder = "Combined/Combined_";
    private string variousFolder = "Various/Various_";

    
    private int metalTotalAudios = 1;
    private int clothTotalAudios = 4;
    private int combinedTotalAudios = 1;
    private int variousTotalAudios = 1;



    private void Start()
    {
        PlayGenericToolSound += OnPlayGenericToolSound;
        PlayToolSound += OnPlayToolSound;
    }

    private void OnPlayToolSound(ToolSound toolSound)
    {
        toolAudioSource.clip = GetSpecificToolSound(toolSound);
        toolAudioSource.Play();
    }

    private void OnPlayGenericToolSound(ToolAudioTexture toolAudioType)
    {
        toolAudioSource.clip = GetGenericToolSound(toolAudioType);
        toolAudioSource.Play();
    }

    private AudioClip GetSpecificToolSound(ToolSound toolSound)
    {
        string soundPath = String.Format("Sounds/SFX/Specific/{0}_{1}_{2}", toolSound.toolAudioTexture, toolSound.toolAudioType, toolSound.id);
        Debug.Log("sound path: " + soundPath);
        toolAudioToPlay = (AudioClip) Resources.Load(soundPath);
        return toolAudioToPlay;
    }

    private AudioClip GetGenericToolSound(ToolAudioTexture toolAudioType)
    {
        string folderToSearch = "";
        int randomToolAudio = 1;

        switch (toolAudioType)
        {
            case ToolAudioTexture.Metal:
                folderToSearch = genericFolder + metalFolder;
                randomToolAudio = UnityEngine.Random.Range(1, metalTotalAudios + 1);
            break;
            case ToolAudioTexture.Cloth:
                folderToSearch = genericFolder + clothFolder;
                randomToolAudio = UnityEngine.Random.Range(1, clothTotalAudios + 1);
            break;
            case ToolAudioTexture.Combined:
                folderToSearch = genericFolder + combinedFolder;
                randomToolAudio = UnityEngine.Random.Range(1, combinedTotalAudios + 1);
            break;
            default:
            break;
        }
        toolAudioToPlay = (AudioClip) Resources.Load(folderToSearch + randomToolAudio);

        return toolAudioToPlay;
    }

    private void OnDestroy()
    {
        PlayGenericToolSound -= OnPlayGenericToolSound;
        PlayToolSound -= OnPlayToolSound;
    }
}
