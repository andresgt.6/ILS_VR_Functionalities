using UnityEngine;
public class ToolSound
{
    public ToolAudioTexture toolAudioTexture;
    public ToolAudioType toolAudioType;
    public int id;
    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="toolAudioTexture">Metal, cloth or combined</param>
    /// <param name="toolAudioType">Grab or drop</param>
    /// <param name="id">Identifier of the audio in resources</param>
    public ToolSound(ToolAudioTexture toolAudioTexture, ToolAudioType toolAudioType, int id)
    {
        this.toolAudioTexture = toolAudioTexture;
        this.toolAudioType = toolAudioType;
        this.id = id;
    }
}
