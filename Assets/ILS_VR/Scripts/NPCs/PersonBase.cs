﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PersonBase : MonoBehaviour
{
    public enum State
    {
        Idle,
        Walking,
        Talking,
        Showing,        
        Rotating,        
        ExplainingObject,
        GoodBye
    }

    [Header("Current state")]
    [SerializeField] private State currentState;
    [SerializeField] private Transform currentObjective;
    [SerializeField] private List<NPCActionSettings> currentActions;

    public event Action OnCompleteAllActions;

    [Space, Header("Settings")]
    [SerializeField] private float lootToTolerance = 5;
    [SerializeField] private float walkSpeed = 2;
    [SerializeField] private List<Transform> wantedTools;
    [SerializeField] private Collider itemSensor;

    [Header("Main Stuffs")]
    [SerializeField] private Transform[] positionPoints;

	//[SerializeField] private CarryObject leftHand;
	//[SerializeField] private CarryObject rightHand;

	private Transform firstParent;

    private PersonAnimator animator;

    private Transform targetedTool;
    private Transform playerEyes;

    private Coroutine idleCoroutine;
    private Coroutine talkCoroutine;

    private NPCActionSettings currentAction = null;

    private readonly Vector3 up = Vector3.up;

    private PersonIKManager personIk;

    private float oldIdleParameter;
    private float oldTalkParameter;

    private void Awake()
    {
        animator = GetComponentInChildren<PersonAnimator>();
        personIk = animator.GetComponent<PersonIKManager>();
        itemSensor.enabled = false;
        itemSensor.isTrigger = true;
        playerEyes = Camera.main.transform;
        //personIk.ActiveIKSystem(new Vector3(1, 10, 1), AIHandActionType.LeftHanded);
        idleCoroutine = StartCoroutine(IdleCoroutine());
        talkCoroutine = StartCoroutine(TalkCoroutine());
		firstParent = transform.parent;

	}

    private void ChangeState(State state)
    {
        currentState = state;

        switch (state)
        {
            case State.Idle:
                if (idleCoroutine == null)
                {
                    idleCoroutine = StartCoroutine(IdleCoroutine());
                }
                break;

            case State.Walking:
                break;
            case State.Talking:
                if (talkCoroutine == null)
                {
                    talkCoroutine = StartCoroutine(TalkCoroutine());
                }
                break;
            case State.Showing:
                break;            
            case State.Rotating:
                break;
            default:
                break;
        }
    }

    //public void CarryObject(Transform tool, bool right = true)
    //{
    //    if (right && rightHand)
    //    {
    //        rightHand.SetObject(tool);
    //    }
    //    if (!right && leftHand)
    //    {
    //        leftHand.SetObject(tool);
    //    }
    //}

    //public void ReleaseObject()
    //{
    //    if (rightHand)
    //        rightHand.ReleaseObject();
    //    if (leftHand)
    //        leftHand.ReleaseObject();
    //}

	public void SetParent(Transform parent)
	{
		if(parent)
		{
			transform.SetParent(parent);
		}
		else
		{
			transform.SetParent(firstParent);
		}
	}

    private IEnumerator IdleCoroutine()
    {
        int timeToChangeAnimation = UnityEngine.Random.Range(3, 10);
        float idleParameter = UnityEngine.Random.Range(0, 2);
        switch (idleParameter)
        {
            case 0:
                idleParameter = 0;
                break;
            case 1:
                idleParameter = 0.33f;
                break;
            default:
                break;
        }
        yield return new WaitForSeconds(timeToChangeAnimation);
        //print("Idle en " + idleParameter);
        if (currentState == State.Idle || currentState == State.Talking)
        {

            if (idleParameter != oldIdleParameter)
            {
                LeanTween.value(gameObject, value => animator.SetFloatParameter(value, "IdleVariation"), oldIdleParameter, idleParameter, 1);
                oldIdleParameter = idleParameter;

            }
            else
            {
                animator.SetFloatParameter(idleParameter, "IdleVariation");
            }
        }
        idleCoroutine = StartCoroutine(IdleCoroutine());
    }

    private IEnumerator TalkCoroutine()
    {
        int timeToChangeAnimation = UnityEngine.Random.Range(2, 6);
        float talkParameter = UnityEngine.Random.Range(0, 3);
        switch (talkParameter)
        {
            case 0:
                talkParameter = 0;
                break;
            case 1:
                talkParameter = 0.5f;
                break;
            case 2:
                talkParameter = 1f;
                break;
            default:
                break;
        }
        yield return new WaitForSeconds(timeToChangeAnimation);
        //print("Idle en " + idleParameter);
        if (talkParameter != oldTalkParameter)
        {
            LeanTween.value(gameObject, value => animator.SetFloatParameter(value, "TalkVariation"), oldTalkParameter, talkParameter, 1);
            oldTalkParameter = talkParameter;

        }
        else
        {
            animator.SetFloatParameter(talkParameter, "TalkVariation");
        }
        talkCoroutine = StartCoroutine(TalkCoroutine());
    }

    /// <summary>
    /// Set the AI action, if you called multiple times it will storage all the actions.
    /// </summary>
    /// <param name="action">Aditional settings, if you set it null it will take the default settings</param>
    /// <param name="customAIAction">if you want to define a custom action</param>
    public void SetAction(NPCActionSettings action = null, AIActions? customAIAction = null, Transform customTarget = null)
    {
        NPCActionSettings currentAction = new NPCActionSettings();
        currentAction.objectToInteract = action.objectToInteract;
        currentAction.action = action.action;
        currentAction.completeCallback = action.completeCallback;
        currentAction.delayToStart = action.delayToStart;
        currentAction.duration = action.duration;

        if (customAIAction.HasValue)
            currentAction.action = customAIAction.Value;

        if (customTarget)
            currentAction.objectToInteract = customTarget;

        currentActions.Add(currentAction);

        if (this.currentAction == null)
        {
            ConfigAction(currentAction);
            LeanTween.delayedCall(currentAction.delayToStart, SelectNextAction);
        }

        //Debug.Log("Add action " + currentActions.Count);

        //if (automaticStartAction)
        //    SelectNextAction();
    }

    private void ConfigAction(NPCActionSettings action)
    {
        currentAction = action;
        OnCompleteAllActions = currentAction.completeCallback;
        currentObjective = currentAction.objectToInteract;
    }

    private void SelectNextAction()
    {
        //print("Action start!");

        if (currentAction == null)
            return;

        Action animationCallback = null;

        switch (currentAction.action)
        {
            case AIActions.Walk:
                WalkToPlace(currentObjective);
                break;
            case AIActions.Talk:
                TalkToPlayer();
                break;
            case AIActions.ExplainObject:
                ExplainObject();
                break;
            case AIActions.Teleport:
                TeleportToPlace(currentObjective, playerEyes.position);
                break;
            case AIActions.Rotate:
                StartRotating();
                break;

            case AIActions.Goodbye:
                break;

            default:
                break;
        }

        if (currentObjective)
            animator.StartAnimation(currentAction.action, currentObjective.position, animationCallback);

    }

    private void CompleteCurrentAction()
    {        
        currentState = State.Idle;

        currentActions.Remove(currentAction);

        if (currentActions.Count == 0)
        {
            //On Complete All actions
            Vector3 foward = transform.forward;
            Vector3 direction = playerEyes.position - transform.position;

            foward.y = 0;
            direction.y = 0;

            OnCompleteAllActions?.Invoke();
            OnCompleteAllActions = null;

            currentAction = null;
        }
        else
        {
            //If remains actions
            ConfigAction(currentActions[0]);
            LeanTween.delayedCall(currentAction.delayToStart, SelectNextAction);
        }

        if (currentState != State.Talking)
        {
            animator?.DesactiveAnimations();
        }
    }

    public void SetDefault()
    {
        currentAction = null;
        currentActions.Clear();
        animator?.DesactiveAnimations();
        currentState = State.Idle;
    }

    private void CheckForActionCompleted()
    {

        CompleteCurrentAction();
    }

    public void StartRotating()
    {
        transform.LookAt(currentObjective);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
        CheckForActionCompleted();
    }

    private void OnCompleteAnimation()
    {
        CheckForActionCompleted();
    }

    public void WalkToPlace(Transform place, bool _lookToPlace = false, Vector3 _placeToLook = new Vector3())
    {
        currentObjective = place;

        transform.LookAt(place);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);

        LeanTween.move(gameObject, place.position, 2).setSpeed(walkSpeed).setOnComplete(OnCompleteMovement);

        currentState = State.Walking;
    }

    private void OnCompleteMovement()
    {
        Vector3 place = currentObjective.position;
        place.y = transform.position.y;
        transform.LookAt(Vector3.Lerp(transform.position + transform.forward, place, Time.deltaTime), transform.up);

        if (currentState == State.Walking)
            currentState = State.Idle;

        CheckForActionCompleted();
    }

    public void TeleportToPlace(int placeID, Vector3 _placeToLook = new Vector3())
    {
        Debug.Log("StartTeleport");
        animator?.DesactiveAnimations();
        animator.transform.localPosition = Vector3.zero;
        animator.transform.localEulerAngles = Vector3.zero;
        transform.position = positionPoints[placeID].position;

        CompleteCurrentAction();
        transform.rotation = positionPoints[placeID].rotation;
    }

    public void TeleportToPlace(Transform place, Vector3 _placeToLook = new Vector3())
    {
        Debug.Log("StartTeleport");
        transform.position = place.position;

        CompleteCurrentAction();
        transform.rotation = place.rotation;
    }

    public virtual void TalkToPlayer()
    {
        currentState = State.Talking;
        if (currentAction.duration.HasValue)
            LeanTween.delayedCall(currentAction.duration.Value, CheckForActionCompleted);
    }

    public virtual void RefuseItem()
    {
        Debug.Log("Que no ome!, que no!");
    }

    public virtual void AcceptItem(Transform tool)
    {
        wantedTools.Remove(tool);
        tool.GetComponent<Collider>().isTrigger = true;
        targetedTool = null;
        animator.StopHandFollowingItem();
        //animator.GetComponent<PersonIKManager>().PutObjectInHand(tool);
        //tool.OnAttachCompleted += ItemRecieved;

        if (wantedTools.Count == 0)
        {
            itemSensor.enabled = false;

            CheckForActionCompleted();
        }
    }

    //TODO:revisar al recibir un item 
    private void ItemRecieved(ILSOculusGrabbable _object)
    {
        //_object.OnAttachCompleted -= ItemRecieved;

        //GrabbableTool tool = _object.GetComponent<GrabbableTool>();

        //if(tool != null)
        //{
        //    tool.SetActiveGrab(false);
        //    LeanTween.delayedCall(0.25f, tool.SetToolAsRecieved);
        //}

    }

    public virtual void ExplainObject()
    {
        ChangeState(State.ExplainingObject);
        if (currentAction.duration.HasValue)
            LeanTween.delayedCall(currentAction.duration.Value, CheckForActionCompleted);

    }

    public virtual void SetActiveLookAt(bool isActive, Transform _target = null)
    {
        personIk.SetActiveLookAtSystem(isActive, _target);
    }

    public virtual void AnimateAnswer(bool isRigthAnswer)
    {
        animator.ChangeAnimation(isRigthAnswer ? "GoodJob" : "Sure", true);
    }

    //TODO: revisar como reemplazar el tool
    private void OnTriggerEnter(Collider other)
    {
        //GrabbableTool tool = other.GetComponent<GrabbableTool>();
        Transform tool = other.transform;

        if (tool != null)
        {
            Debug.Log("Tool entered");

            bool isWantedTool = false;

            for (int i = 0; i < wantedTools.Count; i++)
            {
                //if(tool.ToolType == wantedTools[i])
                //{
                //    isWantedTool = true;
                //    targetedTool = tool;
                //    Debug.Log("Tool found");
                //    break;
                //}
            }

            if (isWantedTool)
                animator.StartHandFollowingItem(other.transform);
            else
                RefuseItem();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //GrabbableTool tool = other.GetComponent<GrabbableTool>();

        //if (tool != null && !tool.isGrabbed && tool == targetedTool)
        //    AcceptItem(tool);
    }

    private void OnTriggerExit(Collider other)
    {
        //GrabbableTool tool = other.GetComponent<GrabbableTool>();

        //if (tool != null && tool == targetedTool)
        //{
        //    Debug.Log("Tool exited");
        //    animator.StopHandFollowingItem();
        //}
    }

    public void RemoveAllActions()
    {
        currentActions.Clear();
    }
}
