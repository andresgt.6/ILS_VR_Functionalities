﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class NPCActionSettings
{
    public Transform objectToInteract;
    public AIActions action = AIActions.Talk;
    public Action completeCallback;            
    public float delayToStart = 0;
    public float? duration = 5;    
}
