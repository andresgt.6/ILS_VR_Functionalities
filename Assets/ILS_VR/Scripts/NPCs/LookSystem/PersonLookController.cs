﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using System;

public enum LookingType
{
    Idle,
    LookingTransform,
    LookingPosition,
    LookingInRandomSpots,
}

public class PersonLookController : MonoBehaviour
{
    public Action CompleteAllLookActions;
    public Action CompleteLookAction;

    [Header("Current State")]
    [SerializeField] private LookingType currentState = LookingType.Idle;

    [SerializeField] private List<NPCLookSettings> currentLookSettings = new List<NPCLookSettings>();

    public Transform LookAtMePoint { get; private set; }

    private NPCLookSettings currentSettings = null;

    private PersonIKManager personIk;
    private Transform headTarget;

    private Coroutine lookSpots;

    private void Awake()
    {
        personIk = GetComponentInChildren<PersonIKManager>();
        headTarget = GameObject.Instantiate(new GameObject(), transform).transform;
        LookAtMePoint = GameObject.Instantiate(new GameObject(), transform).transform;
        LookAtMePoint.localPosition = new Vector3(0, 1.8f, 0);
    }

    public void AddAction(NPCLookSettings currentLook)
    {
        NPCLookSettings look = new NPCLookSettings();
        look = new NPCLookSettings();
        look.completeCallback = currentLook.completeCallback;
        look.lookObject = currentLook.lookObject;
        look.vectorToLook = currentLook.vectorToLook;
        look.enabled = currentLook.enabled;
        look.delay = currentLook.delay;
        look.duration = currentLook.duration;
        look.lookType = currentLook.lookType;
        look.spotsToLook = currentLook.spotsToLook;

        currentLookSettings.Add(look);

        if (currentSettings == null)
        {
            ConfigAction(look);
            LeanTween.delayedCall(look.delay, StartLooking);
        }
    }

    private void ConfigAction(NPCLookSettings look)
    {
        currentSettings = look;
    }

    private void StartLooking()
    {
        //print("Empezó");

        if (currentSettings == null)
            return;

        if (currentSettings.enabled)
        {
            if (!currentSettings.lookType.HasValue)
            {
                if (currentSettings.lookObject)
                {
                    LookToPlace(true, currentSettings.lookObject, currentSettings.duration);
                }
                if (currentSettings.vectorToLook.HasValue)
                {
                    LookToPlace(true, currentSettings.vectorToLook.Value, currentSettings.duration);
                }
            }
            else
            {
                switch (currentSettings.lookType.Value)
                {
                    case LookingType.Idle:
                        break;
                    case LookingType.LookingTransform:
                        LookToPlace(true, currentSettings.lookObject, currentSettings.duration);
                        break;
                    case LookingType.LookingPosition:
                        LookToPlace(true, currentSettings.vectorToLook.Value, currentSettings.duration);
                        break;
                    case LookingType.LookingInRandomSpots:
                        LookToPlace(true, currentSettings.spotsToLook, currentSettings.duration);
                        break;
                    default:
                        break;
                }
            }

        }
        else
        {
            LookToPlace(false, transform);
        }
    }

    public void LookToPlace(bool enabled, Transform objectToLook = null, float? duration = null)
    {
        currentState = LookingType.LookingTransform;

        personIk.SetActiveLookAtSystem(enabled, objectToLook);

        if (duration.HasValue)
            LeanTween.delayedCall(duration.Value, () => { LookToPlace(false, transform); CompleteLookingAction(); });
    }

    public void LookToPlace(bool enabled, Vector3? customPos = null, float? duration = null)
    {
        currentState = LookingType.LookingPosition;

        if (customPos.HasValue)
            headTarget.transform.position = customPos.Value;

        personIk.SetActiveLookAtSystem(enabled, headTarget.transform);

        if (duration.HasValue)
            LeanTween.delayedCall(duration.Value, () => { LookToPlace(false, transform); CompleteLookingAction(); });
    }

    public void LookToPlace(bool enabled, List<Vector3> spots, float? duration)
    {
        currentState = LookingType.LookingInRandomSpots;

        if (spots.Count > 0 && gameObject.activeInHierarchy)
        {
            lookSpots = StartCoroutine(SetLookSpots(spots));
            LeanTween.delayedCall(duration.Value, StopLookSpots);
        }

        if (duration.HasValue)
            LeanTween.delayedCall(duration.Value, () => { LookToPlace(false, transform); CompleteLookingAction(); });
    }

    private IEnumerator SetLookSpots(List<Vector3> spots)
    {
        int timeToLook = UnityEngine.Random.Range(1, 8);
        int idSpotToLook = UnityEngine.Random.Range(0, spots.Count);

        headTarget.transform.position = spots[idSpotToLook];

        //Debug.Log("Mirando a:" + spots[idSpotToLook]);

        personIk.SetActiveLookAtSystem(enabled, headTarget.transform);

        yield return new WaitForSeconds(timeToLook);
        if (lookSpots != null)
        {
            lookSpots = StartCoroutine(SetLookSpots(spots));
        }
    }

    private void StopLookSpots()
    {
        if (lookSpots == null)
            return;

        StopCoroutine(lookSpots);
        lookSpots = null;
    }

    private void CompleteLookingAction()
    {
        CompleteLookAction?.Invoke();

        currentState = LookingType.Idle;

        currentLookSettings.Remove(currentSettings);

        currentSettings = null;

        if (currentLookSettings.Count > 0)
        {
            ConfigAction(currentLookSettings[0]);
            if (currentLookSettings[0].duration.HasValue)
                LeanTween.delayedCall(currentLookSettings[0].duration.Value, StartLooking);
        }
        else
        {
            CompleteAllLook();
        }
    }

    public void SetDefault()
    {
        currentState = LookingType.Idle;
        currentLookSettings.Clear();
        currentSettings = null;
        StopLookSpots();
    }

    private void CompleteAllLook()
    {
        CompleteAllLookActions?.Invoke();
    }
}
