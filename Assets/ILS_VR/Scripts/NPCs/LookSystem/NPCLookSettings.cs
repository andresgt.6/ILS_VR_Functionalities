﻿
using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class NPCLookSettings
{   
    /// <summary>
    /// Called when complete looking
    /// </summary>
    public Action completeCallback = null;
    public Transform lookObject = null;
    public Vector3? vectorToLook = null;
    public List<Vector3> spotsToLook;
    public LookingType? lookType = null;
    public bool enabled = true;
    public float delay = 0;
    public float? duration = 5;
}
