﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PersonAnimator : AnimatorManager
{
    private event Action OnCompleteCurrentAnimation;

    private PersonIKManager ikManager;

    protected override void Awake()
    {
        ikManager = GetComponent<PersonIKManager>();
        base.Awake();
    }

    public void SetFloatParameter(float value, string animationParameter)
    {
        animator.SetFloat(animationParameter, value);
    }

    public void StartAnimation(AIActions action, Vector3 targetPosition, Action _onComplete = null, bool isTrigger = false)
    {
        if(_onComplete != null)
            OnCompleteCurrentAnimation = _onComplete;        

        switch (action)
        {
            case AIActions.Walk:
                break;
            case AIActions.Talk:
                //ikManager.ActiveIKSystem(targetPosition, AIHandActionType.RightHanded);
                break;                        
            case AIActions.ExplainObject:                
                break;

            case AIActions.Ok_Gesture:
                break;

            default:
                break;
        }        

        for (int i = 0; i < animator.parameterCount; i++)
        {
            if (animator.parameters[i].name == action.ToString())
            {                
                ChangeAnimation(action, isTrigger);
                break;
            }
        }
    }

    protected override void OnCompleteAnimation(string animationName)
    {
        base.OnCompleteAnimation(animationName);

        if(OnCompleteCurrentAnimation != null)
        {
            OnCompleteCurrentAnimation();
            OnCompleteCurrentAnimation = null;
        }
    }

    public override void DesactiveAnimations()
    {
        for (int i = 0; i < animator.parameterCount; i++)
            if (animator.parameters[i].type == AnimatorControllerParameterType.Bool && animator.parameters[i].name != "OnGround")
                animator.SetBool(animator.parameters[i].name, false);

        ikManager.DeactiveIKSystem();
    }

    public void StartHandFollowingItem(Transform item, AIHandActionType _handActionType = AIHandActionType.None)
    {
        ikManager.StartHandsConstantObjectFollowing(item, _handActionType);
    }

    public void StopHandFollowingItem()
    {
        ikManager.DeactiveIKSystem();
    }
}
