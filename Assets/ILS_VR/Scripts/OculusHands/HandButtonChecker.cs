﻿using UnityEngine;
using UnityEngine.UI;

public class HandButtonChecker : MonoBehaviour
{
    [SerializeField] private Side handSide = Side.RightSide;
    [SerializeField] private float TimeToInteract = 1;
    
    private Image proggressBar = null;

    private HoverButton currentButton = null;

    private float currentHoverTime = 0;

    private void Awake()
    {
        proggressBar = GetComponentInChildren<Image>();
        proggressBar.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        HoverButton button = null;

        SetNullCurrentButton();

        if (other.TryGetComponent(out HoverButton hoverButton) || 
            (other.transform.parent != null && other.transform.parent.TryGetComponent(out hoverButton)))
        {
            button = hoverButton;
        }

        if (button == null)
            return;

        if (currentButton == null && button.CanInteract)
            SetNewCurrentButton(button);
    }

    private void OnTriggerStay(Collider other)
    {
        HoverButton button = null;

        if (other.TryGetComponent(out HoverButton hoverButton) ||
            (other.transform.parent != null && other.transform.parent.TryGetComponent(out hoverButton)))
        {
            button = hoverButton;
        }

        if (button == null)
            return;

        if(currentButton == null)
        {
            if(button.CanInteract)
                SetNewCurrentButton(button);
        }
        else if (currentHoverTime < TimeToInteract && currentButton == button)
        {
            currentHoverTime += Time.fixedDeltaTime;
            proggressBar.fillAmount = currentHoverTime / TimeToInteract;

            if (currentHoverTime >= TimeToInteract)
                TryInteractWithButton();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        HoverButton button = null;

        if (other.TryGetComponent(out HoverButton hoverButton) ||
            (other.transform.parent != null && other.transform.parent.TryGetComponent(out hoverButton)))
        {
            button = hoverButton;
        }

        if (button == null)
            return;

        if (currentButton == button)
            SetNullCurrentButton();
    }


    private void TryInteractWithButton()
    {
        proggressBar.enabled = false;

        if(currentButton != null)
        {
            currentButton.SetButtonHoverState(false);
            currentButton.InteractWithButton(handSide);
        }
    }

    private void SetNewCurrentButton(HoverButton _button)
    {
        currentButton = _button;
        currentHoverTime = 0;
        currentButton.SetButtonHoverState(true);
        proggressBar.enabled = true;
        proggressBar.fillAmount = 0;
    }

    private void SetNullCurrentButton()
    {
        currentButton?.SetButtonHoverState(false);
        currentButton = null;
        currentHoverTime = 0;
        proggressBar.enabled = false;
    }
}
