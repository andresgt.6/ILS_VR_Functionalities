﻿using System;
using UnityEngine;

public class WeareableTool : HoverButton
{
    public event Action<WearableType> OnWeared;

    public WearableType wearableType = WearableType.Others;

    public void ActiveWeareable()
    {
        ActiveInteractions(1);
    }

    protected override void ActiveButton()
    {
        base.ActiveButton();
        OnWeared?.Invoke(wearableType);

        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
            renderers[i].enabled = false;
    }

    protected override void DeactiveButton()
    {
        base.DeactiveButton();

        OnWeared?.Invoke(wearableType);

        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
            renderers[i].enabled = false;
    }
}
