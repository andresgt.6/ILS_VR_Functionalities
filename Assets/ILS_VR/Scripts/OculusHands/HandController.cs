﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HandController : MonoBehaviour
{
    public enum HandSide
    {
        Unasigned,
        Left,
        Rigth
    }

    public enum State
    {
        Untracked,
        Tracked,
        Enabled
    }

	public event Action<State> ChangeState;

	[Header("Current state")]
    public State currentState;

    [Header("Hand settings")]
    public HandSide handSide;
    public Transform comunicatorpoint;
	public Transform gasDetectorpoint;


	[SerializeField] private float timeToReactivateHands = 3;

    [Header("Hand main components")]    
    [SerializeField] private HandButtonChecker buttonChecker;
    [SerializeField] private ILSOculusGrabber grabber;
    //private OVRHand oVRHand;

	public Transform pencilPointRight;
	public Transform pencilPointLeft;

	private float handTrackerTimer;

    private void Awake()
    {
        //oVRHand = GetComponent<OVRHand>();
    }

    private void Update()
    {
        //if(oVRHand.IsTracked)
        //{
        //    switch (currentState)
        //    {
        //        case State.Untracked:
        //            currentState = State.Tracked;
        //            break;
        //        case State.Tracked:
        //            handTrackerTimer += Time.deltaTime;
        //            if(handTrackerTimer >= timeToReactivateHands)
        //            {
        //                TrackedHand();
        //                currentState = State.Enabled;
        //            }
        //            break;
        //        case State.Enabled:
        //            break;
        //        default:
        //            break;
        //    }
        //}
        //else
        //{
        //    handTrackerTimer = 0;
        //    currentState = State.Untracked;
        //    UnTrackedHand();
        //}
    }

    private void TrackedHand()
    {
        ChangeState?.Invoke(State.Enabled);                
    }

    private void UnTrackedHand()
    {
        handTrackerTimer = 0;
        ChangeState?.Invoke(State.Untracked);                
        //grabber.ForceRelease(null);
    }
    
}
