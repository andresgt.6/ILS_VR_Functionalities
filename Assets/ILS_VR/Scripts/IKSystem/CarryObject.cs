﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryObject : MonoBehaviour
{
    private enum Hand
    {
        leftHand,
        RigthHand
    }

    [SerializeField] private Hand hand;
    public Transform currentObject;

    public void SetObject(Transform objectToSet)
    {
        currentObject = objectToSet;
        objectToSet.SetParent(transform);        
    }

    private void Update()
    {
        if (!currentObject)
            return;
        currentObject.transform.localPosition = Vector3.zero;
    }

    public void ReleaseObject()
    {
        //currentObject.SetParent(null);
        if(currentObject)
            currentObject = null;
    }
}
