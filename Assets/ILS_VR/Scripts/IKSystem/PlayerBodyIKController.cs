﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBodyIKController : MonoBehaviour
{
    [SerializeField] private Transform leftHandTarget = null;
    [SerializeField] private Transform rightHandTarget = null;
    [SerializeField] private Transform leftElbowTarget = null;
    [SerializeField] private Transform righElbowTarget = null;
    [SerializeField] private LayerMask raycastLayer;

    [Space]
    [SerializeField] private Transform playerHead = null;
    [SerializeField] private float movementSpeed = 2f;
    [SerializeField] private float bodyLerpSpeed = 4f;

    private bool isActiveIK;
    private bool hasTargets;

    private float leftHandWeight = 0;
    private float rightHandWeight = 0;

    private float activationWeight;
    private float deactivationWeight;

    private Animator animator;

    private Vector3 bodyOffsetFromHead;

    private AIHandActionType handActionType;


    ///  Hands constant following ////////////////////
    private Transform rightHandFollowTarget;
    private Transform leftHandFollowTarget;

    private bool areHandsFollowingTarget;

    void Awake()
    {
        animator = GetComponent<Animator>();

        bodyOffsetFromHead = transform.position - playerHead.position;

    }

    public void DeactiveIKSystem()
    {
        deactivationWeight = rightHandWeight > leftHandWeight ? rightHandWeight : leftHandWeight;
        hasTargets = false;
        areHandsFollowingTarget = false;
        rightHandFollowTarget = null;
        leftHandFollowTarget = null;
        handActionType = AIHandActionType.None;
    }

    private void FixedUpdate()
    {
        if (isActiveIK)
        {
            //transform.position = Vector3.Lerp(transform.position, playerHead.position + bodyOffsetFromHead, Time.fixedDeltaTime*bodyLerpSpeed);

            if (hasTargets)
                SetHandsPosition();
            else
                SetHandsAtRestPosition();
        }
    }

    private void SetHandsPosition()
    {
        if (activationWeight < 1)
        {
            activationWeight += Time.fixedDeltaTime * movementSpeed;

            if (activationWeight > 1)
                activationWeight = 1;

            leftHandWeight = handActionType == AIHandActionType.LeftHanded || handActionType == AIHandActionType.BothHands ? activationWeight : 0;
            rightHandWeight = handActionType == AIHandActionType.RightHanded || handActionType == AIHandActionType.BothHands ? activationWeight : 0;
        }

        if (areHandsFollowingTarget)
            SetHandsOffset();
    }

    private void SetHandsAtRestPosition()
    {
        deactivationWeight -= Time.fixedDeltaTime;

        leftHandWeight = leftHandWeight > deactivationWeight ? deactivationWeight : leftHandWeight;
        rightHandWeight = rightHandWeight > deactivationWeight ? deactivationWeight : rightHandWeight;

        if (deactivationWeight <= 0)
        {
            deactivationWeight = 0;
            leftHandWeight = 0;
            rightHandWeight = 0;
            isActiveIK = false;
        }

        activationWeight = deactivationWeight;
    }

    private void SetHandsOffset(bool LerpPosition = false)
    {
        if (handActionType == AIHandActionType.LeftHanded || handActionType == AIHandActionType.BothHands)
        {            
            if (!LerpPosition)
                leftHandTarget.position = leftHandFollowTarget.position;
            else
                leftHandTarget.position = Vector3.Lerp(leftHandTarget.position, leftHandFollowTarget.position, Time.fixedDeltaTime * movementSpeed);
        }

        if (handActionType == AIHandActionType.RightHanded || handActionType == AIHandActionType.BothHands)
        {
            if (!LerpPosition)
                rightHandTarget.position = rightHandFollowTarget.position;
            else
                rightHandTarget.position = Vector3.Lerp(rightHandTarget.position, rightHandFollowTarget.position, Time.fixedDeltaTime * movementSpeed);
        }
    }

    public void ActiveHandsFollow(AIHandActionType _handActionType = AIHandActionType.BothHands, Transform _rightHandFollowTarget = null, Transform _leftHandFollowTarget = null)
    {

        if (_handActionType != AIHandActionType.None)
        {
            areHandsFollowingTarget = true;
            rightHandFollowTarget = _rightHandFollowTarget;
            leftHandFollowTarget = _leftHandFollowTarget;
            handActionType = _handActionType;
            isActiveIK = true;
            hasTargets = true;
            deactivationWeight = 1;
        }
        else
            DeactiveIKSystem();

    }

    /// This is called from the animator 
    private void OnAnimatorIK(int layerIndex)
    {
        if (isActiveIK)
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandWeight);
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, rightHandWeight);

            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandWeight);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, rightHandWeight);

            animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.position);
            animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);

            animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandTarget.rotation);
            animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandTarget.rotation);

            animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, leftHandWeight);
            animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, rightHandWeight);

            animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, leftHandWeight);
            animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, rightHandWeight);

            animator.SetIKHintPosition(AvatarIKHint.LeftElbow, leftElbowTarget.position);
            animator.SetIKHintPosition(AvatarIKHint.RightElbow, righElbowTarget.position);

            animator.SetLookAtPosition(playerHead.position + playerHead.forward);
            animator.SetLookAtWeight(1);

        }
    }
}
