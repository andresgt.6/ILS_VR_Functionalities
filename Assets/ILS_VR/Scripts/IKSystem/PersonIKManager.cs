﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AIHandActionType
{
    LeftHanded,
    RightHanded,
    BothHands,
    None
}

public class PersonIKManager : MonoBehaviour
{
    [Header("Targets")]
    [SerializeField] private Transform leftHandTarget = null;
    [SerializeField] private Transform rightHandTarget = null;
    [SerializeField] private Transform leftElbowTarget = null;
    [SerializeField] private Transform righElbowTarget = null;
    [SerializeField] private Transform headFollowTarget;

    [Space,Header("Settings")]
    [SerializeField] private LayerMask raycastLayer;

    [SerializeField] private Vector3 rightHandAimOffset = new Vector3(0, -0.25f, 0);
    [SerializeField] private Vector3 leftHandAimOffset = new Vector3(0, -0.25f, 0);
    [SerializeField] private float movementSpeed = 2f;

    private Transform leftShoulder;
    private Transform rightShoulder;

    private bool isActiveIK;
    private bool hasTarget;

    private float leftHandWeight = 0;
    private float rightHandWeight = 0;

    private float activationWeight;
    private float deactivationWeight;

    private Animator animator;

    private Vector3 currentTargetPosition;

    private RaycastHit leftRayHit;
    private RaycastHit rightRayHit;

    private AIHandActionType handActionType;


    ///  Hands constant following ////////////////////
    private bool areHandsFollowingTarget;
    private Transform handsFollowTarget;


    ///  head constant following ////////////////////
    ///  

    [Space]
    [Range(0, 1)][SerializeField] private float maxLookAtWeight = 1;    
    [SerializeField] private float lookLerpSpeed = 3;
    private Transform currentLookTarget;
    private float lookAtWeight;
    private bool hasLookTarget;
    private bool isActiveLookAt;

    #region MONO
    private void Awake()
    {
        animator = GetComponent<Animator>();

        leftShoulder = animator.GetBoneTransform(HumanBodyBones.LeftShoulder);
        rightShoulder = animator.GetBoneTransform(HumanBodyBones.RightShoulder);
    }
    private void OnAnimatorIK(int layerIndex)
    {

        if (isActiveIK)
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandWeight);
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, rightHandWeight);

            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandWeight);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, rightHandWeight);

            animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.position);
            animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);

            animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandTarget.rotation);
            animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandTarget.rotation);

            animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, leftHandWeight);
            animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, rightHandWeight);

            animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, leftHandWeight);
            animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, rightHandWeight);

            animator.SetIKHintPosition(AvatarIKHint.LeftElbow, leftElbowTarget.position);
            animator.SetIKHintPosition(AvatarIKHint.RightElbow, righElbowTarget.position);            
        }

        /// Testing
        //if (Input.GetKeyDown(KeyCode.L))
        //    SetActiveLookAtSystem(true, Camera.main.transform);

        if (isActiveLookAt)
        {
            animator.SetLookAtWeight(lookAtWeight, lookAtWeight / 3f, lookAtWeight, 0, 1f);

            if (currentLookTarget != null)
            {
                headFollowTarget.position = Vector3.Lerp(headFollowTarget.position, currentLookTarget.position, Time.deltaTime * lookLerpSpeed);
                animator.SetLookAtPosition(headFollowTarget.position);
            }
        }
    }

    private void FixedUpdate()
    {
        

        if (isActiveIK)
        {
            if (hasTarget)
            {
                //print(string.Format("IK|isActiveIK:{0}|hasTarget{1}|actionWeight > 1", isActiveIK, hasTarget, activationWeight < 1));
                if (activationWeight < 1)
                {
                    activationWeight += Time.fixedDeltaTime * movementSpeed;

                    if (activationWeight > 1)
                        activationWeight = 1;

                    leftHandWeight = handActionType == AIHandActionType.LeftHanded || handActionType == AIHandActionType.BothHands ? activationWeight : 0;
                    rightHandWeight = handActionType == AIHandActionType.RightHanded || handActionType == AIHandActionType.BothHands ? activationWeight : 0;                    
                }

                if (areHandsFollowingTarget)
                {
                    currentTargetPosition = handsFollowTarget.position;
                    SendHandsRaycast();                    
                }
            }
            else
            {
                deactivationWeight -= Time.fixedDeltaTime;

                leftHandWeight = leftHandWeight > deactivationWeight ? deactivationWeight : leftHandWeight;
                rightHandWeight = rightHandWeight > deactivationWeight ? deactivationWeight : rightHandWeight;

                if (deactivationWeight <= 0)
                {
                    deactivationWeight = 0;
                    leftHandWeight = 0;
                    rightHandWeight = 0;
                    isActiveIK = false;
                }

                activationWeight = deactivationWeight;
            }
        }

        if (isActiveLookAt)
        {
            if (hasLookTarget)
            {
                if (lookAtWeight < maxLookAtWeight)
                {
                    lookAtWeight += Time.fixedDeltaTime * movementSpeed;

                    if (lookAtWeight > maxLookAtWeight)
                        lookAtWeight = maxLookAtWeight;
                }
            }
            else
            {
                lookAtWeight -= Time.fixedDeltaTime;

                if (lookAtWeight <= 0)
                {
                    lookAtWeight = 0;
                    isActiveLookAt = false;
                    currentLookTarget = null;
                }
            }
        }

    } 
    #endregion

    public void ActiveIKSystem(Vector3 targetPosition, AIHandActionType _handActionType)
    {
        Debug.Log("Activate IK, as " + _handActionType);

        currentTargetPosition = targetPosition;
        handActionType = _handActionType;

        SendHandsRaycast(true);

        isActiveIK = true;
        hasTarget = true;
        deactivationWeight = 1;
    }

    public void DeactiveIKSystem()
    {
        deactivationWeight = rightHandWeight > leftHandWeight ? rightHandWeight : leftHandWeight;
        hasTarget = false;
        areHandsFollowingTarget = false;
        handsFollowTarget = null;
    }

    public void SetActiveLookAtSystem(bool isActive, Transform _target = null)
    {
        Debug.Log("Setting look at as >" + isActive);

        hasLookTarget = isActive;

        if(hasLookTarget)
        {
            currentLookTarget = _target;
            isActiveLookAt = true;
        }
    }

    private void SendHandsRaycast(bool instantTargeting = false)
    {
        Vector3 center = (leftShoulder.position + rightShoulder.position) / 2;

        if(handActionType == AIHandActionType.LeftHanded || handActionType == AIHandActionType.BothHands)
        {
            Vector3 leftHandAimingPos = leftHandTarget.position;

            if (Physics.Raycast(leftShoulder.position, currentTargetPosition - leftShoulder.position, out leftRayHit, 1.5f, raycastLayer))
            {
                //leftHandTarget.position = Vector3.Lerp(leftRayHit.point + (leftShoulder.position - leftRayHit.point).normalized * 0.1f,
                leftHandAimingPos = Vector3.Lerp(leftRayHit.point,
                                                        leftShoulder.position - transform.rotation * leftHandAimOffset + (currentTargetPosition - center) * 0.9f, 0.5f);
            }
            else
            {
                leftHandAimingPos = leftShoulder.position + (currentTargetPosition - leftShoulder.position).normalized * 0.6f;
            }

            if (instantTargeting)
                leftHandTarget.position = leftHandAimingPos;
            else
                leftHandTarget.position = Vector3.Lerp(leftHandTarget.position, leftHandAimingPos, Time.fixedDeltaTime * movementSpeed);
        }

        if (handActionType == AIHandActionType.RightHanded || handActionType == AIHandActionType.BothHands)
        {
            Vector3 rightHandAimingPos = rightHandTarget.position;

            if (Physics.Raycast(rightShoulder.position, currentTargetPosition - rightShoulder.position, out rightRayHit, 1.5f, raycastLayer))
            {
                rightHandAimingPos = Vector3.Lerp(rightRayHit.point + (rightShoulder.position - rightRayHit.point).normalized * 0.1f,
                                                        rightShoulder.position + transform.rotation * rightHandAimOffset + (currentTargetPosition - center) * 0.9f, 0.5f);
            }
            else
            {
                rightHandAimingPos = rightShoulder.position + (currentTargetPosition - rightShoulder.position).normalized * 0.6f;
            }

            if (instantTargeting)
                rightHandTarget.position = rightHandAimingPos;
            else
                rightHandTarget.position = Vector3.Lerp(rightHandTarget.position, rightHandAimingPos, Time.fixedDeltaTime * movementSpeed);
        }
    }

    public void StartHandsConstantObjectFollowing(Transform _target, AIHandActionType _handActionType = AIHandActionType.None)
    {
        if (_handActionType == AIHandActionType.None)
            _handActionType = Vector3.Distance(_target.position, leftShoulder.position) > Vector3.Distance(_target.position, rightShoulder.position) ?
                                AIHandActionType.RightHanded : AIHandActionType.LeftHanded;

        areHandsFollowingTarget = true;
        handsFollowTarget = _target;
        handActionType = _handActionType;

        isActiveIK = true;
        hasTarget = true;
        deactivationWeight = 1;
    }

    //public void PutObjectInHand (Transform _object)
    //{
    //    _object.SetAttachProperties(true, animator.GetBoneTransform(handActionType == AIHandActionType.LeftHanded ? HumanBodyBones.LeftHand : HumanBodyBones.RightHand));
    //    _object.transform.localPosition = Vector3.zero;
    //    _object.transform.localRotation = Quaternion.identity;
    //}
}
