// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ILS/Unlit/Characters"
{
	Properties
	{
		[NoScaleOffset][Header (Image Textures)][Space]_MainTex("Main Tex", 2D) = "white" {}
		[NoScaleOffset]_Maps_RAO_GC_BTh("Maps_R(AO)_G(C)_B(Th)", 2D) = "white" {}
		_RimPower("Rim Power", Float) = 5
		_RimIntensity("Rim Intensity", Float) = 0.25
		_ShadowColor("Shadow Color", Color) = (0.245283,0.2299528,0.2084906,0)
		_LightPosition("Light Position", Vector) = (0,0,0,0)
		[NoScaleOffset]_EmissionMask("Emission Mask", 2D) = "white" {}
		_HighlightColor("Highlight Color", Color) = (0.3843138,1,0,1)
		[Toggle]_Highlight("Highlight", Float) = 0
		_HighlightFreq("Highlight Freq", Float) = 0.25
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Opaque" }
	LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend Off
		Cull Off
		ColorMask RGBA
		ZWrite On
		ZTest LEqual
		Offset 0 , 0
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
				float3 ase_normal : NORMAL;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord2 : TEXCOORD2;
			};

			uniform sampler2D _Maps_RAO_GC_BTh;
			uniform sampler2D _MainTex;
			uniform float3 _LightPosition;
			uniform float4 _ShadowColor;
			uniform float _RimPower;
			uniform float _RimIntensity;
			uniform sampler2D _EmissionMask;
			uniform float4 _HighlightColor;
			uniform float _HighlightFreq;
			uniform float _Highlight;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.ase_texcoord1.xyz = ase_worldPos;
				float3 ase_worldNormal = UnityObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord2.xyz = ase_worldNormal;
				
				o.ase_texcoord.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord.zw = 0;
				o.ase_texcoord1.w = 0;
				o.ase_texcoord2.w = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = vertexValue;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				float2 uv_Maps_RAO_GC_BTh119 = i.ase_texcoord.xy;
				float4 tex2DNode119 = tex2D( _Maps_RAO_GC_BTh, uv_Maps_RAO_GC_BTh119 );
				float2 uv_MainTex2 = i.ase_texcoord.xy;
				float4 Albedo6 = tex2D( _MainTex, uv_MainTex2 );
				float4 lerpResult127 = lerp( Albedo6 , ( Albedo6 * 1.25 ) , tex2DNode119.b);
				float3 ase_worldPos = i.ase_texcoord1.xyz;
				float3 normalizeResult153 = normalize( ( ase_worldPos - _LightPosition ) );
				float3 ase_worldNormal = i.ase_texcoord2.xyz;
				float dotResult99 = dot( normalizeResult153 , ase_worldNormal );
				float smoothstepResult100 = smoothstep( 0.125 , 0.25 , dotResult99);
				float3 ase_worldViewDir = UnityWorldSpaceViewDir(ase_worldPos);
				ase_worldViewDir = normalize(ase_worldViewDir);
				float fresnelNdotV95 = dot( ase_worldNormal, ase_worldViewDir );
				float fresnelNode95 = ( 0.0 + 1.0 * pow( max( 1.0 - fresnelNdotV95 , 0.0001 ), _RimPower ) );
				float2 uv_EmissionMask156 = i.ase_texcoord.xy;
				float mulTime159 = _Time.y * _HighlightFreq;
				
				
				finalColor = ( ( tex2DNode119.r * ( ( ( tex2DNode119.g * Albedo6 ) + Albedo6 ) / 2.0 ) ) + ( ( lerpResult127 + ( Albedo6 * smoothstepResult100 ) + ( Albedo6 * ( 1.0 - smoothstepResult100 ) * _ShadowColor ) ) / 3.0 ) + ( Albedo6 * fresnelNode95 * _RimIntensity ) + ( tex2D( _EmissionMask, uv_EmissionMask156 ) * _HighlightColor * abs( sin( ( mulTime159 * UNITY_PI ) ) ) * _Highlight ) );
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=17700
1920;0;1570;1029;2001.317;1071.03;2.801039;True;False
Node;AmplifyShaderEditor.SamplerNode;119;-1291.667,-969.0342;Inherit;True;Property;_Maps_RAO_GC_BTh;Maps_R(AO)_G(C)_B(Th);1;1;[NoScaleOffset];Create;True;0;0;False;0;-1;d3c9dda3be5e46c4d83668c61e0293d2;8c7f0e0a0d7db83459bdf6ed364cfd45;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;154;-1209.287,-194.9645;Inherit;False;Property;_LightPosition;Light Position;5;0;Create;True;0;0;False;0;0,0,0;-20,15,21;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;151;-1190.287,-326.9645;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WireNode;145;-640.947,-886.495;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;152;-996.0176,-265.3376;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NormalizeNode;153;-853.0176,-265.3376;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;144;-622.947,-879.495;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-394.1788,-958.96;Inherit;True;Property;_MainTex;Main Tex;0;1;[NoScaleOffset];Create;True;0;0;False;2;Header (Image Textures);Space;-1;None;cbf99deb3a9266c47953d2f4be28d433;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;138;-885.8186,-863.1589;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;98;-863.4661,-196.147;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;158;-44.18979,422.4948;Inherit;False;Property;_HighlightFreq;Highlight Freq;9;0;Create;True;0;0;False;0;0.25;0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;99;-641.4661,-263.1469;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;142;-620.0125,-694.2438;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;6;51.86816,-961.8632;Float;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;137;-869.1462,-861.001;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;9;-386.1978,-667.907;Inherit;False;6;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;120;-518.3267,-907.9481;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;100;-497.1583,-262.7105;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.125;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;139;-871.2075,-457.7282;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;133;-856.4059,-532.5428;Inherit;False;6;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;143;-600.8125,-689.6435;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;141;-847.7007,-407.3757;Inherit;False;Constant;_Float1;Float 1;8;0;Create;True;0;0;False;0;1.25;0.32;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;159;131.8102,427.4947;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;136;-858.9017,-444.6224;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PiNode;160;290.4745,427.5731;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;130;-290.3362,-111.4621;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;109;-173.6527,-714.2853;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;134;-579.8411,-84.45907;Inherit;False;6;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;140;-669.7007,-498.3757;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;121;-496.5268,-900.6478;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;132;-372.4651,-19.33445;Inherit;False;Property;_ShadowColor;Shadow Color;4;0;Create;True;0;0;False;0;0.245283,0.2299528,0.2084906,0;0.2452829,0.2299527,0.2084905,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;116;-52.07374,-592.8086;Inherit;False;Constant;_Float0;Float 0;11;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;127;-515.5432,-520.8931;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;96;-67.27292,75.09237;Inherit;False;Property;_RimPower;Rim Power;2;0;Create;True;0;0;False;0;5;0.35;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;102;-292.6373,-285.8838;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SinOpNode;161;457.7417,427.573;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;131;-115.2586,-81.8102;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;114;-32.74385,-689.1171;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;122;-488.7269,-740.0485;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;149;174.3354,-83.75679;Inherit;False;6;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;157;455.0593,265.8308;Inherit;False;Property;_HighlightColor;Highlight Color;7;0;Create;True;0;0;False;0;0.3843138,1,0,1;0.3843138,1,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.AbsOpNode;162;572.6041,427.7888;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;123;-471.827,-737.4481;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;106;416.1241,-303.3295;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;115;96.21426,-689.3806;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;105;128.9837,147.5352;Inherit;False;Property;_RimIntensity;Rim Intensity;3;0;Create;True;0;0;False;0;0.25;-0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;95;119.4201,-12.54848;Inherit;False;Standard;WorldNormal;ViewDir;True;True;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;163;551.8312,494.2085;Inherit;False;Property;_Highlight;Highlight;8;1;[Toggle];Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;148;397.2371,-180.8057;Inherit;False;Constant;_Float2;Float 2;5;0;Create;True;0;0;False;0;3;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;156;404.4037,82.55243;Inherit;True;Property;_EmissionMask;Emission Mask;6;1;[NoScaleOffset];Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;118;231.2196,-762.0214;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;150;407.3357,-35.75687;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;164;751.5385,247.02;Inherit;False;4;4;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;147;576.2953,-303.0418;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;146;934.2098,-325.7906;Inherit;False;4;4;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;155;1359.872,-325.7946;Float;False;True;-1;2;ASEMaterialInspector;100;1;ILS/Unlit/Characters;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;0;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;False;True;2;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;RenderType=Opaque=RenderType;True;2;0;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
WireConnection;145;0;119;2
WireConnection;152;0;151;0
WireConnection;152;1;154;0
WireConnection;153;0;152;0
WireConnection;144;0;145;0
WireConnection;138;0;119;3
WireConnection;99;0;153;0
WireConnection;99;1;98;0
WireConnection;142;0;144;0
WireConnection;6;0;2;0
WireConnection;137;0;138;0
WireConnection;120;0;119;1
WireConnection;100;0;99;0
WireConnection;139;0;137;0
WireConnection;143;0;142;0
WireConnection;159;0;158;0
WireConnection;136;0;139;0
WireConnection;160;0;159;0
WireConnection;130;0;100;0
WireConnection;109;0;143;0
WireConnection;109;1;9;0
WireConnection;140;0;133;0
WireConnection;140;1;141;0
WireConnection;121;0;120;0
WireConnection;127;0;133;0
WireConnection;127;1;140;0
WireConnection;127;2;136;0
WireConnection;102;0;134;0
WireConnection;102;1;100;0
WireConnection;161;0;160;0
WireConnection;131;0;134;0
WireConnection;131;1;130;0
WireConnection;131;2;132;0
WireConnection;114;0;109;0
WireConnection;114;1;9;0
WireConnection;122;0;121;0
WireConnection;162;0;161;0
WireConnection;123;0;122;0
WireConnection;106;0;127;0
WireConnection;106;1;102;0
WireConnection;106;2;131;0
WireConnection;115;0;114;0
WireConnection;115;1;116;0
WireConnection;95;3;96;0
WireConnection;118;0;123;0
WireConnection;118;1;115;0
WireConnection;150;0;149;0
WireConnection;150;1;95;0
WireConnection;150;2;105;0
WireConnection;164;0;156;0
WireConnection;164;1;157;0
WireConnection;164;2;162;0
WireConnection;164;3;163;0
WireConnection;147;0;106;0
WireConnection;147;1;148;0
WireConnection;146;0;118;0
WireConnection;146;1;147;0
WireConnection;146;2;150;0
WireConnection;146;3;164;0
WireConnection;155;0;146;0
ASEEND*/
//CHKSM=8461C62490DB90D3F139F254E594C80ABDC77FED