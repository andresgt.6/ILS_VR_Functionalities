﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class IndicatorController : MonoBehaviour
{
    public enum State
    {
        Disabled,
        Enabled
    }

    [Header("Current State")]
    [SerializeField] private State currentState;

    [Header("Texts")]    
    [SerializeField] private string actionToMake;

    [Header("Main Components")]
    [SerializeField] private Transform[] lineTargets;
    [SerializeField] private LineRenderer line;

    private Canvas canvas;
    private Transform player;
    private UIIndicator uiIndicator;

    private void Awake()
    {
        canvas = GetComponentInChildren<Canvas>();
        uiIndicator = GetComponent<UIIndicator>();
        player = Camera.main.transform;
        Disable();
    }

    private void Update()
    {
        if (currentState == State.Disabled)
            return;

        SetLines();
    }

    public void Enabled(TagInfo info = null)
    {
        canvas.enabled = true;
        line.enabled = true;
        currentState = State.Enabled;
        if (info != null)
        {
            if (info.actionInfo != null)
            {
                actionToMake = info.actionInfo;
            }
			else
			{
				actionToMake = "";
			}
        }

        uiIndicator.SetMainTexts(actionToMake,info);
    }

    public void Disable()
    {
        canvas.enabled = false;
        line.enabled = false;
        currentState = State.Disabled;
    }

    private void SetLines()
    {
        transform.LookAt(player);
        line.SetPosition(0, lineTargets[0].position);
        line.SetPosition(1, lineTargets[1].position);
    }
}

public enum TagInfoActionType
{
	NormalString,
	OkHand,
	Pointing,
    NoHand,
	Touch,
	Grab
}

public class TagInfo
{    
    public string actionInfo = null;
	public TagInfoActionType actionType = TagInfoActionType.NormalString;
	public Vector3? customPos;

	public TagInfo(string actionInfo = null, TagInfoActionType type = TagInfoActionType.NormalString, Vector3? customPos = null)
	{
		this.actionInfo = actionInfo;
		this.actionType = type;
        this.customPos = customPos;
    }
}
