﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIInformative : MonoBehaviour
{
    public Action OnPressRepeatButton;    

    [SerializeField] private HoverButton buttonRepeat;    

    [SerializeField] private Text moduleText;
    [SerializeField] private Text titleText;
    [SerializeField] private Text bodyText;

    [SerializeField] private Canvas canvas;

    private void Awake()
    {
        SetActive(false);
    }

    public void SetActive(bool activate)
    {
        canvas.enabled = activate;
    }

    public void SetUIInformative(InformativeSettings info)
    {
        SetActive(true);
        moduleText.text = info.moduleString;
        titleText.text = info.titleString;
        bodyText.text = info.bodyString;

        buttonRepeat.OnStateChanged += OnButtonRepeatPressed;
        buttonRepeat.ActiveInteractions(1);
    }

    private void OnButtonRepeatPressed(bool enabled)
    {
        buttonRepeat.OnStateChanged -= OnButtonRepeatPressed;
        SetActive(false);
        OnPressRepeatButton?.Invoke();
    }
}

public enum InformativeState
{
    Fail    
}

public class InformativeSettings
{
    public string moduleString;
    public string titleString;
    public string bodyString;
    public InformativeState state;
}
