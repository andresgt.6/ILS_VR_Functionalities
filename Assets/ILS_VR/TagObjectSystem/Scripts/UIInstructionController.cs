﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInstructionController : MonoBehaviour
{
    [SerializeField] private Transform[] lineTargets;
    [SerializeField] private Transform player;
    [SerializeField] private LineRenderer line;

    private void Awake()
    {
        player = Camera.main.transform;
        line.enabled = true;
    }

    private void Update()
    {
        transform.LookAt(player);
        line.SetPosition(0, lineTargets[0].position);        
        line.SetPosition(1, lineTargets[1].position);
    }

}
