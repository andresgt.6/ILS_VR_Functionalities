﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIIndicator : MonoBehaviour
{
	[Tooltip("Main texts")]
	[SerializeField] private Text actionText;

	[Tooltip("Sprites")]
	[SerializeField] private Sprite[] grabSprites;
	[SerializeField] private Sprite[] noHandSprites;
	[SerializeField] private Sprite[] poiting;

	[Tooltip("Images")]
	[SerializeField] private Image imagePanel;
	[SerializeField] private Image textPanel;

	[Tooltip("Settings")]
	[SerializeField] private float secuenceTime;

	private TagInfoActionType currentType;

	private Coroutine SecuenceCoroutine;

	public void SetMainTexts(string actionToMake = null, TagInfo info = null)
	{
		actionText.enabled = info.actionType.Equals(TagInfoActionType.NormalString);
		textPanel.enabled = info.actionType.Equals(TagInfoActionType.NormalString);
		imagePanel.enabled = !info.actionType.Equals(TagInfoActionType.NormalString);

		currentType = info.actionType;

		if (SecuenceCoroutine != null)
		{
			StopCoroutine(SecuenceCoroutine);
			SecuenceCoroutine = null;
		}

		if (info.customPos.HasValue)
			transform.localPosition = info.customPos.Value;

		switch (info.actionType)
		{
			case TagInfoActionType.NormalString:
				if (actionToMake != null)
					actionText.text = actionToMake;
				break;
			case TagInfoActionType.OkHand:
			case TagInfoActionType.Pointing:
			case TagInfoActionType.Touch:
			case TagInfoActionType.Grab:
			case TagInfoActionType.NoHand:				
				SecuenceCoroutine = StartCoroutine(StartSpriteSecuence());
				break;
			default:
				break;
		}
	}

	private IEnumerator StartSpriteSecuence()
	{
		Sprite[] spritesToShow = new Sprite[0];

		switch (currentType)
		{
			case TagInfoActionType.OkHand:
				break;
			case TagInfoActionType.Pointing:
				spritesToShow = poiting;
				break;
			case TagInfoActionType.Touch:
				break;
			case TagInfoActionType.Grab:
				spritesToShow = grabSprites;
				break;
			case TagInfoActionType.NoHand:
				spritesToShow = noHandSprites;
				break;
		}

		for (int i = 0; i < spritesToShow.Length; i++)
		{
			imagePanel.sprite = spritesToShow[i];
			yield return new WaitForSeconds(secuenceTime);
		}

		if(SecuenceCoroutine != null)
			SecuenceCoroutine = StartCoroutine(StartSpriteSecuence());
	}
}
