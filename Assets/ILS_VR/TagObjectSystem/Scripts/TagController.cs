﻿using UnityEngine;
using System.Collections;

public class TagController : MonoBehaviour
{
    [SerializeField] private float translationSpeed = 4;
    [SerializeField] private float rotationSpeed = 4;

    public Transform currentScreen;

    private bool areTagsActive;

    [SerializeField] private TagLineController line;

    private Vector3 relativePos = new Vector3();
    private Quaternion relativeRot = Quaternion.identity;

    private Transform originalParent;

    private void Awake()
    {        
        relativePos = transform.localPosition;
        relativeRot = transform.localRotation;

        originalParent = transform.parent;
        //transform.SetParent(transform.root);

        //Debug.Log(lines.Length + " // " + tagsGroups.Length);

        Desactive();
    }

    public void Desactive()
    {
        areTagsActive = false;

        line.SetActiveLine(false);
    }

    public void ActiveTag(Transform newScreen)
    {
        currentScreen = newScreen;
        areTagsActive = true;

        line.SetActiveLine(true);

        transform.position = originalParent.position + transform.rotation * relativePos;
        transform.rotation = originalParent.rotation * relativeRot;

        StartCoroutine(UpdateLines());
    }

    private IEnumerator UpdateLines()
    {
        Transform player = Camera.main.transform;

        while (areTagsActive)
        {
            line.UpdateLinePositions(player.position);

            LerpCanvas();

            yield return null;
        }
    }

    private void LerpCanvas()
    {
        transform.position = Vector3.Lerp(transform.position, originalParent.position + transform.rotation * relativePos, Time.deltaTime * translationSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, originalParent.rotation * relativeRot, Time.deltaTime * rotationSpeed);
    }
}
