﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagLineController : MonoBehaviour
{
    [SerializeField] private Transform[] linePoints;

    private LineRenderer line;

    private void Awake()
    {
        line = GetComponent<LineRenderer>();
        line.positionCount = linePoints.Length;
        SetActiveLine(false);
    }

    public void UpdateLinePositions(Vector3 positionsToLook)
    {
        transform.LookAt(transform.position + (transform.position - positionsToLook));

        for (int i = 0; i < linePoints.Length; i++)
            line.SetPosition(i, linePoints[i].position);
    }

    public void SetActiveLine(bool _isActive)
    {
        if(line)
            line.enabled = _isActive;
    }
}
