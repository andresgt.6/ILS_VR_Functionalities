﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoverButtonTouchController : MonoBehaviour
{
    [SerializeField] private Image circleImage;
    [SerializeField] private float circleFillTime = 2;

    private void Awake()
    {
        circleImage.fillAmount = 0;
    }

    public void ActionTime()
    {
        LeanTween.value(gameObject, value => circleImage.fillAmount = value, 0, 1, circleFillTime);
    }

    public void OnComplete()
    {
        circleImage.fillAmount = 0;
    }
}
