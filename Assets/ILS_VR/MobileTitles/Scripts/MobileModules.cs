﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;

public class MobileModules : MonoBehaviour
{
    public Action OnCompleteWelcome;

    public enum ModuleScreens
    {
        Disable,
        Tutorial,
        Welcome,
        Intruction,
        NormalPanel,
        CountDown
    }

    [Header("Current State")]
    [SerializeField] private ModuleScreens currentModuleScreen;

    [Header("Module screen")]
    [SerializeField] private Image myImage;
    [SerializeField] private HeadLerpFollower headLerpFollower;

    [Header("Screens")]
    [SerializeField] private CanvasGroup welcomePanel;
    [SerializeField] private CanvasGroup intructionPanel;
    [SerializeField] private CanvasGroup modulePanel;
    [SerializeField] private CanvasGroup didUknowPanel;
    [SerializeField] private CanvasGroup[] countDownPanels;
    [SerializeField] private CanvasGroup clockPanel;

    [Header("Hover buttons")]
    [SerializeField] private HoverButton welcomeArrow;
    [SerializeField] private HoverButton instructiveArrow;

    [Header("Texts")]
    [SerializeField] private Text moduleTitleText;
    [SerializeField] private Text moduleNameText;
	[SerializeField] private Text DidUknowTitleText;
	[SerializeField] private Text DidUknowNameText;
	[SerializeField] private Text timerText;
    [SerializeField] private GameObject medal;


    private Slider clockSlider;

    private void Awake()
    {
        gameObject.SetActive(true);
        DisableAllScreens();
        clockSlider = clockPanel.GetComponentInChildren<Slider>();
    }

    #region Start Secuence
    public void PlayWelcome()
    {
        SetScreen(ModuleScreens.Welcome);
    }

    private void SetScreen(ModuleScreens state)
    {
        DisableAllScreens();
        currentModuleScreen = state;
        switch (state)
        {
            case ModuleScreens.Disable:
                SetEnabled(false);
                break;
            case ModuleScreens.Tutorial:
                modulePanel.alpha = 1;
                break;
            case ModuleScreens.Welcome:
                welcomePanel.alpha = 1;
                welcomeArrow.ActiveInteractions(1);
                welcomeArrow.OnStateChanged += WelcomeArrow_OnStateChanged;
                break;
            case ModuleScreens.Intruction:
                clockPanel.alpha = 1;
                SetClockTween(30, .1f);
                intructionPanel.alpha = 1;
                instructiveArrow.ActiveInteractions(1);
                instructiveArrow.OnStateChanged += InstructiveArrow_OnStateChanged;
                break;
            case ModuleScreens.NormalPanel:
                clockPanel.alpha = 1;
                SetClockTween(163.7f, .3f);
                modulePanel.alpha = 1;
                LeanTween.delayedCall(5, () => { SetScreen(ModuleScreens.CountDown); });
                break;
            case ModuleScreens.CountDown:
                clockPanel.alpha = 1;
                SetClockTween(228, .5f);
                StartCoroutine(CountDown());
                break;
            default:
                break;
        }
    }

    private void InstructiveArrow_OnStateChanged(bool arg0)
    {
        instructiveArrow.OnStateChanged -= InstructiveArrow_OnStateChanged;
        SetScreen(ModuleScreens.NormalPanel);
    }

    private void WelcomeArrow_OnStateChanged(bool arg0)
    {
        welcomeArrow.OnStateChanged -= WelcomeArrow_OnStateChanged;
        SetScreen(ModuleScreens.Intruction);
    }

    private void SetClockTween(float yValue, float time)
    {
        LeanTween.moveLocalY(clockPanel.gameObject, yValue, time);
    }

    private void DisableAllScreens()
    {
        medal.SetActive(false);
        welcomePanel.alpha = 0;
        intructionPanel.alpha = 0;
        if(didUknowPanel)
            didUknowPanel.alpha = 0;
        modulePanel.alpha = 0;
        for (int i = 0; i < countDownPanels.Length; i++)
        {
            countDownPanels[i].alpha = 0;
        }
        clockPanel.alpha = 0;
    }

    private IEnumerator CountDown()
    {
        countDownPanels[0].alpha = 1;        
        yield return new WaitForSeconds(1);

        for (int i = 1; i < countDownPanels.Length; i++)
        {
            LeanTween.alphaCanvas(countDownPanels[i - 1], 0, .3f);            
            countDownPanels[i].alpha = 0;
            LeanTween.alphaCanvas(countDownPanels[i], 1, .7f);
            yield return new WaitForSeconds(1);
        }

        DisableAllScreens();
        clockPanel.alpha = 1;
        OnCompleteWelcome?.Invoke();
    }
    #endregion

    public void SetEnabled(bool enabled, String moduleTitle = null, String moduleName = null, float? duration = null, bool tutorial = false)
    {
        if (!enabled)
            currentModuleScreen = ModuleScreens.Disable;

        if (moduleTitle != null)
        {
            moduleTitleText.text = moduleTitle;
            if (currentModuleScreen != ModuleScreens.Welcome)
            {
                if(tutorial == false)
                {
                    clockPanel.alpha = 1;
                    SetClockTween(163.7f, .3f);
                } 
                else
                    clockPanel.alpha = 0;
                modulePanel.alpha = 1;
            }
        }

        if (moduleName != null)
            moduleNameText.text = moduleName;        

        if (duration.HasValue)
            LeanTween.delayedCall(duration.Value, CompleteDuration);

        headLerpFollower.SetFollowerActive(enabled);
    }

	public void SetDidUKnowEnabled(bool enabled, String moduleTitle, String moduleName, float duration)
	{
        DidUknowTitleText.text = moduleTitle;
        DidUknowNameText.text = moduleName;
        LeanTween.delayedCall(1, () => { didUknowPanel.alpha = 1; });        
        LeanTween.delayedCall(duration, CompleteDuration);
        headLerpFollower.SetFollowerActive(enabled);
        medal.SetActive(enabled);
	}

	public void SetTimer(float currentTime, float time)
    {
        clockSlider.maxValue = time;
        clockSlider.value = currentTime;
        string minutes = Mathf.Floor(currentTime / 60).ToString("00");
        string seconds = (currentTime % 60).ToString("00");
        timerText.text = string.Format("{0}:{1}", minutes, seconds);
    }

    private void CompleteDuration()
    {
        DisableAllScreens();
        if(currentModuleScreen != ModuleScreens.Tutorial)
        {
            clockPanel.alpha = 1;
            SetClockTween(228, .5f);
        }        
    }
}
