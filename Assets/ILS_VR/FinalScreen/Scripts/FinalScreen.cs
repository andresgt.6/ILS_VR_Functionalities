﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalScreen : MonoBehaviour
{
    [SerializeField] private Image fillImage;

    private void Awake()
    {
        SetActiveScreen(false);
    }

    public void SetActiveScreen(bool activate)
    {
        gameObject.SetActive(activate);
        fillImage.fillAmount = 0;
    }

    public void SetFillAmount(float fillTime)
    {
        LeanTween.value(gameObject, value => fillImage.fillAmount = value, 0, 1, fillTime);
    }
}
