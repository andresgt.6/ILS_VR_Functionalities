﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRDebugger : MonoBehaviour
{
    public string output = "";
    public string stack = "";

    //[SerializeField] private OVRInput.Button debugButton;

    [Space]
    [SerializeField] private Text[] debugTexts;
    [SerializeField] private Text[] debugNames;
    [SerializeField] private Color errorColor;
    [SerializeField] private Color warningColor;
    [SerializeField] private Color exceptionColor;

    [Space]
    [SerializeField] private bool logWarnings;
    [SerializeField] private bool alwaysEnabled;

    private List<DebugInfo> debugInfo;

    private bool isDebuggerActive;
    private HeadLerpFollower follower;
    private Canvas followerCanvas;

    private void Awake()
    {
        debugInfo = new List<DebugInfo>();
        follower = GetComponentInChildren<HeadLerpFollower>();
        followerCanvas = GetComponentInChildren<Canvas>();

        if(alwaysEnabled)
        {
            isDebuggerActive = true;
            SetActiveConsole(true);
        }
    }

    private void Start()
    {
        isDebuggerActive = false;
        SetActiveConsole(false);
    }

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    private void OnDestroy()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        output = logString;
        stack = stackTrace;

        switch (type)
        {
            case LogType.Error:
            case LogType.Exception:
                AddLog(logString, stackTrace, type);
                break;
            case LogType.Warning:
                if(logWarnings)
                    AddLog(logString, stackTrace, type);
                break;
            case LogType.Assert:
                break;
            case LogType.Log:
                break;
            default:
                break;
        }
    }

    private void AddLog(string logString, string stackTrace, LogType type)
    {
        debugInfo.Add(new DebugInfo(logString, stackTrace, type));

        if (debugInfo.Count > debugTexts.Length)
            debugInfo.RemoveAt(0);

        UpdateConsole();

    }

    private void UpdateConsole()
    {
        for (int i = 0; i < debugTexts.Length; i++)
        {
            if (i >= debugInfo.Count)
                break;

            debugTexts[i].text = debugInfo[i].message;
            debugNames[i].text = debugInfo[i].goName;

            switch (debugInfo[i].logType)
            {
                case LogType.Error:
                    debugTexts[i].color = errorColor;
                    break;
                case LogType.Warning:
                    debugTexts[i].color = warningColor;
                    break;
                case LogType.Exception:
                    debugTexts[i].color = exceptionColor;
                    break;
                default:
                    break;
            }
        }
    }

    private void Update()
    {
        //if(OVRInput.GetDown(debugButton) || Input.GetKeyDown(KeyCode.D))
        //{
        //    isDebuggerActive = !isDebuggerActive;
        //    SetActiveConsole(isDebuggerActive);
        //}
    }


    private void SetActiveConsole(bool _isActive)
    {
        follower.SetFollowerActive(_isActive);
        followerCanvas.enabled = _isActive;
    }
}

public class DebugInfo
{
    public string message;
    public string goName;
    public LogType logType;

    public DebugInfo(string _message, string _goName, LogType _type)
    {
        message = _message;
        goName = _goName;
        logType = _type;
    }
}
