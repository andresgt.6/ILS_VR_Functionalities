﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeadLerpFollower : MonoBehaviour
{
	[Space,Header("Main components")]
	[SerializeField] private Transform playerEyes;

	[Space,Header("Settings")]
	[SerializeField] private float lerpPositionSpeed = 1;
	[SerializeField] private float lerpRotationSpeed = 1;
	[SerializeField] private bool startFollowInAwake = true;	
	public bool continuousFollow = true;	

	private bool isActive;

	private void Awake ()
	{
		isActive = false;

		if(startFollowInAwake)
			SetFollowerActive(true);

    }

	public void SetFollowerActive (bool isTrue)
	{
		isActive = isTrue;
		transform.position = playerEyes.position;
		transform.rotation = playerEyes.rotation;

		if (isTrue && gameObject.activeInHierarchy)
			StartCoroutine(FollowingProcess());
	}

	private IEnumerator  FollowingProcess ()
	{
		while(isActive)
		{
			CalculateLerp();
			yield return new WaitForEndOfFrame();
			isActive = continuousFollow;
		}

	}

	private void CalculateLerp ()
	{
		transform.position = Vector3.Lerp (transform.position, playerEyes.position, Time.deltaTime * lerpPositionSpeed);
        Quaternion myRotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);
        Quaternion playerEyeRotation = Quaternion.Euler(0, playerEyes.eulerAngles.y, 0);
		transform.rotation = Quaternion.Lerp (myRotation, playerEyeRotation, Time.deltaTime * lerpRotationSpeed);
	}
}
