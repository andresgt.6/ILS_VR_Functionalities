﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SignalRaycastObject : RaycastTargetBase
{

	public Action CompleteScanning;
    public enum State
    {
        Disabled,
        Idle,
        Scanning,
        Reveal
    }

	[Tooltip("Current State")]
    [SerializeField] protected State currentState;

    [Tooltip("Settings")]
    [SerializeField] protected float scanningTime = 0.5f;

	private CanvasGroup images;

    protected float scanProgress;

	private void Awake()
	{
		images = GetComponent<CanvasGroup>();
	}

	public void SetEnabled(bool enabled)
    {
        if (enabled)
        {
            currentState = State.Idle;
            scanProgress = 0;
            if(images)
			    images.alpha = 1;
        }
        else
        {
            currentState = State.Disabled;
            if (images)
                images.alpha = 0;
		}
    }

    protected override void OnLookingAtMe()
    {
        base.OnLookingAtMe();

        if (currentRaycast != RaycastType.Finger)
            return;

        if (currentState.Equals(State.Disabled) || currentState.Equals(State.Reveal))
            return;

        currentState = State.Scanning;

        //if(!LeanTween.isTweening(gameObject))
        //{
        //    LeanTween.value(gameObject, value => mesh.material.SetFloat(shaderPropertyName, value), 0, 1, scanningTime).setOnComplete(OnCompleteScanningProcess);
        //}
    }

	protected virtual void Update()
	{
		switch (currentState)
		{
			case State.Disabled:
				return;
			case State.Idle:
				print("Idle");
				scanProgress -= Time.deltaTime;
				if (scanProgress <= 0)
				{
					scanProgress = 0;
				}
				break;
			case State.Scanning:
				print("Scann");
				scanProgress += Time.deltaTime;

				if (scanProgress >= scanningTime)
				{
					OnCompleteScanningProcess();
				}
				break;
			case State.Reveal:
				return;
			default:
				break;
		}
	}

	protected virtual void OnCompleteScanningProcess()
    {
        currentState = State.Reveal;
		CompleteScanning?.Invoke();
    }

    protected override void OnStopLookingAtMe()
    {
        base.OnStopLookingAtMe();

        if (currentRaycast != RaycastType.Finger)
            return;

        if (!currentState.Equals(State.Reveal))
        {
            currentState = State.Idle;
            //if (LeanTween.isTweening(gameObject))
            //{
            //    LeanTween.cancel(gameObject);
            //    mesh.material.SetFloat(shaderPropertyName, 0);
            //    currentState = State.Idle;
            //}
        }
    }

	protected virtual void SetReset()
	{
		currentState = State.Disabled;
		scanProgress = 0;
    }
}
