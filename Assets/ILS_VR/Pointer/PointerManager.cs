﻿using System.Collections;
using UnityEngine;

public class PointerManager : MonoBehaviour
{
    public enum State
    {
        Disable,
        Idle,
        WaitingForGesture,
        Pointing
    }

    [Tooltip("Current State")]
    [SerializeField] private State currentState;

    [Tooltip("Fingers")]
    [SerializeField] private Transform fingerLeft;
    [SerializeField] private Transform fingerRight;

    [Tooltip("Line Renderer")]
    [SerializeField] private LineRenderer line;

    [Space, Header("Raycast Settings")]
    [SerializeField] private LayerMask raycastLayer;
    [SerializeField] private float raycastMaxdistance = 10;

    private Transform currentFinger;

    private RaycastType type = RaycastType.Finger;

    private Side? currentSide;    

    private GestureManager gesture;

    private RaycastTargetBase lastTarget;

    private bool isRayActive = false;

    private void Awake()
    {
        line.enabled = false;
    }

    public void Initialized(GestureManager gesture)
    {
        this.gesture = gesture;
        currentState = State.Idle;
    }

    public void SetEnabled(bool activated)
    {
        if(!gesture)
        {
            Debug.LogError("No se ha inicializado");
            return;
        }    

        if(activated)
        {
            currentState = State.WaitingForGesture;
            gesture.StartListeningForAnyGesture();
            gesture.AnyGestureMade += OnAnyGestureMade;
        }
        else
        {
            currentState = State.Idle;
            gesture.StopListeningForCustomGesture();
            gesture.StopListenGestures();
            gesture.AnyGestureMade -= OnAnyGestureMade;
            SetActiveRay(false);
        }
    }

    private void OnAnyGestureMade(Side handSide, GestureType gestureMade)
    {

        print("Haaay gesto " + gestureMade + " con la mano " + handSide);

        switch (gestureMade)
        {
            case GestureType.None:
                if (currentSide.HasValue && handSide.Equals(currentSide.Value))
                    DisabledLine();
                break;                           

            case GestureType.Point:
                EnableLine(handSide);
                break;

            default:
                break;
        }
    }

    private void SetActiveRay(bool active, Side hand = Side.LeftSide)
    {
        if (isRayActive == active)
            return;

        isRayActive = active;

        if (isRayActive)
            StartCoroutine(PointingProcess(hand));
    }

    private void EnableLine(Side hand)
    {
        //print("Linea");

        currentSide = hand;
        currentState = State.Pointing;

        if (hand.Equals(Side.LeftSide))
            currentFinger = fingerLeft;
        else
            currentFinger = fingerRight;

        SetActiveRay(true, hand);
    }

    private void DisabledLine()
    {
        currentSide = null;

        line.enabled = false;
        currentState = State.WaitingForGesture;

        SetActiveRay(false);
    }

    private IEnumerator PointingProcess(Side hand)
    {
        RaycastHit hitInfo = new RaycastHit();

        //print("Sale de la mano " + hand);
        line.enabled = true;

        while(isRayActive)
        {
            if (Physics.Raycast(currentFinger.position, currentFinger.TransformDirection(Vector3.left), out hitInfo, raycastMaxdistance, raycastLayer))
            {            
                line.SetPosition(0, currentFinger.position);
                line.SetPosition(1, hitInfo.point);

                if(hitInfo.transform.TryGetComponent(out RaycastTargetBase target))
                {
                    lastTarget = target;
                    target.LookAtMe(true, type);
                }
            }
            else
            {
                line.SetPosition(0, currentFinger.position);
                line.SetPosition(1, currentFinger.TransformDirection(Vector3.left) * 1000);

                if (lastTarget)
                    lastTarget.LookAtMe(false, type);
            }        

            yield return null;
        }

        line.enabled = false;
    }
}
